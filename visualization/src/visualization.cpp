#include <visualization/visualization.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include <X11/Xlib.h>
#include <pcl/io/file_io.h>
#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkRenderWindow.h>
#include <core/conversions.h>

using namespace std;
using namespace Eigen;

namespace heuros
{

bool run = true;

// -----------------------
// ----- CONSTRUCTOR -----
// -----------------------

Visualization::Visualization( int _num_viewports ) : max_viewports(4)
{
    // Init X server multithreading
    XInitThreads();

    // Setup
    num_viewports = 0;
    instructed_num_viewports = _num_viewports;
    axes_on = false;
    selected_window = 0;
    point_size = 1;
    default_feature = 0;
    update_metacloud = false;
    scene_processed = false;
    viewport_vector.resize( max_viewports );
    for( int i=0; i<max_viewports; i++ )
        viewport_vector[i].ftr_idx = default_feature;

    if( num_viewports > max_viewports ){
        cout << "Too many viewports. Setting max: " << max_viewports << endl;
        num_viewports = max_viewports;
    }

    loop_thread = threadPtr( new boost::thread( &Visualization::runViewer, this ) );
}

Visualization::~Visualization()
{
    cout << "[Heuros::Visualization] Destructor start" << endl;
    run = false;
    loop_thread->join();
    cout << "[Heuros::Visualization] Destructor end" << endl;
}

// ------------------------------------
// ----- MISC CONVENIENCE METHODS -----
// ------------------------------------

vector<string> Visualization::getMulticloudNames()
{
   vector<string> names;
   MAP_FOR_CONST( metacloud_unstable->multiclouds, string, MulticloudPtr )
       names.push_back(it->first);
   //for( map<string, int>::iterator it=annotation_map.begin(); it!=annotation_map.end(); ++it ){
   //for( int i=0; i<metacloud_unstable->multiclouds.size(); i++ )
   //    names.push_back( metacloud_unstable->multiclouds[i]->name );
   return names;
}

void Visualization::setNumViewports( int n )
{
    instructed_num_viewports = n;
}

// ----------------------------------
// ----- THREAD-RELATED METHODS -----
// ----------------------------------

void Visualization::initialize()
{
    // Remove previous renderers
    vtkSmartPointer<vtkRendererCollection> rc = viewer->getRendererCollection();
    vtkSmartPointer<vtkRenderWindow> wh = viewer->getRenderWindow();
    vtkRenderer *renderer = rc->GetNextItem();
    while ( renderer = rc->GetNextItem() ){
        wh->RemoveRenderer( renderer );
    }

    num_viewports = max_viewports;
    // !!!
    // Careful now! viewport_vector elements' id is -1
    // !!!

    float x_step = 1.0;
    if( num_viewports > 1 ) x_step = 0.5;
    float y_step = 1.0;
    if( num_viewports > 2 ) y_step = 0.5;

    for( int i=0; i<max_viewports; i++ ){

        int x = i%2;
        int y = int(i/2);
        if( max_viewports > 2 ) y = 1-y;

        ViewportWindow &vp_win = viewport_vector[i];
        //vp_win.ftr_repr = 0;
        // Here the id is assigned
        viewer->createViewPort( x*x_step+0.001, y*y_step+0.001, (x+1)*x_step-0.001, (y+1)*y_step-0.001, vp_win.id );
        vp_win.setStr();
        //viewer->addCoordinateSystem( 1.0, vp_win.id );
    }
    setBackgrounds();
    setCloudAllViewports();
}

void Visualization::rearrangeViewports( int _num_viewports )
{
    num_viewports = _num_viewports;
    float x_step = 1.0;
    if( num_viewports > 1 ) x_step = 0.5;
    float y_step = 1.0;
    if( num_viewports > 2 ) y_step = 0.5;

    vtkSmartPointer<vtkRendererCollection> rc = viewer->getRendererCollection();
    rc->InitTraversal();
    // First element is not a renderer
    vtkRenderer *renderer = rc->GetNextItem();
    // Loop the renderers
    for ( int i=0; i<max_viewports; i++ ){
        renderer = rc->GetNextItem();
        if( i<num_viewports ){
            int x = i%2;
            int y = int(i/2);
            if( num_viewports > 2 ) y = 1-y;
            renderer->SetViewport( x*x_step+0.001, y*y_step+0.001, (x+1)*x_step-0.001, (y+1)*y_step-0.001 );
        }else{
            renderer->SetViewport( 2, 2, 2.1, 2.1 );
        }
    }
    // Correct selected window if needed
    if( selected_window > num_viewports-1 ){
        selected_window = num_viewports-1;
        setBackgrounds();
    }
    // Update rendering window (needed?)
    vtkSmartPointer<vtkRenderWindow> win = viewer->getRenderWindow();
    win->Modified ();
}

void Visualization::waitForSceneProc()
{
    while( !scene_processed )
        boost::this_thread::sleep( boost::posix_time::milliseconds(1) );
}

void Visualization::runViewer()
{
    initViewer();
    initialize();
    while(run){

        if( instructed_num_viewports != num_viewports )
            rearrangeViewports( instructed_num_viewports );

        // Secure the stable metacloud
        if( update_metacloud ){
            mtx.lock();
            update_metacloud = false;
            metacloud = metacloud_unstable;
            mtx.unlock();
            // Process and display
            processMetacloud();
            scene_processed = true;
        }

        // Call stored functions
        mtx.lock();
        for( int i=0; i<call_list.size(); i++ ){
            call_list[i]();
        }
        call_list.clear();
        mtx.unlock();

        // Loop the viewer and sleep 10 ms
        viewer->spinOnce();
        boost::this_thread::sleep( boost::posix_time::milliseconds(1) );
    }
}

void Visualization::setMetacloud( const MetacloudConstPtr &_metacloud )
{
    scene_processed = false;
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();

    mtx.lock();
    update_metacloud = true;
    metacloud_unstable = _metacloud;
    mtx.unlock();

    // Get elapsed time
    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
    cout << "\033[31m" << "Visualization: Processing time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
}

void Visualization::processMetacloud()
{
    // Lock mutex
    if( !metacloud ) return;

    // Download stuff
    cloud_in = Conversions::downloadXYZRGB( metacloud );
    cloud_out = Conversions::downloadXYZRGB( metacloud );
    normals = Conversions::downloadNormal( metacloud );
    features = Conversions::downloaDFloatArrs( metacloud );
    segment_indices = Conversions::downloadSegmentIndices( metacloud );

    setCloudAllViewports();
}

// -------------------------------------
// ----- SET VISUALIZATION TARGETS -----
// -------------------------------------

void Visualization::setCloudAllViewports()
{
    if( !metacloud ) return;

    // Clear multiclouds
    //multiclouds.clear();

    // Set point cloud
    //cloud = _cloud;
    //cloud_out = cloud->makeShared();

    cloud_out->sensor_orientation_ = Eigen::Quaternionf(1,0,0,0);

    for( int i=0; i<max_viewports; i++ ){

        ViewportWindow &vp_win = viewport_vector[i];

        //displayColors(i);
        attachFeatureToViewport( i, vp_win.ftr_idx );

        // Prevent red warnings
        if( vp_win.ftr_idx == -1 ) continue;

        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_IMMEDIATE_RENDERING, 1.0, vp_win.id_str + "_cloud", vp_win.id );
    }

    setBackgrounds();
}

// --------------------------
// ----- OTHER SETTINGS -----
// --------------------------

void Visualization::selectViewport( int n )
{
    call_list.push_back(
        boost::bind( &Visualization::selectViewport_, this, n )
    );
}

void Visualization::selectViewport_( int n )
{
    selected_window = n;
    if( selected_window > num_viewports-1 )
        selected_window = num_viewports-1;
    setBackgrounds();
}

void Visualization::setBackgrounds()
{
    for( int i=0; i<max_viewports; i++ ){
        ViewportWindow &vp_win = viewport_vector[i];
        if( i == selected_window ){
            viewer->setBackgroundColor( 0.1, 0.1, 0.1, vp_win.id ); // +1 needed in prevous versions of PCL
        }else{
            viewer->setBackgroundColor( 0.0, 0.0, 0.0, vp_win.id ); // +1 needed in prevous versions of PCL
        }
    }
    //displayKinfu();
}

void Visualization::setPointSize( int size )
{
    call_list.push_back(
        boost::bind( &Visualization::setPointSize_, this, size )
    );
}

void Visualization::setPointSize_( int size )
{
    point_size = size;
    for( int i=0; i<max_viewports; i++ ){
        ViewportWindow &vp_win = viewport_vector[i];
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    }
}

void Visualization::setCallbacks()
{
    viewer->registerPointPickingCallback( &Visualization::pickCb, *this, 0 );
    viewer->registerKeyboardCallback( &Visualization::onKeyboard, *this, 0 );
    viewer->registerMouseCallback( &Visualization::onMouse, *this, 0 );
}

void Visualization::initViewer()
{
    viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer>(new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->initCameraParameters();
    viewer->setCameraPosition( 1.0, 0.6,-1.0,
                               1.0, 1.0, 1.0,
                               0.0,-1.0, 0.0 );
    setCallbacks();
}

void Visualization::displayFeature( int ftr_idx )
{
    call_list.push_back(
        boost::bind( &Visualization::attachFeatureToViewport, this, -1, ftr_idx, "" )
    );
}

void Visualization::attachFeatureToViewport( int window_pos, int ftr_idx, string title )
{
    if( window_pos < 0 ) window_pos = selected_window;
    ViewportWindow &vp_win = viewport_vector[window_pos];
    assert( vp_win.id > 0 );
    vp_win.ftr_idx = ftr_idx;
    vp_win.title = title;

    if( !metacloud || ftr_idx < -1 )
        return;

    int n_point_ftrs = metacloud->numFeatures();
    int n_segment_sets = metacloud->numSegmentSets();

    vector<string> ftr_names = SimpleFeatures::getFeatureNames();
    vector<string> seg_names = segmentation->getFeatureNames();

    if( ftr_idx == -1 ){ // Colors
        if( title == "" ) vp_win.title = "RGB colors";
        displayColors( window_pos );
    }else if( ftr_idx < n_point_ftrs ){ // Point feature
        if( title == "" ) vp_win.title = ftr_names[ftr_idx];
        displayCurrentFeature( window_pos );
    }else if( ftr_idx < n_point_ftrs + n_segment_sets ){ // Segment set
        int set_idx = ftr_idx - n_point_ftrs;
        if( title == "" ) vp_win.title = seg_names[set_idx];
        displaySegmentSet( window_pos, set_idx );
    }else if( ftr_idx == n_point_ftrs + n_segment_sets ){ // Colors
        displayColors( window_pos );
    }else if( ftr_idx == n_point_ftrs + n_segment_sets + 1 ){ // No feature
        displayCloudOnly( window_pos );
    }
}

// -----------------------------
// ----- DISPLAY FUNCTIONS -----
// -----------------------------

void Visualization::displayColors( int window_pos )
{
    if( !cloud_out ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB>rgb(cloud_in);
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, rgb, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "RGB colors", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    // Display normals
    setNormals_( window_pos, vp_win.show_normals );

    // Display cluster
    //displayCluster( window_pos );

    // Display results
    displayResults( window_pos );
}

void Visualization::displayCloudOnly( int window_pos )
{
    if( !cloud_out ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    for( int i=0; i<cloud_out->size(); i++ ){
        unsigned int rgb_uint = 100 << 16 | 100 << 8 | 0;
        cloud_out->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( cloud_out );
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "No feature", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    // Display results
    displayResults( window_pos );
}

void Visualization::displayCurrentFeature( int window_pos )
{
    if( !cloud_out ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    if( vp_win.ftr_idx < 0 ){
        //displayColors( window_pos );
        displayCloudOnly( window_pos );
        return;
    }

    clearWindow( window_pos );

    for( int i=0; i<cloud_out->size(); i++ ){

        float ftr_val = features[ vp_win.ftr_idx ]->at(i);
        cv::Vec3b rgb = representations.f2givenRepresentation( ftr_val, vp_win.ftr_repr );

        unsigned int rgb_uint = (unsigned int)rgb[0] << 16 | (unsigned int)rgb[1] << 8 | (unsigned int)rgb[2];
        cloud_out->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( cloud_out );
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( vp_win.title, 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    // Display normals
    setNormals_( window_pos, vp_win.show_normals );

    // Display cluster
    //displayCluster( window_pos );

    // Display results
    displayResults( window_pos );

    //Display props
    //displayProps(window_pos);
}

void Visualization::displaySegmentSet( int window_pos, int set_idx )
{
    if( !cloud_out ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    // Pick segment colors
    vector<cv::Vec3b> colors( metacloud->segmented_features[set_idx][0]->values.size() );
    for( int i=0; i<colors.size(); i++ ){
        // Get segment's number of points
        //int num_pts = metacloud->segmented_features[set_idx][0]->values[i];
        //colors[i] = Representations::f2rainbow( (num_pts%10000)/5000.0-1.0 );

        // Pick the color
        if( vp_win.ftr_repr > 0 ){
            cv::Vec3b color = cv::Vec3b( rand()%256, rand()%256, rand()%256 );
            color[rand()%3] = 255;
            colors[i] = color;
        }else{
            cv::Vec3b color = cv::Vec3b( 255, 255, 255 );
            colors[i] = color;
        }
    }

    // Get the segment indices
    IndicesConstPtr seg_indices = segment_indices[set_idx];

    for( int i=0; i<cloud_out->size(); i++ ){
        cv::Vec3b rgb;
        // Read segment index
        int seg_idx = seg_indices->at(i);
        if( seg_idx < 0 ){
            float ftr_val = -0.5;
            if( vp_win.ftr_repr == 0 )
                ftr_val = 0.5*features[ default_feature ]->at(i);

            rgb = representations.f2givenRepresentation( ftr_val, 0 );
        }else
            rgb = colors[seg_idx];

        unsigned int rgb_uint = (unsigned int)rgb[0] << 16 | (unsigned int)rgb[1] << 8 | (unsigned int)rgb[2];
        cloud_out->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( cloud_out );
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( vp_win.title, 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    //Display results
    displayResults( window_pos );
}

/*void Visualization::colorSegment( int window_pos, int segment_index )
{
    if( !cloud_out ) return;
    if( segment_index >= metacloud->segmented_points.size() ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    for( int i=0; i<metacloud->segmented_points[segment_index]->size(); i++ ){
        unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
        cloud_out->at( metacloud->segmented_points[segment_index]->at(i) ).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( cloud_out );
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
}*/

void Visualization::displayResults( int window_pos, string multicloud_name, string subcloud_name )
{
    // Set viewport window algo and class if given
    ViewportWindow &vp_win = viewport_vector[window_pos];
    if( !multicloud_name.empty() ) vp_win.multicloud = multicloud_name;
    if( !subcloud_name.empty() ) vp_win.subcloud = subcloud_name;

    if( !cloud_out ) return;
    if( !vp_win.show_results ) return;

    // Get cloud
    if( !metacloud->multiclouds.count(vp_win.multicloud) ) return;
    MulticloudConstPtr multicloud = metacloud->multiclouds.at(vp_win.multicloud);
    if( !multicloud ) return;
    if( !multicloud->clouds.count(vp_win.subcloud) ) return;
    DPointCloudConstPtr curr_cloud = multicloud->clouds.at(vp_win.subcloud);
    if( !curr_cloud ) return;

    // Remove shapes, clouds and title
    viewer->removeAllShapes(vp_win.id );

    // Create point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr results_cloud( new pcl::PointCloud<pcl::PointXYZRGB> );
    results_cloud->resize(curr_cloud->size());
    vector<DPoint> h_curr_cloud( curr_cloud->size() );
    curr_cloud->download( h_curr_cloud.data() );
    for( int i=0; i<curr_cloud->size(); i++ ){
        //pcl::PointXYZ &point = (pcl::PointXYZ&)(curr_cloud->data[i]);
        results_cloud->at(i).x = h_curr_cloud[i].x;
        results_cloud->at(i).y = h_curr_cloud[i].y;
        results_cloud->at(i).z = h_curr_cloud[i].z;
        unsigned int rgb_uint = 0 << 16 | 255 << 8 | 0;
        results_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    // Set title
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( results_cloud );
    viewer->addPointCloud<pcl::PointXYZRGB>( results_cloud, custom, vp_win.id_str + "_results", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, vp_win.id_str + "_results", vp_win.id );
    viewer->addText( vp_win.multicloud, 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

/*void Visualization::displayProps(int window_pos)
{
    if( !cloud_out ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];
    if( !vp_win.show_results ) return;

    clearWindow( window_pos );

    for(size_t i=0; i<metacloud->props.size(); i++){
        Prop pr = metacloud->props[i];
        pcl::PointXYZ center = pr.center;

        // Pick unique name
        string name = pr.object_class;
        stringstream ss;
        ss << "prop_" << i << "_" << vp_win.id_str;

        // Display name
        pcl::PointXYZ text_pos = pr.center;
        text_pos.y -= 0.1;
        text_pos.x -= 0.1;
        viewer->addText3D( name, text_pos, 0.05, 0.0, 1.0, 0.0, ss.str()+"_name", vp_win.id+1 );

        // Display marker
        viewer->addSphere(center, 0.02, 0, 1, 0, ss.str(), vp_win.id );

        int seg_idx = pr.segment_index;
        for( int i=0; i<metacloud->segmented_points[seg_idx]->size(); i++ ){
            unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
            cloud_out->at( metacloud->segmented_points[seg_idx]->at(i) ).rgb = *reinterpret_cast<float*>(&rgb_uint);
        }
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( cloud_out );
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "Recognized objects", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}*/

void Visualization::displayCluster()
{
    call_list.push_back(
        boost::bind( &Visualization::displayCluster, this, -1 )
    );
}

void Visualization::displayCluster( int window_pos )
{
    if( !cloud_out ) return;
    AnnotationPtr annot = segmentation->getSelectedAnnotation(); 
    if( !annot ) return;
    vector<int> &cluster = annot->indices;
    if( window_pos < 0 ) window_pos = selected_window;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    for( int i=0; i<cloud_out->size(); i++ ){
        float ftr_val = features[ default_feature ]->at(i);
        cv::Vec3b rgb = representations.f2givenRepresentation( ftr_val, 0 );
        unsigned int rgb_uint = rgb_uint = (unsigned int)rgb[0] << 16 | (unsigned int)rgb[1] << 8 | (unsigned int)rgb[2];
        cloud_out->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    for( int i=0; i<cluster.size(); i++ ){
        unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
        cloud_out->at( cluster.at(i) ).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom( cloud_out );
    viewer->addPointCloud<pcl::PointXYZRGB>( cloud_out, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "Cluster", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    // Sparse clusters composed of points
    if( cluster.size() < 200 ){
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cluster_cloud( new pcl::PointCloud<pcl::PointXYZRGB> );
        cluster_cloud->resize(cluster.size());
        for( int i=0; i<cluster.size(); i++ ){
            //pcl::PointXYZ &point = (pcl::PointXYZ&)(curr_cloud->data[i]);
            cluster_cloud->at(i).x = cloud_out->at( cluster.at(i) ).x;
            cluster_cloud->at(i).y = cloud_out->at( cluster.at(i) ).y;
            cluster_cloud->at(i).z = cloud_out->at( cluster.at(i) ).z;
            unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
            cluster_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
        }
        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> custom2( cluster_cloud );
        viewer->addPointCloud<pcl::PointXYZRGB>( cluster_cloud, custom2, vp_win.id_str + "_cluster", vp_win.id );
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, vp_win.id_str + "_cluster", vp_win.id );
    }
}

void Visualization::setNormals( bool on_off )
{
    call_list.push_back(
        boost::bind( &Visualization::setNormals_, this, -1, on_off )
    );
}

void Visualization::setNormals_( int window_pos, bool on_off )
{
    if( window_pos < 0 ) window_pos = selected_window;
    if( !cloud_out ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    viewer->removePointCloud( vp_win.id_str + "_normals", vp_win.id );

    if( on_off == true ){
        vp_win.show_normals = true;
        viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal>( cloud_out, normals, 10, 0.02, vp_win.id_str + "_normals", vp_win.id );
    }else if( on_off == false ){
        vp_win.show_normals = false;
    }
}

void Visualization::setRepresentation( int repr )
{
    call_list.push_back(
        boost::bind( &Visualization::setRepresentation_, this, -1, repr )
    );
}

void Visualization::setRepresentation_( int window_pos, int repr )
{
    if( window_pos < 0 ) window_pos = selected_window;
    if( repr >= representations.num_representations ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    vp_win.ftr_repr = repr;
    attachFeatureToViewport( window_pos, vp_win.ftr_idx );
}

void Visualization::clearWindow( int window_pos )
{
    ViewportWindow &vp_win = viewport_vector[window_pos];
    viewer->removeAllPointClouds( vp_win.id );
    viewer->removeAllShapes( vp_win.id );
}

void Visualization::setResults( int window_pos, int on_off,
                                string multicloud_name, string subcloud_name )
{
    ViewportWindow &vp_win = viewport_vector[window_pos];
    if( on_off == 0 ) vp_win.show_results = false;
    else if( on_off == 1 ) vp_win.show_results = true;
    else vp_win.show_results = !vp_win.show_results;

    if( !multicloud_name.empty() ) vp_win.multicloud = multicloud_name;
    if( !subcloud_name.empty() ) vp_win.subcloud = subcloud_name;
}

// ----------------------------------
// ----- HANDLE KEYBOARD EVENTS -----
// ----------------------------------

void Visualization::onKeyboard( const pcl::visualization::KeyboardEvent &event, void* )
{
    ViewportWindow &vp_win = viewport_vector[ selected_window ];

    // ================== All viewports options ==================

    // Increase point size
    if( event.getKeyCode() == '+' && event.keyDown () )
    {
        if( point_size < 7 )
            setPointSize_( point_size+1 );
    }

    // Decrease point size
    if( event.getKeyCode() == '-' && event.keyDown () )
    {
        if( point_size > 1 )
            setPointSize_( point_size-1 );
    }

    // Increase number of viewports
    if( event.getKeyCode() == ']' && event.keyDown () )
    {
        if( num_viewports < 4 )
            instructed_num_viewports++;
        if( instructed_num_viewports == 3 )
            instructed_num_viewports++;
    }

    // Decrease number of viewports
    if( event.getKeyCode() == '[' && event.keyDown () )
    {
        if( num_viewports > 1 )
            instructed_num_viewports--;
        if( instructed_num_viewports == 3 )
            instructed_num_viewports--;
    }

    // Show / hide coordinate system
    if( event.getKeyCode() == 'a' && event.keyDown () )
    {
        if( axes_on ){
            for( int i=0; i<max_viewports; i++ )
                viewer->removeCoordinateSystem( viewport_vector[i].id_str + "_cs",
                                                viewport_vector[i].id );
            axes_on = false;
        }else{
            for( int i=0; i<max_viewports; i++ )
                viewer->addCoordinateSystem( 1.0, viewport_vector[i].id_str + "_cs",
                                             viewport_vector[i].id );
            axes_on = true;
        }
    }

    // ================== Viewport navigation ==================

    if (event.getKeySym () == "Right" && event.keyDown ())
    {
        if( selected_window < num_viewports-1 )
            selected_window++;
        else
            selected_window = 0;
        setBackgrounds();
    }

    if (event.getKeySym () == "Left" && event.keyDown ())
    {
        if( selected_window >= 0 ) // allow -1 to hide the selection
            selected_window--;
        else
            selected_window = num_viewports-1;
        setBackgrounds();
    }

    if (event.getKeySym () == "Down" && event.keyDown ())
    {
        if( selected_window < 2 && num_viewports > 2 )
            selected_window += 2;
        else
            selected_window = num_viewports-1;
        setBackgrounds();
    }

    if (event.getKeySym () == "Up" && event.keyDown ())
    {
        if( selected_window > 1 && num_viewports > 2 )
            selected_window -= 2;
        else
            selected_window = 0;
        setBackgrounds();
    }

    // ================== Current viewport options ==================

    if( selected_window < 0 ){
        cout << "WARNING: No viewport selected." << endl;
        return;
    }

    // Display colors
    if( event.getKeySym() == "c" && event.keyDown () )
    {
        attachFeatureToViewport( selected_window, -1 );
    }

    // Display segmented cluster
    if ((event.getKeySym() == "s" && event.keyDown ()) && event.isCtrlPressed())
    {
        cout << "SAVING CLOUD" << endl;
        io->saveClusterAsCloud();
    }

    // Display segmented cluster
    //if( event.getKeySym() == "s" && event.keyDown () )
    //{
    //    displayCluster( selected_window );
    //}

    // Toggle normals
    if( event.getKeySym() == "n" && event.keyDown () )
    {
        if( vp_win.show_normals )
            setNormals_( selected_window, false );
        else
            setNormals_( selected_window, true );
    }

    // Change feature representation (cyclic)
    if( event.getKeyCode() == '`' && event.keyDown () )
    {
        int n_point_ftrs = metacloud->numFeatures();
        int n_segment_sets = metacloud->numSegmentSets();

        if( vp_win.ftr_idx < n_point_ftrs ){
            if( vp_win.ftr_repr < representations.num_representations-1 )
                vp_win.ftr_repr++;
            else
                vp_win.ftr_repr = 0;
        }else if( vp_win.ftr_idx < n_point_ftrs + n_segment_sets ){
            if( vp_win.ftr_repr == 0 )
                vp_win.ftr_repr = 1;
            else
                vp_win.ftr_repr = 0;
        }
        attachFeatureToViewport( selected_window, vp_win.ftr_idx );
    }

    // Hide features
    if( event.getKeyCode() == '0' )
    {
        displayCloudOnly( selected_window );
    }

    // Display the selected feature
    char code = event.getKeyCode();
    int feature_idx = atoi( &code ) - 1;
    if( feature_idx >= 0 )
    {
        attachFeatureToViewport( selected_window, feature_idx );
    }
}

// -------------------------------
// ----- HANDLE MOUSE EVENTS -----
// -------------------------------

void Visualization::onMouse( const pcl::visualization::MouseEvent &event, void* )
{
    if( event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease )
    {
        unsigned int mouse_x = event.getX();
        unsigned int mouse_y = event.getY();
        //viewer->getViewerPose()
    }
}

void Visualization::pickCb( const pcl::visualization::PointPickingEvent &event, void* args )
{
    pcl::PointXYZ point_xyz;
    event.getPoint( point_xyz.x, point_xyz.y, point_xyz.z );
    int point_idx = event.getPointIndex();

    // Return if the cloud is not segmented
    if (!metacloud->numSegmentSets())
        return;

    // Return if the point is from another cloud;
    pcl::PointXYZRGB pt_from_idx = cloud_in->at(point_idx);
    if( pt_from_idx.x != point_xyz.x || pt_from_idx.y != point_xyz.y || pt_from_idx.z != point_xyz.z )
        return;

    // Update selection
    segmentation->updateSelection( point_idx, 1, false );

    // Display the cluster
    ViewportWindow &vp_win = viewport_vector[ selected_window ];
    displayCluster( selected_window );
}

}
