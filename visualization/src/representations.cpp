#include <visualization/representations.h>

using namespace std;

Representations::Representations()
{
    // Just add a function and push it back to the vector

    representation_functions.push_back( f2grayscale );
    representation_functions.push_back( f2coldHot );
    representation_functions.push_back( f2blueYellow );
    representation_functions.push_back( f2abnormal );

    num_representations = representation_functions.size();
}

cv::Vec3b Representations::f2givenRepresentation( const float x, const int representation )
{
    return representation_functions[ representation ]( x );
}

cv::Vec3b Representations::f2coldHot( const float x )
{
    int r, g, b;

    if( x < 0 ){
        b = 255;
        r = 255 * (x+1);
        g = r;
    }else{
        r = 255;
        b = 255 * (1-x);
        g = b;
    }

    int sgn = 1;
    if( x<0 ) sgn = -1;
    float v = 1 - 0.2 * pow( x, 4 );
    //float v = 1-0.3*fabs(x);
    r*=v; g*=v; b*=v;

    // handle NaN
    if( NAN_CHECK(x) ){
        r = 0; g = 0; b = 0;
    }

    // handle limits
    if( x > 1 ){
        r = 60; g = 0; b = 0;
    }else if( x < -1 ){
        r = 0; g = 0; b = 60;
    }

    return cv::Vec3b( r, g, b );
}

cv::Vec3b Representations::f2grayscale( const float x )
{
    int r = 255 * (x+1) * 0.5;
    int g = r;
    int b = r;

    // handle NaN
    if( NAN_CHECK(x) ){
        r = 0; g = 0; b = 0;
    }

    // handle limits
    if( x > 1 ){
        r = 255; g = 255; b = 255;
    }else if( x < -1 ){
        r = 0; g = 0; b = 0;
    }

    return cv::Vec3b( r, g, b );
}

cv::Vec3b Representations::f2blueYellow( const float x )
{
    int r = 255 * (x+1) * 0.5;
    int g = r;
    int b = 255 * (1-x) * 0.5;

    // handle NaN
    if( NAN_CHECK(x) ){
        r = 0; g = 0; b = 0;
    }

    // handle limits
    if( x > 1 ){
        r = 255; g = 0; b = 0;
    }else if( x < -1 ){
        r = 0; g = 255; b = 0;
    }

    return cv::Vec3b( r, g, b );
}

cv::Vec3b Representations::f2abnormal( const float x )
{
    int r = 0;
    int g = 0;
    int b = 0;

    // handle NaN
    if( NAN_CHECK(x) ){
        r = 255; g = 255; b = 255;
    }

    // handle limits
    if( x > 1 ){
        r = 255; g = 0; b = 0;
    }else if( x < -1 ){
        r = 0; g = 0; b = 255;
    }

    return cv::Vec3b( r, g, b );
}

cv::Vec3b Representations::hsv2rgb( cv::Vec3f hsv ){

    float h = hsv[0];
    float s = hsv[1];
    float v = hsv[2];

    float r, g, b;

    int i = floor(h * 6);
    float f = h * 6 - i;
    float p = v * (1 - s);
    float q = v * (1 - f * s);
    float t = v * (1 - (1 - f) * s);

    switch(i % 6){
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }

    return cv::Vec3b( 255*r, 255*g, 255*b );
}

cv::Vec3b Representations::f2rainbow( float x )
{
    cv::Vec3f hsv( 0.66*(1-0.5*(1+x)), 1.0, 1.0 );
    cv::Vec3b rgb = hsv2rgb( hsv );
    float r = rgb[0], g = rgb[1], b = rgb[2];

    //return Vec3b( r, g, b );
    return cv::Vec3b( b, g, r );
}
