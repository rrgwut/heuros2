#ifndef VISUALIZATION_H
#define VISUALIZATION_H


#include <iostream>
#include <boost/function.hpp>
#include <cv.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <vtkImageMapper.h>
#include <sensor_msgs/Image.h>

#include <core/metacloud.h>
#include <core/multicloud.h>
#include <features/simple_features.h>
#include <segmentation/segmentation.h>
#include <interface/interface.h>
#include <io/io.h>
#include <visualization/representations.h>

typedef boost::shared_ptr<sensor_msgs::Image> ImagePtr;
typedef boost::shared_ptr<boost::thread> threadPtr;


namespace heuros
{

class SimpleFeatures;
class Segmentation;
class Interface;
class IO;

class Visualization
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Pointers to the other modules instances */
    // @{
    Segmentation *segmentation;
    Interface *interface;
    IO *io;
    // }@

    /** \brief Vector of multicloud shared pointers */
    //vector<MulticloudPtr> multiclouds;

    /** \brief Index of the selected window */
    int selected_window;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. Requires number of viewports */
    Visualization( int _num_viewports );

    ~Visualization();

    /** \brief Initialize the viewer object */
    void initialize();

    /** \brief Return the current list of multicloud names */
    std::vector<std::string> getMulticloudNames();

    /** \brief Change the number of viewports */
    void setNumViewports( int n );

    /** \brief Set the active viewport */
    void selectViewport( int n );

    /** \brief Set the metacoud */
    void setMetacloud( const MetacloudConstPtr &_metacloud );

    /** \brief Set point size for all viewports */
    void setPointSize( int size );

    /** \brief Visualize the selected feature */
    void displayFeature( int ftr_idx );

    /**\brief Toggle or change results display for a viewport */
    void setResults( int window_pos , int on_off=-1,
                     std::string multicloud_name="", std::string subcloud_name="" );

    /** \brief Display cluster */
    void displayCluster();

    /** \brief Display multicloud results. -1 means don't change */
    void displayResults( int window_pos, std::string multicloud_name="", std::string subcloud_name="" );

    /** \brief Display props */
    //void displayProps( int window_pos );

    /** \brief Color segment */
    //void colorSegment( int window_pos, int segment_index );

    /** \brief Turn normals on (1), off (0) */
    void setNormals( bool on_off );

    /** \brief Set the color representation of the features */
    void setRepresentation( int repr );

    /** \brief Blocking call to wait until the scene is processed */
    void waitForSceneProc();

protected:

    //=====================================================
    //================= PROTECTED STRUCTURES ==============
    //=====================================================

    /** \brief Structure holding settings for the display windows */
    struct ViewportWindow
    {
        int id; // Not the same as position on the viewport_vector

        volatile int ftr_idx; // Index of the active feature

        std::string multicloud, subcloud; // Names of the active multicloud and subcloud

        std::string id_str; // Id number as string for pointcloud names

        std::string title; // Displayed title

        bool show_colors, show_normals, show_results;

        int ftr_repr; // Feature representation style

        ViewportWindow()
        {
            id = -1;
            ftr_idx = 0;
            show_normals = show_results = show_results = false;
            ftr_repr = COLD_HOT;
        }

        void setStr()
        {
            std::stringstream ss;
            ss << id;
            id_str = ss.str();
        }
    };

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief PCLVisualizer object */
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;

    /** \brief Most recent metacloud shared pointer */
    MetacloudConstPtr metacloud_unstable;

    /** \brief Stable Metacloud shared pointer for visualization */
    MetacloudConstPtr metacloud;

    /** \brief Kinfu view shared pointer */
    ImagePtr kinfu_view;

    /** \brief Vector of stored callbacks */
    std::vector<boost::function<void(void)> > call_list;

    /** \brief Wether to update the metacloud */
    volatile bool update_metacloud, scene_processed;

    /** \brief Whether currently displaying the axes */
    bool axes_on;

    /** \brief Maximum number of viewports handled by the visualizer */
    const int max_viewports;

    /** \brief Number of viewports (display windows) */
    int num_viewports;

    /** \brief Number of viewports instructed to be set */
    int instructed_num_viewports;

    /** \brief Point size for all viewports */
    int point_size;

    /** \brief Feature displayed by default */
    int default_feature;

    /** \brief Mutex used to set new cloud */
    boost::mutex mtx;

    /** \brief Visualization thread object pointer */
    threadPtr loop_thread;

    /** \brief Input pointcloud */
    pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud_in;

    /** \brief Input normals */
    pcl::PointCloud<pcl::Normal>::ConstPtr normals;

    /** \brief Output (colored) pointcloud */
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_out;

    /** \brief Point segment indices for different segment sets */
    std::vector<IndicesPtr> segment_indices;

    /** \brief Point cloud features */
    std::vector<FeaturePtr> features;

    /** \brief Vector of viewport windows */
    std::vector<ViewportWindow> viewport_vector;

    /** \brief Features representation object */
    Representations representations;

    /** \brief Kinfu visualization objects */
    vtkSmartPointer<vtkImageMapper>kinfu_mapper;
    vtkSmartPointer<vtkActor2D>kinfu_actor;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Launch viewer loop in another thread */
    void runViewer();

    /** \brief Process the metacloud for visualization */
    void processMetacloud();

    /** \brief Set the new sizes for num_viewports */
    void rearrangeViewports( int _num_viewports );

    /** \brief Set input pointcloud */
    void setCloudAllViewports();

    /** \brief Set the active viewport */
    void selectViewport_( int n );

    /** \brief Set backgrond color for all viewport windows (i.e. to visualize selection) */
    void setBackgrounds();

    /** \brief Set number of view ports (visualization windows) */
    void initViewer();

    /** \brief Attach a feature (vector of floats) to a view port (internal) */
    void attachFeatureToViewport( int window_pos, int ftr_idx, std::string title="" );

    /** \brief Display cloud with rgb colors */
    void displayColors( int window_pos );

    /** \brief Display cloud without color */
    void displayCloudOnly( int window_pos );

    /** \brief Display the selected segment set */
    void displaySegmentSet( int window_pos, int set_idx );

    /** \brief Visualize the currently set feature (internal) */
    void displayCurrentFeature( int window_pos );

    /** \brief Display cluster (internal) */
    void displayCluster( int window_pos );

    /** \brief Set point size for all viewports (internal) */
    void setPointSize_( int size );

    /** \brief Set the color representation of the features (internal) */
    void setRepresentation_( int window_pos, int repr );

    /** \brief Turn normals on (1), off (0) (internal) */
    void setNormals_( int window_pos, bool on_off );

    /** \brief Set the callback functions to events */
    void setCallbacks();

    /** \brief Handle keyboard events */
    void onKeyboard( const pcl::visualization::KeyboardEvent &event, void* );

    /** \brief Handle mouse events */
    void onMouse( const pcl::visualization::MouseEvent &event, void* );

    /** \brief Picked point callback */
    void pickCb( const pcl::visualization::PointPickingEvent &event, void* args );

    /** \brief Remove everything from the selected viewport */
    void clearWindow( int window_pos );
};

/** \brief Class shared pointer */
// @{
typedef boost::shared_ptr<Visualization> VisualizationPtr;
typedef boost::shared_ptr<const Visualization> VisualizationConstPtr;
// @}

}

#endif
