#ifndef REPRESENTATIONS_H
#define REPRESENTATIONS_H

#include <iostream>
#include <vector>
#include <cv.h>

// Available feature representation types.

#ifndef NAN_CHECK
#define NAN_CHECK(a) a==a?false:true
#endif //NAN_CHECK

#define COLD_HOT 0
#define GRAYSCALE 1
#define BLUE_YELLOW 2
#define ABNORMAL 3

class Representations
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Vector of pointers to available representation functions */
    std::vector<cv::Vec3b (*)( const float )> representation_functions;

    /** \brief Number of available representations */
    int num_representations;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    Representations();

    /** \brief Convert float value into a given RGB color representation */
    cv::Vec3b f2givenRepresentation( const float x, const int representation );

    /** \brief Convert float value into cold-hot RGB color representation */
    static cv::Vec3b f2coldHot( const float x );

    /** \brief Convert float value into grayscale representation */
    static cv::Vec3b f2grayscale( const float x );

    /** \brief Convert float value into blue-yellow RGB color representation */
    static cv::Vec3b f2blueYellow( const float x );

    /** \brief Convert float value into abnormal detector RGB color representation */
    static cv::Vec3b f2abnormal( const float x );

    /** \brief Convert HSV to RGB */
    static cv::Vec3b hsv2rgb( cv::Vec3f hsv );

    /** \brief Convert float value into rainbow color representation */
    static cv::Vec3b f2rainbow( float x );
};

#endif
