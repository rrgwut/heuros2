#ifndef REGISTRATION_H
#define REGISTRATION_H


#include <iostream>
#include <core/metacloud.h>
#include <core/conversions.h>

#include <pcl/features/fpfh.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/registration/sample_consensus_prerejective.h>

#include <registration/registration_utils_gpu.h>

#include <boost/chrono.hpp>
#include <pcl/gpu/octree/octree.hpp>


namespace heuros
{

class Registration
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    //====================================================
    //================== PUBLIC METHODS ==================
    //====================================================

    /** \brief Constructor */
    Registration();

    /** \brief Main routine method (CPU version) */
    MetacloudPtr processCPU(const MetacloudPtr &target_multicloud_, const MetacloudPtr &source_multicloud_);

    /** \brief Main routine method (CPU version) */
    MetacloudPtr processCPUOMP(const MetacloudPtr &target_multicloud_, const MetacloudPtr &source_multicloud_);

    /** \brief Main routine method (GPU version) */
    MetacloudPtr processGPU(const MetacloudPtr &target_multicloud_, const MetacloudPtr &source_multicloud_);

protected:

    //====================================================
    //================ PROTECTED VARIABLES ===============
    //====================================================

    /** \brief Target cloud shared pointer */
    MetacloudPtr target_metacloud;

    /** \brief Source cloud shared pointer */
    MetacloudPtr source_metacloud;

    //====================================================
    //================= PROTECTED METHODS ================
    //====================================================



    //====================================================
    //================= PROTECTED DATATYPES ==============
    //====================================================

    /** \brief Point type. */
    typedef pcl::PointXYZRGB PointNT;

    /** \brief Point cloud type. */
    typedef pcl::PointCloud<PointNT> PointCloudT;

    typedef pcl::search::KdTree<PointNT> KDTreeT;

    /** \brief Feature type. */
    typedef pcl::FPFHSignature33 FeatureT;

    /** \brief Feature estimation type. */
    typedef pcl::FPFHEstimationOMP<PointNT,PointNT,FeatureT> FeatureEstimationT;

    /** \brief Feature cloud type. */
    typedef pcl::PointCloud<FeatureT> FeatureCloudT;

};

}

#endif //REGISTRATION_H
