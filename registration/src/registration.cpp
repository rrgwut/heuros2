#include <registration/registration.h>
#include <boost/chrono.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/gpu/features/features.hpp>
#include <core/utils_gpu.h>


using namespace std;
using namespace pcl;
using namespace pcl::gpu;


typedef DeviceArray<pcl::PointXYZRGB> DPointCloudXYZRGB;
typedef boost::shared_ptr<DPointCloudXYZRGB> DPointCloudXYZRGBPtr;


namespace heuros
{

Registration::Registration(){}

MetacloudPtr Registration::processCPU(const MetacloudPtr &target_metacloud_, const MetacloudPtr &source_metacloud_)
{
    assert( target_metacloud_->cloud );
    assert( source_metacloud_->cloud );
    target_metacloud = target_metacloud_;
    source_metacloud = source_metacloud_;

    //Memory download & declarations
    MetacloudPtr metacloud = MetacloudPtr( new Metacloud );
    PointCloudT::Ptr target_cloud = Conversions::downloadXYZRGB(target_metacloud);
    PointCloudT::Ptr source_cloud = Conversions::downloadXYZRGB(source_metacloud);
    PointCloud<Normal>::Ptr target_normals = Conversions::downloadNormal(target_metacloud);
    PointCloud<Normal>::Ptr source_normals = Conversions::downloadNormal(source_metacloud);
    FeatureCloudT::Ptr target_features (new FeatureCloudT(target_cloud->size(),1));
    FeatureCloudT::Ptr source_features (new FeatureCloudT(source_cloud->size(),1));
    PointCloudT::Ptr aligned_object (new PointCloudT(source_cloud->size(),1));

//    cout << "\x1B[32m" << "[Heuros::Registration] Target cloud size   = " << target_cloud->size() << "\x1B[0m" << endl;
//    cout << "\x1B[32m" << "[Heuros::Registration] Source cloud size   = " << source_cloud->size() << "\x1B[0m" << endl;
//    cout << "\x1B[32m" << "[Heuros::Registration] Target normals size = " << target_normals->size() << "\x1B[0m" << endl;
//    cout << "\x1B[32m" << "[Heuros::Registration] Source normals size = " << source_normals->size() << "\x1B[0m" << endl;
//    cout << "\x1B[32m" << "[Heuros::Registration] Source point 1 = " << source_cloud->at(0) << "\x1B[0m" << endl;
//    cout << "\x1B[32m" << "[Heuros::Registration] Target point 1 = " << target_cloud->at(0) << "\x1B[0m" << endl;

    //Estimate FPFH features
    cout << "\x1B[32m" << "[Heuros::Registration] Estimating FPFH features." << "\x1B[0m" << endl;
    pcl::FPFHEstimationOMP<PointNT,Normal,FeatureT> fest;
    fest.setRadiusSearch(0.025);
    fest.setInputCloud(target_cloud);
    fest.setInputNormals(target_normals);
    fest.compute(*target_features);
    fest.setInputCloud(source_cloud);
    fest.setInputNormals(source_normals);
    fest.compute(*source_features);

    //Ransac
    cout << "\x1B[32m" << "[Heuros::Registration] Performing ransac alignment." << "\x1B[0m" << endl;
    SampleConsensusPrerejective<PointNT,PointNT,FeatureT> ransac;
    ransac.setInputTarget(target_cloud);
    ransac.setTargetFeatures(target_features);
    ransac.setInputSource(source_cloud);
    ransac.setSourceFeatures(source_features);
    ransac.setNumberOfSamples(3);
    ransac.setCorrespondenceRandomness(2);
    ransac.setSimilarityThreshold(0.8f);
    ransac.setMaxCorrespondenceDistance(2.0*0.005);            // 1.5*0,005

    ransac.setMaximumIterations(16384);
    ransac.setInlierFraction((float)target_cloud->size() * 0.70f /source_cloud->size() );

    // DO IT!
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();
    ransac.align(*aligned_object);
    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;

    //Display original target and source clouds
    //*aligned_object = *source_cloud;

    // Display transformation matrix
//    Eigen::Matrix<float, 4, 4> transformation = ransac.getFinalTransformation();
//    cout << "\x1B[32m" << transformation << "\x1B[0m" << endl;

    if (ransac.hasConverged())
    {
        //Info
        cout << "\x1B[32m" << "[Heuros::Registration] Success!" << "\x1B[0m" << endl;
        cout << "\033[31m" << "[Heuros::Registration] CPU RANSAC time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
        cout << "\033[31m" << "[Heuros::Registration] CPU Inliers ratio: "
             << (float)ransac.getInliers().size() / target_cloud->size() << " \x1B[0m" << endl;

        //Make aligned cloud green
        for (int i=0; i<aligned_object->size(); i++)
        {
            aligned_object->at(i).r = 0;
            aligned_object->at(i).g = 255;
            aligned_object->at(i).b = 0;
        }

        //Append target cloud
        *aligned_object += *target_cloud;

        //Make target cloud red
        for (int i=(aligned_object->size()-target_cloud->size()); i<aligned_object->size(); i++)
        {
            aligned_object->at(i).r = 255;
            aligned_object->at(i).g = 0;
            aligned_object->at(i).b = 0;
        }

        //Upload to device
        DPointCloudXYZRGBPtr d_in_cloud( new DPointCloudXYZRGB(aligned_object->size()) );
        d_in_cloud->upload( aligned_object->points.data(), aligned_object->size() );

        //Split points and colors
        metacloud->cloud->create( d_in_cloud->size() );
        metacloud->colors->create( d_in_cloud->size() );
        UtilsGPU::splitPointColor( *d_in_cloud, *metacloud->cloud, *metacloud->colors );

    }
    else cout << "\x1B[32m" << "[Heuros::Registration] RANSAC CPU failed!" << "\x1B[0m" << endl;

    //Return
    return metacloud;
}

MetacloudPtr Registration::processGPU(const MetacloudPtr &target_metacloud_, const MetacloudPtr &source_metacloud_)
{
    // Remember variables
    assert( target_metacloud_ );
    assert( source_metacloud_ );
    target_metacloud = target_metacloud_;
    source_metacloud = source_metacloud_;

    //Memory definitions
    MetacloudPtr metacloud = MetacloudPtr( new Metacloud );
    pcl::gpu::Feature::PointCloud &target_cloud_gpu = (pcl::gpu::Feature::PointCloud&)(*target_metacloud->cloud);
    pcl::gpu::Feature::Normals &target_normals_gpu = (pcl::gpu::Feature::Normals&)(*target_metacloud->normals);
    pcl::gpu::Feature::PointCloud &source_cloud_gpu = (pcl::gpu::Feature::PointCloud&)(*source_metacloud->cloud);
    pcl::gpu::Feature::Normals &source_normals_gpu = (pcl::gpu::Feature::Normals&)(*source_metacloud->normals);

    //Fpfh settings
    float fpfh_search_radius = 0.025;
    int target_avg_nbrs = fpfh_search_radius*fpfh_search_radius / (NBR_R_1*NBR_R_1) * target_metacloud->avg_neighbors;
    int source_avg_nbrs = fpfh_search_radius*fpfh_search_radius / (NBR_R_1*NBR_R_1) * source_metacloud->avg_neighbors;
    int fpfh_target_max_results = target_avg_nbrs*1.2;
    int fpfh_source_max_results = source_avg_nbrs*1.2;

    //Target fpfh estimation on gpu
    cout << "\x1B[32m" << "[Heuros::Registration] Estimating FPFH features for source cloud." << "\x1B[0m" << endl;
    DeviceArray2D<FPFHSignature33> target_features_gpu;
    pcl::gpu::FPFHEstimation target_fpfh_estimation;
    target_fpfh_estimation.setInputCloud(target_cloud_gpu);
    target_fpfh_estimation.setInputNormals(target_normals_gpu);
    target_fpfh_estimation.setRadiusSearch(fpfh_search_radius, fpfh_target_max_results);
    target_fpfh_estimation.compute(target_features_gpu);

    //Source fpfh estimation on gpu
    cout << "\x1B[32m" << "[Heuros::Registration] Estimating FPFH features for target cloud." << "\x1B[0m" << endl;
    DeviceArray2D<FPFHSignature33> source_features_gpu;
    pcl::gpu::FPFHEstimation source_fpfh_estimation;
    source_fpfh_estimation.setInputCloud(source_cloud_gpu);
    source_fpfh_estimation.setInputNormals(source_normals_gpu);
    source_fpfh_estimation.setRadiusSearch(fpfh_search_radius, fpfh_source_max_results);
    source_fpfh_estimation.compute(source_features_gpu);

    //Building octree for target cloud
    cout << "\x1B[32m" << "[Heuros::Registration] Building octree for target cloud." << "\x1B[0m" << endl;
    pcl::gpu::Octree target_octree_gpu;
    target_octree_gpu.setCloud(target_cloud_gpu);
    target_octree_gpu.build();

    //INFO
    cout << "\x1B[32m" << "[Heuros::Registration] Source cloud points = " << source_cloud_gpu.size()  << "\x1B[0m" << endl;
    cout << "\x1B[32m" << "[Heuros::Registration] Target cloud points = " << target_cloud_gpu.size()  << "\x1B[0m" << endl;

    //Calculate nearest neighbors in features space
    cout << "\x1B[32m" << "[Heuros::Registration] Calculating nearest neighbors in features space for source cloud." << "\x1B[0m" << endl;
    int* d_neighbors;
    boost::chrono::high_resolution_clock::time_point start_n = boost::chrono::high_resolution_clock::now();
    RegistrationUtilsGPU::calculateNeighborIndex((float*)(&(*source_features_gpu)), source_features_gpu.rows(), source_features_gpu.step(),
                                                 (float*)(&(*target_features_gpu)), target_features_gpu.rows(), target_features_gpu.step(),
                                                 &d_neighbors);
    boost::chrono::nanoseconds sec_n = boost::chrono::high_resolution_clock::now() - start_n;

    //Perform RanSaC
    cout << "\x1B[32m" << "[Heuros::Registration] Performing GPU RanSaC." << "\x1B[0m" << endl;
    RegistrationUtilsGPU::setRadius(1.5*0.005);                                                             //1.5 voxel size
    RegistrationUtilsGPU::setThreshold((float)target_cloud_gpu.size() * 0.75 / source_cloud_gpu.size());    //80% of maximm inliers ratio
    RegistrationUtilsGPU::setPoly(0.20);                                                                     //Set poly threshold to 0.2 - the lower the stronger is the rejection


    float transformation_matrix[16];
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();
    RegistrationUtilsGPU::performRanSaC(&(*source_cloud_gpu), source_cloud_gpu.size(), d_neighbors,
                                        &(*target_cloud_gpu), target_octree_gpu,
                                        transformation_matrix);
    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;

    if (RegistrationUtilsGPU::getConverged() != 1)
    {
        cout << "\033[31m" << "[Heuros::Registration] RANSAC GPU failed!" << "\x1B[0m" << endl;
        return metacloud;
    } else
    {
        int inliers = RegistrationUtilsGPU::getInliers();
        cout << "\033[31m" << "[Heuros::Registration] GPU Inliers ratio: " << (float)inliers / target_cloud_gpu.size() << " \033[0m" << endl;
        cout << "\033[31m" << "[Heuros::Registration] GPU NEIGHBORS time: " << sec_n.count()/1000000 << " ms" << "\033[0m" << endl;
        cout << "\033[31m" << "[Heuros::Registration] GPU RANSAC time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
    }

    //Convert to eigen
    Eigen::Matrix<float, 4, 4> transformation;
    memcpy(transformation.data(), transformation_matrix, 16*sizeof(float));
    std::cout << "Eigen transformation matrix = \n" << transformation << std::endl;

    //Make aligned cloud green
    PointCloudT::Ptr aligned_object = Conversions::downloadXYZRGB(source_metacloud);
    pcl::transformPointCloud(*aligned_object, *aligned_object, transformation);
    for (int i=0; i<aligned_object->size(); i++)
    {
        aligned_object->at(i).r = 0;
        aligned_object->at(i).g = 255;
        aligned_object->at(i).b = 0;
    }

    //Make target cloud red
    PointCloudT::Ptr target_cloud = Conversions::downloadXYZRGB(target_metacloud);
    *aligned_object += *target_cloud;
    for (int i=(aligned_object->size()-target_cloud->size()); i<aligned_object->size(); i++)
    {
        aligned_object->at(i).r = 255;
        aligned_object->at(i).g = 0;
        aligned_object->at(i).b = 0;
    }

    //Make source cloud blue
    PointCloudT::Ptr source_cloud = Conversions::downloadXYZRGB(source_metacloud);
    *aligned_object += *source_cloud;
    for (int i=(aligned_object->size()-source_cloud->size()); i<aligned_object->size(); i++)
    {
        aligned_object->at(i).r = 0;
        aligned_object->at(i).g = 0;
        aligned_object->at(i).b = 255;
    }

    //Upload to device
    DPointCloudXYZRGBPtr d_in_cloud( new DPointCloudXYZRGB(aligned_object->size()) );
    d_in_cloud->upload( aligned_object->points.data(), aligned_object->size() );

    //Split points and colors
    metacloud->cloud->create( d_in_cloud->size() );
    metacloud->colors->create( d_in_cloud->size() );
    UtilsGPU::splitPointColor( *d_in_cloud, *metacloud->cloud, *metacloud->colors );

    return metacloud;
}

}
