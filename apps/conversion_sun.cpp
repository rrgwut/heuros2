#include <iostream>
#include <stdlib.h>
#include <QtCore/QCoreApplication>
#include <QtGui>
#include <QApplication>
#include <QFileDialog>
#include <QProgressBar>
#include <opencv2/opencv.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <yaml-cpp/yaml.h>
#include <rapidjson/document.h>
#include <rapidjson/reader.h>
#include <rapidjson/filereadstream.h>
#include <sstream>
#include <pcl/common/transforms.h>
#include <io/io.h>

using namespace std;
using namespace cv;
using namespace heuros;

const bool use_smooth = false;
const bool visualize = false;

struct SunAnnotation
{
    string name;
    float y_min, y_max;
    vector<Point2f> xz_ctr;
};

vector<SunAnnotation> loadAnnotations( const string &file_path )
{
    // Load and parse the file
    stringstream ss;
    ifstream ifs( file_path.c_str() );
    if( !ifs.is_open() ){
        cout << "ERROR: failed to read file: " << file_path << endl;
        assert(false);
    }
    ss << ifs.rdbuf();
    ifs.close();
    rapidjson::Document d;
    if( d.Parse<0>(ss.str().c_str()).HasParseError() ) // 2
        throw std::invalid_argument("Json parse error");

    // Extract the values
    assert( d.IsObject() );
    assert( d["frames"].Size() == 1 );

    // Object labels
    const rapidjson::Value &objects = d["objects"];
    vector<SunAnnotation> annotations;
    for( int i=0; i<objects.Size(); i++ ){
        if( !objects[i].IsObject() ) continue;
        SunAnnotation annot;
        // Object name
        const rapidjson::Value &name_obj = objects[i]["name"];
        annot.name = name_obj.GetString();
        // 3D box
        const rapidjson::Value &box = objects[i]["polygon"][0];
        if( !box.IsObject() ) continue;
        annot.y_min = box["Ymin"].GetDouble();
        annot.y_max = box["Ymax"].GetDouble();
        const rapidjson::Value &X = box["X"];
        const rapidjson::Value &Z = box["Z"];
        if( X.Size() != Z.Size() ){
            cout << "WARING: Wrong X-Z contour in the annotation" << endl;
            continue;
        }
        annot.xz_ctr.resize(X.Size());
        for( int j=0; j<X.Size(); j++ )
            annot.xz_ctr[j] = Point2f( X[j].GetDouble(), Z[j].GetDouble() );
        // Push back annotation
        annotations.push_back(annot);
    }

    return annotations;
}

string getSingleFile( const QString &in_dir_path )
{
    QDir in_dir( in_dir_path );
    QStringList files = in_dir.entryList( QDir::Files );
    if( files.size() != 1 ){
        cout << "ERROR: The number of files in " << in_dir_path.toStdString() << " should be 1" << endl;
        assert(false);
    }
    string file_path = in_dir_path.toStdString() + "/" + files[0].toStdString();
    return file_path;
}

string getLastFile( QString in_dir_path )
{
    QDir in_dir( in_dir_path );
    QStringList files = in_dir.entryList( QDir::Files );
    string file_path = in_dir_path.toStdString() + "/" + files.last().toStdString();
    return file_path;
}

vector<AnnotationPtr> convertAnnotations( const vector<SunAnnotation> &s_annotations,
                                          const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud )
{
    vector<AnnotationPtr> h_annotations;
    for( int i=0; i<s_annotations.size(); i++ ){

        // Get the name
        const SunAnnotation &s_annot = s_annotations[i];
        AnnotationPtr h_annot( new Annotation );
        h_annot->name = s_annot.name;

        // Get the indices
        if( s_annot.xz_ctr.size() == 0 ) continue;
        for( int j=0; j<cloud->size(); j++ ){
            if( cloud->at(j).y > s_annot.y_min || cloud->at(j).y < s_annot.y_max ) continue;
            Point2f xz_pt = Point2f( cloud->at(j).x, cloud->at(j).z ) ;
            if( pointPolygonTest( s_annot.xz_ctr, xz_pt, false ) >= 0 )
                h_annot->indices.push_back(j);
        }
        if( h_annot->indices.size() < 5 ) continue;
        h_annotations.push_back(h_annot);
    }

    return h_annotations;
}

void saveAnnotations( const vector<AnnotationPtr> &h_annotations,
                      const string &out_path )
{
    // Output file
    ofstream out_file( out_path.c_str(), std::ios_base::out | std::ios_base::binary );
    if( !out_file.is_open() ){
        cout << "Couldn't create file " << out_path << endl;
        assert(false);
    }

    // Write the structure
    { // Use scope to ensure archive and filtering stream buffer go out of scope before stream
        boost::iostreams::filtering_streambuf<boost::iostreams::output> out;
        out.push(boost::iostreams::zlib_compressor(boost::iostreams::zlib::best_speed));
        out.push(out_file);
        boost::archive::binary_oarchive oa(out);
        oa << h_annotations;
    }
    out_file.close();
}

void saveSceneInfo( const vector<AnnotationPtr> &h_annotations,
                    const string &scene_path,
                    const string &info_path )
{
    // Read scene name
    ifstream in_file( scene_path.c_str() );
    if( !in_file.is_open() ){
        cout << "Couldn't open file " << scene_path << endl;
        assert(false);
    }
    string scene_name;
    in_file >> scene_name;
    in_file.close();

    // List and count objects
    map<string, int> annotation_map;
    for( int i=0; i<h_annotations.size(); i++ ){
        string obj_name = h_annotations[i]->name;
        annotation_map[obj_name] = annotation_map[obj_name]+1;
    }

    // Write to yaml file
    YAML::Node objects_node;
    for( map<string, int>::iterator it=annotation_map.begin(); it!=annotation_map.end(); ++it ){
        YAML::Node node;
        node["name"] = it->first;
        node["count"] = it->second;
        objects_node.push_back(node);
    }
    YAML::Node root_node;
    root_node[ "scene" ] = scene_name;
    root_node["objects"] = objects_node;
    ofstream out_file( info_path.c_str() );
    out_file << root_node;
    out_file.close();
}

void copyFile( string input, string output )
{
    string command = "cp " + input + " " + output;
    system( command.c_str() );
}

int main( int argc, char** argv )
{
    // Initialize ROS
    ros::init( argc, argv, "heuros_converter" );
    ros::NodeHandle n;

    heuros::IO io;
    QApplication* app = new QApplication( argc, argv );

    // Choose the database path
    QString in_path = QFileDialog::getExistingDirectory(
                0, "Choose database directory",
                "/home",
                QFileDialog::ShowDirsOnly
                | QFileDialog::DontResolveSymlinks);
    //QString in_path = "/home/bogdan/bogdan_data/datasets/SUN/SUNRGBD/kv1/NYUdata";

    // Create output dirs
    QString out_path = in_path + "_converted";
    QDir out_dir(out_path);
    out_dir.mkpath( out_path );
    //out_dir.mkpath( out_path + "clusters" );

    // Dirs list
    QDir in_dir(in_path);
    QStringList scene_dirs = in_dir.entryList( QDir::Dirs | QDir::NoDotAndDotDot );

    // Loop over scenes
    QProgressBar progress;
    progress.setRange(0, scene_dirs.size());
    progress.show();
    for( int i=0; i<scene_dirs.size(); i++ ){
        //if(i<0) continue;
        cout << "Processing scene: " << scene_dirs[i].toStdString() << endl;

        // Input paths
        QString in_scene_path = in_path + "/" + scene_dirs[i] + "/";
        string in_depth_path = getSingleFile( in_scene_path + "depth" );
        string in_color_path = getSingleFile( in_scene_path + "image" );
        string in_intrinsics_path = in_scene_path.toStdString() + "intrinsics.txt";
        string in_extrinsics_path = getLastFile( in_scene_path + "extrinsics" );
        string in_annotation_path = in_scene_path.toStdString() + "annotation3Dfinal/index.json";
        string in_scene_name_path = in_scene_path.toStdString() + "scene.txt";

        // Output paths
        QString out_scene_path_qt = out_path + "/" + scene_dirs[i] + "/";
        out_dir.mkpath( out_scene_path_qt );
        string out_scene_path = out_scene_path_qt.toStdString();
        string out_depth_path = out_scene_path + "depth.png";
        string out_color_path = out_scene_path + "color.jpg";
        string out_intrinsics_path = out_scene_path + "intrinsics.txt";
        string out_extrinsics_path = out_scene_path + "extrinsics.txt";
        string out_annotation_path = out_scene_path + "annotation.dat";
        string out_info_path = out_scene_path + "info.yaml";

        // Copy files
        copyFile( in_depth_path, out_depth_path );
        copyFile( in_color_path, out_color_path );
        copyFile( in_intrinsics_path, out_intrinsics_path );
        copyFile( in_extrinsics_path, out_extrinsics_path );

        // Concatenate pointcloud
        Mat color = imread(in_color_path);
        Mat depth = imread( in_depth_path, CV_LOAD_IMAGE_ANYDEPTH );
        vector<double> intrinsics, extrinsics;
        io.loadVector( in_intrinsics_path, intrinsics );
        io.loadVector( in_extrinsics_path, extrinsics );
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud = io.buildPointCloud( depth, color, intrinsics, extrinsics, true );

        // Rotate the point cloud
        Eigen::Affine3f transform = Eigen::Affine3f::Identity();
        transform.rotate( pointcloud->sensor_orientation_ );
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZRGB> ());
        pcl::transformPointCloud( *pointcloud, *transformed_cloud, transform );

        //string out_file_path = out_scene_path + "cloud.pcd";
        //io::savePCDFile( out_file_path, *pointcloud, true );

        // Read and process annotations
        vector<SunAnnotation> s_annotations = loadAnnotations( in_annotation_path );
        vector<AnnotationPtr> h_annotations = convertAnnotations( s_annotations, transformed_cloud );
        saveAnnotations( h_annotations, out_annotation_path );

        // Write file info
        saveSceneInfo( h_annotations, in_scene_name_path, out_info_path );

        progress.setValue(i);
        app->processEvents();
        if( i==10 ) break;
    }
    progress.hide();

    cout << "Done!" << endl;
    return 0;
}
