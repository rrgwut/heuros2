#include <iostream>
#include <ros/ros.h>
#include <QtGui>
#include <QApplication>

#include <agent/agent.h>
#include <io/io.h>
#include <segmentation/segmentation.h>
#include <detection/detection.h>
#include <visualization/visualization.h>
#include <interface/threaded_interface.h>
#include <registration/registration.h>

using namespace std;

int main( int argc, char** argv )
{
    // Initialize ROS
    ros::init( argc, argv, "heuros" );
    ros::NodeHandle n;

    // Create the module objects
    heuros::Agent agent;
    heuros::IO io;
    heuros::Segmentation segmentation;
    heuros::Detection detection;
    heuros::Visualization visualization(1);
    heuros::Registration registration;
    heuros::ThreadedInterface th_interface( argc, argv );
    heuros::Interface *interface = th_interface.interface.get();

    // Set pointers to interconnect module objects
    io.agent = &agent;
    io.segmentation = &segmentation;
    io.visualization = &visualization;
    agent.segmentation = &segmentation;
    agent.detection = &detection;
    agent.visualization = &visualization;
    agent.registration = &registration;
    agent.interface = interface;
    visualization.segmentation = &segmentation;
    visualization.io = &io;
    visualization.interface = interface;
    interface->io = &io;
    interface->segmentation = &segmentation;
    interface->detection = &detection;
    interface->visualization = &visualization;

    // Additional initialization
    interface->initialize();

    // Start the main ros loop
    ros::Rate rate(100);
    while( ros::ok() && interface->isVisible()){
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
