#include <features/ftr_generic.h>


using namespace std;


namespace heuros
{

FtrGeneric::FtrGeneric()
{
    cudaEventCreate( &start );
    cudaEventCreate( &stop );
}

FtrGeneric::~FtrGeneric(){}

vector<int> FtrGeneric::allocFeatures( int num_features )
{
    vector<int> ftr_indices;
    for( int i=0; i<num_features; i++ ){
        ftr_indices.push_back( metacloud->features->countUp() );
    }
    return ftr_indices;
}

void FtrGeneric::process( MetacloudPtr _metacloud )
{
    metacloud = _metacloud;

    // Perform the actual GPU calculations
    calculate();
}

}
