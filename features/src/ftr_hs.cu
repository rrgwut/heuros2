#include <features/ftr_hs.h>


using namespace std;


namespace heuros
{

__device__ void getHS( float rgb_float, float &h, float &s )
{
    unsigned int rgb = *reinterpret_cast<int*>(&rgb_float);

    float r = 1.0f/255 * ( (rgb >> 16) & 0x0000ff );
    float g = 1.0f/255 * ( (rgb >> 8)  & 0x0000ff );
    float b = 1.0f/255 * ( (rgb)       & 0x0000ff );

    float K = 0.f;

    if (g < b){
        float tmp = g; g = b; b = tmp;
        K = -1.f;
    }

    if (r < g){
        float tmp = r; r = g; g = tmp;
        K = -2.f / 6.f - K;
    }

    if (g < b){
        float tmp = g; g = b; b = tmp;
        K = -K;
    }

    float chroma = r - b;
    h = fabs(K + (g - b) / (6.f * chroma + 1e-20f));
    s = chroma / (r + 1e-20f);
    //v = r;
}

__global__ void calculateDevice( const float *rgb_begin,
                                 float* feature1_begin,
                                 float* feature2_begin,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Convenience variables
        const float rgb = *(rgb_begin + idx);
        float &feature1 = *(feature1_begin + idx);
        float &feature2 = *(feature2_begin + idx);

        // Calculate actual H, S
        getHS( rgb, feature1, feature2 );

        // Normalize to [-1, 1] range
        feature1 = feature1*2 - 1;
        feature2 = feature2*2 - 1;
    }
}

FtrHS::FtrHS(vector<string> &names)
{
   names.push_back("Hue"); 
   names.push_back("Saturation"); 
}

void FtrHS::calculate()
{
    // Allocate n features in metacloud and create vector of corresponding indices
    vector<int> ftr_indices = allocFeatures(2);

    // Set up kernel params
    int N = metacloud->cloud->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call kernel and wait for it to finish
    timerStart(start);
    calculateDevice <<< nBlocks, blockSize >>> ( metacloud->colors->ptr(),
                                                 metacloud->features->getFeaturePtr(ftr_indices[0]),
                                                 metacloud->features->getFeaturePtr(ftr_indices[1]),
                                                 N );

    cudaSafeCall(cudaGetLastError());
    cout << "HS calculated in " << fixed << timerStop(start, stop) << " ms." << endl;
    cudaSafeCall(cudaDeviceSynchronize());
}

}
