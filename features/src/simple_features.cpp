#include <features/simple_features.h>
#include <boost/chrono.hpp>


using namespace std;


namespace heuros
{

vector<string> SimpleFeatures::feature_names;

FtrTheta SimpleFeatures::ftr_theta(feature_names);
FtrConvexityAnisotropy SimpleFeatures::ftr_convexity_anisotropy(feature_names);
FtrHS SimpleFeatures::ftr_hs(feature_names);

void SimpleFeatures::process(const MetacloudPtr &_metacloud)
{
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();
    assert( _metacloud->cloud );

    // :::::::::::::::::::::::::
    // ::: EACH FEATURE HERE :::
    // :::::::::::::::::::::::::

    // Initialize local features
    const int num_features = 5;
    _metacloud->features->create( num_features, _metacloud->size());

    // Perform calculations for each feature class
    ftr_theta.process( _metacloud );
    ftr_convexity_anisotropy.process( _metacloud );
    ftr_hs.process( _metacloud );

    // Get elapsed time
    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
    cout << "\033[31m" << "SimpleFeatures: Processing time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
}

vector<string> SimpleFeatures::getFeatureNames()
{
    return feature_names;
}

}
