#ifndef FTR_THETA_H
#define FTR_THETA_H


#include <features/ftr_generic.h>


namespace heuros
{

class FtrTheta : public FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** Append the features names */
    FtrTheta(std::vector<std::string> &names);

    /** \brief Main method to calculate the feature values */
    void calculate();
};

}

#endif
