// PLEASE NOTE:
/*//-------------------------------------------------------
  ---------------------------------------------------------

    Features should be normalized to the range [-1, 1]


    Every new feature class should be included in the code
    of the following files:

        features.h

        features.cpp

  ---------------------------------------------------------
*///-------------------------------------------------------


#ifndef FTR_GENERIC_H
#define FTR_GENERIC_H

#include <core/metacloud.h>
#include <core/timers.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif


namespace heuros
{

class FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    FtrGeneric();

    /** \brief Empty destructor */
    ~FtrGeneric();

    /** \brief Main method to calculate the feature values (needs implementation) */
    virtual void calculate()=0;

    /** \brief Process the point cloud (calculate and do stuff) */
    virtual void process( MetacloudPtr _metacloud );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU metacloud */
    MetacloudPtr metacloud;

    /** \brief GPU timer events */
    cudaEvent_t start, stop;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Allocate the features on GPU metacloud and return corresponding indices */
    virtual std::vector<int> allocFeatures( int num_features );
};

}

#endif
