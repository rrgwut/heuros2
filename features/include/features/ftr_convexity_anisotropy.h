#ifndef CONVEXITY_ANISOTROPY_H
#define CONVEXITY_ANISOTROPY_H


#include <features/ftr_generic.h>


namespace heuros
{

class FtrConvexityAnisotropy : public FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    FtrConvexityAnisotropy(std::vector<std::string> &names);

    /** \brief Main method to calculate the feature values */
    void calculate();
};

}

#endif
