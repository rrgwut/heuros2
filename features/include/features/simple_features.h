#ifndef SIMPLE_FEATURES_H
#define SIMPLE_FEATURES_H


#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <core/metacloud.h>
#include <features/ftr_theta.h>
#include <features/ftr_convexity_anisotropy.h>
#include <features/ftr_hs.h>


namespace heuros
{

class SimpleFeatures
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Set input point cloud and trigger all the active feature calculations */
    static void process(const MetacloudPtr &_metacloud);

    /** \brief Get the feature names */
    static std::vector<std::string> getFeatureNames();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Feature names std::vector */
    static std::vector<std::string> feature_names;

    // :::::::::::::::::::::::::
    // ::: EACH FEATURE HERE :::
    // :::::::::::::::::::::::::

    /** \brief Theta inclination angle */
    static FtrTheta ftr_theta;

    /** \brief Convexity and anisotropy of convexity */
    static FtrConvexityAnisotropy ftr_convexity_anisotropy;

    /** \brief Color hue and saturation */
    static FtrHS ftr_hs;

};

}

#endif
