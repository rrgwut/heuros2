#ifndef FTR_HS_H
#define FTR_HS_H


#include <features/ftr_generic.h>


namespace heuros
{

class FtrHS : public FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** Append the features names */
    FtrHS(std::vector<std::string> &names);

    /** \brief Main method to calculate the feature values */
    void calculate();
};

}

#endif
