#ifndef THREADED_INTERFACE_H
#define THREADED_INTERFACE_H


#include <iostream>
#include <interface/interface.h>


namespace heuros
{

class ThreadedInterface
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Actual Interface object */
    InterfacePtr interface;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    ThreadedInterface( int argc, char** argv );

    /**
     * @brief ~ThreadedInterface        Destructor.
     */
    ~ThreadedInterface();

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Run the interface thread */
    void run( int argc, char** argv );

    /** \brief Wait for initialization */
    void waitForInit();

private:

    typedef boost::shared_ptr<boost::thread> threadPtr;

    /**
     * @brief interface_thread          Interface thread.
     */
    threadPtr interface_thread;

    /**
     * @brief thread_initialized        Thread initialized flag.
     */
    bool thread_initialized;
};

}

#endif
