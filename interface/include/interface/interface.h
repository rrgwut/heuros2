#ifndef INTERFACE_HPP
#define INTERFACE_HPP


#include <QMainWindow>
#include <boost/shared_ptr.hpp>
#include <io/io.h>
#include <features/simple_features.h>
#include <segmentation/segmentation.h>
#include <detection/detection.h>
#include <visualization/visualization.h>
#include "ui_main_window.h"
#include <QCloseEvent>


namespace heuros
{

class IO;
class SimpleFeatures;
class Segmentation;
class Detection;
class Visualization;

class Interface : public QMainWindow {

    Q_OBJECT

public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Pointers to the other modules instances */
    // @{
    IO *io;
    Segmentation *segmentation;
    Detection *detection;
    Visualization *visualization;
    // }@

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    Interface( int argc, char** argv, QWidget *parent = 0 );

    /** \brief Destructor */
    ~Interface();

    /** \brief Initialization after the pointers are set */
    void initialize();

    /** \brief Get the available results vizualizations */
    void update( const MetacloudConstPtr &_metacloud );

    //================================================
    //================= PUBLIC SLOTS =================
    //================================================

private Q_SLOTS:

    /** \brief Online capturing slots */
    // @{
    void onBtnStart();
    void onBtnStop();
    void onBtnSaveScene();
    void periodChanged( double period );
    void checkW84Heuros( bool state );
    void checkPubCPU( bool state );
    // @}

    /** \brief Load menu slots */
    // @{
    void on_combo_scene_activated( QString qstr );
    void on_combo_annotation_activated( int idx );
    void on_combo_directory_activated( int idx );
    // @}

    /** \brief Visualization slots */
    // @{
    void on_combo_representation_activated( int representation );
    void on_btn_n_clicked();
    void on_btn_0_clicked();
    void on_btn_1_clicked();
    void on_btn_2_clicked();
    void on_btn_3_clicked();
    void on_check_normals_clicked( bool state );
    void on_spin_point_size_valueChanged( int size );
    void on_spin_num_vps_valueChanged( int n );
    void on_combo_feature_activated( int ftr_idx );
    // @}

    /** \brief Detection slots */
    // @{
    void on_btn_showhide_results_clicked();
    void on_combo_multicloud_activated( int class_idx );
    void on_combo_subcloud_activated( int class_idx );
    void loadDSModels( const std::string &dataset, const std::vector<std::string> &classes );
    void loadSceneModels( const std::vector<std::string> &classes );
    // @}

    /** \brief Registration slots */
    // @{
    void on_btn_register_clicked();
    void on_combo_model_cloud_activated(QString qstr);
    // @}

    /** \brief Create clusters slots */
    // @{
    void prefixChanged( QString prefix );
    void onBtnSaveCluster();
    void onBtnUndoCluster();
    void onBtnResetCluster();
    void setSegmentation( bool value );
    void setMultiple( bool value );
    // @}

    /** \brief Scripts slots */
    // @{
    void on_pb_script_edit_clicked();
    void on_pb_script_execute_clicked();
    // @}

private:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================
    
    /** \brief Metacloud shared pointer. */
    MetacloudConstPtr metacloud;

    /** \brief GUI object */
    Ui::MainWindowDesign ui;

    /** \brief List of detection results */
    std::vector<std::string> results_list;

    /** \brief Previous number of viewports (used to avoid 3) */
    int prev_n_vps;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    void updateLoadMenu();
    void readDirectories();
    void readFeatures();


    /** \brief Get the known class names */
    void readClassNames();

    /** \brief Get the available scripts */
    void readScripts();

    /** \brief Get the avaliable base files */
    void readBaseFiles();
};

typedef boost::shared_ptr<Interface> InterfacePtr;

}

#endif // MAIN_WINDOW_H
