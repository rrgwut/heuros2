# Name of this module
set(MODULE_NAME interface)

# General settings
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS
     src/*.cpp src/*.hpp src/*.cu include/*.h include/*.hpp include/*.cuh)

# Includes
include_directories(${PROJECT_SOURCE_DIR})
include_directories(${PROJECT_BINARY_DIR})

#VTK QT5
if(VTK_QT_VERSION VERSION_GREATER "4")

  # Find qt5
  find_package(Qt5 REQUIRED COMPONENTS Gui Core)

  # Wrap headers and ui's
  qt5_wrap_cpp(${MODULE_NAME}_HDR_MOC ${CMAKE_CURRENT_SOURCE_DIR}/include/interface/interface.h)
  qt5_wrap_ui(${MODULE_NAME}_UI_MOC ${CMAKE_CURRENT_SOURCE_DIR}/ui/main_window.ui)

  # Set qt libs
  set(QT_LIBS Qt5::Core Qt5::Gui ${VTK_LIBRARIES})

# VTK QT4
else()

  # Find qt4
  find_package(Qt4 REQUIRED)

  # Wrap headers and ui's
  qt4_wrap_cpp(${MODULE_NAME}_HDR_MOC ${CMAKE_CURRENT_SOURCE_DIR}/include/interface/interface.h)
  qt4_wrap_ui(${MODULE_NAME}_UI_MOC ${CMAKE_CURRENT_SOURCE_DIR}/ui/main_window.ui)

  # Set qt libs
  set(QT_LIBS ${QT_LIBRARIES} QVTK)

endif()

  # Compile module_qt library
  add_library(${MODULE_NAME}_QT src/interface.cpp ${${MODULE_NAME}_HDR_MOC} ${${MODULE_NAME}_UI_MOC})
  target_link_libraries(${MODULE_NAME}_QT ${QT_LIBS})

  # Add module CUDA library
  cuda_add_library(${MODULE_NAME} STATIC ${SOURCES})
  target_link_libraries(${MODULE_NAME} ${MODULE_NAME}_QT )
