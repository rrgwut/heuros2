#include <interface/interface.h>
#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <ros/ros.h>
#include <ros/package.h>
#include <core/knowledge_rw.h>

using namespace std;
using namespace boost::filesystem;

namespace heuros
{

// --------------------------------
// ----- FREESTANDING METHODS -----
// --------------------------------

vector<string> getDirElements( string dir )
{
    string directory = ros::package::getPath("heuros") + "/data/" + dir;
    vector<string> file_list;
    if( exists( directory ) ){
        directory_iterator end ;
        for( directory_iterator iter(directory) ; iter != end ; ++iter )
            if ( iter->path().extension() == ".txt" || 
                 iter->path().extension() == ".pcd" || 
                 is_directory(*iter) ){
                file_list.push_back( iter->path().filename().string() );
            }
    }
    sort( file_list.begin(), file_list.end() );
    return file_list;
}

// ------------------------------------------
// ----- CONSTRUCTOR AND INITIALIZATION -----
// ------------------------------------------

Interface::Interface( int argc, char** argv, QWidget *parent ) : QMainWindow(parent)
{
    prev_n_vps = 1;

    ui.setupUi(this);

    // GUI components
    this->setFixedSize(this->size());
    ui.btn_n->setStyleSheet("* { background-color: rgb(100,100,150) }");
    ui.btn_0->setStyleSheet("* { background-color: rgb(150,150,255) }");
    ui.btn_1->setStyleSheet("* { background-color: rgb(150,150,255) }");
    ui.btn_2->setStyleSheet("* { background-color: rgb(150,150,255) }");
    ui.btn_3->setStyleSheet("* { background-color: rgb(150,150,255) }");
    cout << "[Heuros::Interface] Constructor done " << endl;
}

Interface::~Interface()
{
    cout << "[Heuros::Interface] Destructor called" << endl;
}

void Interface::initialize()
{
    // Read stuff
    readDirectories();
    updateLoadMenu();
    readFeatures();
    readScripts();
}

// ------------------------
// ----- SLOT METHODS -----
// ------------------------

void Interface::updateLoadMenu()
{
    cout << "Reloading scene, cluster and model list" << endl;


    string path_comp = ui.combo_directory->currentText().toStdString();

//    ros::NodeHandle n;
//    n.setParam( "heuros/scenes_dir", path_comp );
    
    // Scenes
    string directory = "scenes/" + path_comp;
    vector<string> scenes = getDirElements( directory );
    ui.combo_scene->clear();
    for( int i=0; i<scenes.size(); i++ ){
        ui.combo_scene->addItem( QString::fromStdString(scenes[i]) );
    }

    // Models
    string models_dir = "models";
    vector<string> models = getDirElements(models_dir);
    ui.combo_model_cloud->clear();
    for( int i=0; i<models.size(); i++ ){
        ui.combo_model_cloud->addItem(QString::fromStdString(models[i]));
    }
}

void Interface::on_combo_directory_activated( int idx )
{
    if( ui.combo_directory->currentIndex() == idx &&
        ui.combo_directory_2->currentIndex() == idx ){
        return;
    }else if( ui.combo_directory->currentIndex() != idx ){
        ui.combo_directory->setCurrentIndex( idx );
    }else{
        ui.combo_directory_2->setCurrentIndex( idx );
    }

    updateLoadMenu();
}

void Interface::on_combo_scene_activated( QString qstr )
{
    string scenes_dir = ros::package::getPath("heuros") + "/data/scenes/";
    string path_comp = ui.combo_directory->currentText().toStdString();
    string file_path = scenes_dir + path_comp + "/" + qstr.toStdString();
    io->loadScene( file_path );
}

void Interface::on_combo_annotation_activated( int idx )
{
    io->selectAnnotation(idx);
}

void Interface::readDirectories()
{
    vector<string> dir_names = getDirElements("scenes");
    ui.combo_directory->clear();
    ui.combo_directory_2->clear();
    for( int i=0; i<dir_names.size(); i++ ){
        ui.combo_directory->addItem( QString::fromStdString(dir_names[i]) );
        ui.combo_directory_2->addItem( QString::fromStdString(dir_names[i]) );
    }
    ui.combo_directory->setCurrentIndex(0);
}

void Interface::readFeatures()
{
    vector<string> features = SimpleFeatures::getFeatureNames();
    vector<string> seg_features = segmentation->getFeatureNames();
    features.insert( features.end(), seg_features.begin(), seg_features.end() );
    ui.combo_feature->clear();
    for( int i=0; i<features.size(); i++ ){
        ui.combo_feature->addItem( QString::fromStdString(features[i]) );
    }
    ui.combo_feature->addItem( "RGB colors" );
    //ui.combo_feature->addItem( "Kinect Fusion" );
    ui.combo_feature->setCurrentIndex(0);
}

void Interface::readClassNames()
{/*
    vector<string> class_names = node->getClassNames( "props" );
    ui.combo_label->clear();
    ui.combo_subcloud->clear();
    for( int i=0; i<class_names.size(); i++ ){
        ui.combo_label->addItem( QString::fromStdString(class_names[i]) );
        ui.combo_subcloud->addItem( QString::fromStdString(class_names[i]) );
    }
*/}

void Interface::update( const MetacloudConstPtr &_metacloud )
{
    metacloud = _metacloud;

    // Annotations
    ui.combo_annotation->clear();
    for( int i=0; i<metacloud->annotations.size(); i++ ){
        AnnotationPtr annot = metacloud->annotations[i];
        ui.combo_annotation->addItem( QString::fromStdString(annot->name) );
    }

    // Multiclouds
    vector<string> multicloud_names = visualization->getMulticloudNames();
    if( multicloud_names.size() == results_list.size() ) return;
    results_list = multicloud_names;

    ui.combo_multicloud->clear();
    for( int i=0; i<results_list.size(); i++ ){
        ui.combo_multicloud->addItem( QString::fromStdString(results_list[i]) );
    }

    // TODO - subcloud combo form multicloud
}

void Interface::readScripts()
{
    string directory = ros::package::getPath("heuros") + "/data/scripts/";

    vector<string> file_list;
    if( boost::filesystem::exists( directory ) ){
        boost::filesystem::directory_iterator end ;
        for( boost::filesystem::directory_iterator iter(directory) ; iter != end ; ++iter )
            if ( iter->path().extension() == ".hes" ){
                file_list.push_back( iter->path().filename().string() );
            }
    }

    for(size_t i=0; i<file_list.size(); i++){
        ui.combo_script->addItem( QString::fromStdString(file_list[i]) );
    }
}

void Interface::on_combo_representation_activated( int representation )
{
    visualization->setRepresentation( representation );
}

void Interface::on_btn_showhide_results_clicked()
{
    visualization->setResults( visualization->selected_window );
    io->reprocessIfOffline();
}

void Interface::on_btn_n_clicked()
{
    visualization->selectViewport(-1);
}

void Interface::on_btn_0_clicked()
{
    visualization->selectViewport(0);
}

void Interface::on_btn_1_clicked()
{
    visualization->selectViewport(1);
}

void Interface::on_btn_2_clicked()
{
    visualization->selectViewport(2);
}

void Interface::on_btn_3_clicked()
{
    visualization->selectViewport(3);
}

void Interface::on_check_normals_clicked( bool state )
{
    visualization->setNormals( state );
}

void Interface::on_spin_point_size_valueChanged( int size )
{
    visualization->setPointSize(size);
}

void Interface::on_spin_num_vps_valueChanged( int n )
{
    // Avoid 3
    if( n==3 ){
        prev_n_vps<3 ? n=4 : n=2;
        ui.spin_num_vps->setValue(n);
        return;
    }
    // Set prev n vps and apply current
    prev_n_vps = n;
    visualization->setNumViewports( n );
}

void Interface::on_combo_feature_activated( int ftr_idx )
{
    visualization->displayFeature( ftr_idx );
}

void Interface::prefixChanged( QString prefix )
{/*
    node->setPrefix( prefix.toStdString() );
*/}

void Interface::setSegmentation( bool value )
{/*
    if( value == false ) return;

    if( ui.radio_nonflat->isChecked() ){
        node->setSegmentation("segment_nonflat");
    }else if( ui.radio_flat->isChecked() ){
        node->setSegmentation("segment_flat");
    }else{
        node->setSegmentation("segment_point");
    }
*/}

void Interface::setMultiple( bool value )
{/*
    node->setMultiple( value );
*/}

void Interface::onBtnSaveCluster()
{
    /*string directory = ros::package::getPath("heuros") + "/data/clusters/" +
                       ui.combo_directory->currentText().toStdString() + "/";
    string obj_class = ui.combo_label->currentText().toStdString();
    io->saveCluster( directory, obj_class );
    updateLoadMenu();*/
}

void Interface::onBtnUndoCluster()
{/*
    node->undoCluster();
*/}

void Interface::onBtnResetCluster()
{/*
    node->resetCluster();
*/}

void Interface::on_combo_multicloud_activated( int algo_idx )
{
    //detection->setAlgorithm( algo_idx );
    //io->reprocessIfOffline();
    //visualization->setResults( visualization->selected_window, 1, algo_idx, 0 );
}

void Interface::on_combo_subcloud_activated( int class_idx )
{/*
    node->setObject( class_idx );
*/}

void Interface::loadSceneModels( const vector<string> &classes )
{
    for( int i=0; i<metacloud->annotations.size(); i++ ){
        const AnnotationPtr &annot = metacloud->annotations.at(i);
        cout << "Loading annotation: " << annot->name << endl;
        ui.combo_annotation->setCurrentIndex(i);
        on_combo_annotation_activated(i);
        QApplication::processEvents();

        bool is_target = false;
        vector<string> annot_split;
        boost::algorithm::split( annot_split, annot->name, boost::is_any_of(":") );
        for( int j=0; j<classes.size(); j++ )
            if( boost::iequals(annot_split[0], classes[j]) ) is_target = true;

        if( is_target ) cout << "MATCH" << endl;
            //detection->addModel( metacloud, annot );
        //else
        //    detection->addAntimodel( metacloud, annot );

        boost::this_thread::sleep( boost::posix_time::milliseconds(1000) );
    }
}

void Interface::on_btn_register_clicked()
{
    //cout << "\x1B[32m" << "[Heuros::Interface] Button register clicked" << "\x1B[0m" << endl;
    io->registerModel();
}

void Interface::on_combo_model_cloud_activated(QString qstr)
{
    string models_dir = ros::package::getPath("heuros") + "/data/models/";
    string file_path = models_dir + qstr.toStdString();
    io->loadModel(file_path);
}

void Interface::loadDSModels( const string &dataset, const vector<string> &classes )
{
    vector<string> dirs = getDirElements("scenes");
    int idx = std::find(dirs.begin(), dirs.end(), dataset) - dirs.begin();
    if( idx >= dirs.size() ){
        cout << "ERROR: Could not find dataset \"" << dataset << "\"" << endl;
        return;
    }
    ui.combo_directory->setCurrentIndex(idx);
    on_combo_directory_activated(idx);

    vector<string> scenes = getDirElements("scenes/" + dataset);
    for( int i=0; i<scenes.size(); i++ ){
        ui.combo_scene->setCurrentIndex(i);
        on_combo_scene_activated(QString::fromStdString(scenes[i]));
        QApplication::processEvents();
        loadSceneModels(classes);
    }
}

void reportScriptError( int line_no, const string &line )
{
    cout << "Wrong script syntax at line " << line_no << ":" << endl;
    cout << "\"" << line << "\"" << endl;
    cout << "Script terminated with error" << endl;
}

void Interface::on_pb_script_execute_clicked(){
    string script_name = ui.combo_script->currentText().toStdString();
    string directory = ros::package::getPath("heuros") + "/data/scripts/";

    // Parse script
    ifstream infile( (directory+script_name).c_str() );
    string line;
    int line_no = 0;
    while( getline(infile, line) ){
        ++line_no;
        istringstream iss(line);
        string cmd;
        iss >> cmd;
        if( cmd.empty() ) continue;
        if( cmd[0] == '#' ) continue;
        if( cmd == "LEARN" ){
            vector<string> classes;
            for( string subc; subc!="FROM"; iss >> subc )
                classes.push_back(subc);
            string dataset;
            iss >> dataset;
            if( dataset.empty() ){
                reportScriptError( line_no, line );
                break;
            }else{
                loadDSModels( dataset, classes );
            }
        }else if( cmd == "TRAIN" ){
            // TODO - Call detection training
        }else if( cmd == "TEST" ){
            // TODO - Run detection on all the scenes and gather stats
        }else{
            reportScriptError( line_no, line );
            break;
        }
    }
}

void Interface::on_pb_script_edit_clicked(){/*
    QString script_name = ui.combo_script->currentText();
    string path = ros::package::getPath("heuros_interface") + "/../scripts/"+script_name.toStdString();
    stringstream ss;
    ss << "gedit " << path << " &";
    system( ss.str().c_str() );
*/}

void Interface::onBtnStart()
{
    io->start();
}

void Interface::onBtnStop()
{
    io->stop();
}

void Interface::onBtnSaveScene()
{
    string directory = ros::package::getPath("heuros") + "/data/scenes/" +
                       ui.combo_directory->currentText().toStdString() + "/";
    string file_name = ui.edit_save_scene->text().toStdString();
    io->saveScene( directory + file_name );
    updateLoadMenu();
}

void Interface::periodChanged( double period )
{/*
    node->kinfuSetPeriod( period );
*/}

void Interface::checkW84Heuros( bool state )
{/*
    node->kinfuSetAutoLoop( !state );
*/}

void Interface::checkPubCPU( bool state )
{/*
    node->kinfuSetPubCPU( state );
*/}

/*void Interface::classAddModel()
{
    detection->addModel( segmentation->getSelectedAnnotation() );

    // TEMP OFF

    int algo_idx = ui.combo_multicloud->currentIndex();
    if( algo_idx < 0 ){
        algo_idx = 3;
        //ui.combo_multicloud->setCurrentIndex( algo_idx );
    }
    multicloudAlgorithmChanged( algo_idx );

    /*
    int idx = ui.combo_label->currentIndex();
    string model_name = ui.combo_label->currentText().toStdString();
    if(model_name.length() != 0){
        node->HSEAddModel(model_name);
    }
    //readClassNames();
    //ui.combo_label->setCurrentIndex(idx);

    notifyUnsavedChanges();
}*/

/*void Interface::onBtnClearModels()
{
    detection->scheduleClear();
}*/

} // namespace
