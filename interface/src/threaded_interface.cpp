#include <interface/threaded_interface.h>
#include <boost/thread.hpp>

namespace heuros
{
    ThreadedInterface::ThreadedInterface( int argc, char** argv ) : thread_initialized(false)
    {
        // Create new thread and wait till it's initialized
        interface_thread = threadPtr(new boost::thread(&ThreadedInterface::run, this, argc, argv));
        waitForInit();
    }

    ThreadedInterface::~ThreadedInterface()
    {
        cout << "[Heuros::ThreadedInterface] Destructor start" << endl;

        // Close the interface window if it is visible
        if (interface && interface->isVisible())
            interface->close();

        // Reset the interface
        interface.reset();

        // Join interface thread
        interface_thread->join();

        cout << "[Heuros::ThreadedInterface] Destructor stop" << endl;
    }

    void ThreadedInterface::run( int argc, char** argv )
    {
        // QApplication create
        QApplication app(argc, argv);

        // Create interface object
        interface = InterfacePtr( new Interface(argc, argv) );
        interface->show();

        // Close QApp if interface is closed
        app.connect(&app, SIGNAL( lastWindowClosed()), &app, SLOT(quit()) );

        // Thread initialized
        thread_initialized = true;

        // Enter app loop
        app.exec();

        // Reset the interface
        interface.reset();
    }

    void ThreadedInterface::waitForInit()
    {
        // Wait till thread is initialized
        while( !thread_initialized )
            boost::this_thread::sleep( boost::posix_time::milliseconds(10) );

        // Wait just for sure
        boost::this_thread::sleep( boost::posix_time::milliseconds(100) );
    }
}
