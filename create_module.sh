#!/bin/bash

if [ $# -eq 0 ] ; then 
  echo "ERROR. Must provide module name: create_module <NAME>"
  exit 1
else
  mkdir $1 $1/include $1/include/$1 $1/src
  echo "# Name of this module
set(MODULE_NAME $1)

# General settings
include_directories(\${CMAKE_CURRENT_SOURCE_DIR}/include)
file(GLOB_RECURSE SOURCES RELATIVE \${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS
     src/*.cpp src/*.hpp src/*.cu include/*.h include/*.hpp include/*.cuh)

# Add CUDA library
cuda_add_library(\${MODULE_NAME} \${SOURCES})
add_dependency(\${MODULE_NAME} interface)" > $1/CMakeLists.txt
fi
