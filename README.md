# README #

Heuros is a natual-inspired system of 3D visual object recognition. It is intended to run on Ubuntu Linux using ROS and PCL. It also requires a GPU with CUDA technology. For information on set up and usage refer to [the wiki page](https://bitbucket.org/rrgwut/heuros2/wiki/Home).

### Authors ###

* [Bogdan Harasymowicz-Boggio](http://repo.bg.pw.edu.pl/index.php/pl/r#/info/author/WUT392715/Bogdan%252CHarasymowicz-Boggio)
* Łukasz Chechliński
* Daniel Koguciuk

Warsaw University of Technology
