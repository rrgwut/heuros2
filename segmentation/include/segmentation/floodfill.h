#ifndef FLOODFILL_H
#define FLOODFILL_H


#include <core/metacloud.h>
#include "core/impl/manipulations.cuh"

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif


namespace heuros
{

class FloodFill
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Set DeviceArray<int> values to value */
    static void cudaSetIntsTo( pcl::gpu::DeviceArray<int> &array, int value );
        
    /** \brief Call GPU floodfill calculations */
    static void ffSegmentation( const pcl::gpu::DeviceArray<int> &connections,
                                const pcl::gpu::DeviceArray<int> &connections_sizes,
                                DIntArrPtr &point_labels,
                                DIntArrPtr &label_sources,
                                DIntArrPtr &cluster_counts,
                                int &num_clusters,
                                int num_floods,
                                int min_count );
};

}

#endif
