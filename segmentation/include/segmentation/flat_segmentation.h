#ifndef FLAT_SEGMENTATION_H
#define FLAT_SEGMENTATION_H


#include <core/metacloud.h>
#include <core/timers.h>


namespace heuros
{

// GEOMETRIC PARAMETERS
#define CTR_RESOLUTION 100

class FlatSegmentation
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Number of detected "flat" clusters */
    int num_clusters;

    /** \brief Indices to contour points (num_clusters*CTR_RESOLUTION) */
    std::vector<int> h_ctr_indices;

    /** \brief Flat coordinates of all the cloud points */
    std::vector<float2> h_flat_points;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    FlatSegmentation();

    /** \brief Main method to calculate the feature values */
    void calculate( MetacloudPtr _metacloud );

    /** Append the features names */
    void appendNames( std::vector<std::string> &names );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU metacloud */
    MetacloudPtr metacloud;

    /** \brief GPU timer events */
    cudaEvent_t start, stop;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Allocate the features on GPU metacloud and return corresponding indices */
    std::vector<int> allocFeatures( int num_features );
};

}

#endif
