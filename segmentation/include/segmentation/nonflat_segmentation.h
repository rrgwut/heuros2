#ifndef NONFLAT_SEGMENTATION_H
#define NONFLAT_SEGMENTATION_H


#include <core/metacloud.h>
#include <core/timers.h>


#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif


namespace heuros
{

class NonflatSegmentation
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Number of detected "flat" clusters */
    int num_clusters;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    NonflatSegmentation();

    /** \brief Main method to calculate the feature values */
    void calculate( MetacloudPtr _metacloud );

    /** Append the features names */
    void appendNames( std::vector<std::string> &names );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU metacloud */
    MetacloudPtr metacloud;

    /** \brief Labels linking points to "flat" clusters */
    DIntArrPtr point_labels;

    /** \brief Number of points in each cluster */
    DIntArrPtr cluster_counts;

    /** \brief GPU timer events */
    cudaEvent_t start, stop;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Allocate the features on GPU metacloud and return corresponding indices */
    std::vector<int> allocFeatures( int num_features );

};

}

#endif
