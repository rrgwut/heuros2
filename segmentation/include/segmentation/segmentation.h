#ifndef SEGMENTATION_H
#define SEGMENTATION_H


#include <opencv2/opencv.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/gpu/octree/octree.hpp>

#include <core/metacloud.h>
#include <segmentation/flat_segmentation.h>
#include <segmentation/nonflat_segmentation.h>


namespace heuros
{

class Segmentation
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    Segmentation();

    /** \brief Destructor */
    ~Segmentation();

    /** \brief Set input point cloud and trigger calculations */
    void process( const MetacloudPtr &_metacloud );

    /** \brief Update the cluster selection */
    void updateSelection( int seed_idx, int seg_set_idx, bool multiple );

    /** \brief Reset the composed cluster */
    void resetSelection();

    /** \brief Undo the last operation on the selected cluster */
    void undoSelection();

    /** \brief Get the currently selected cluster */
    AnnotationPtr getSelectedAnnotation();

    /** \brief Set the currently selected cluster */
    void setSelection( const AnnotationPtr &selection );

    /** \brief Get the feature names */
    std::vector<std::string> getFeatureNames();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud shared pointer */
    MetacloudPtr metacloud;

    /** \brief Feature names std::vector */
    std::vector<std::string> feature_names;

    /** \brief Flat segmentation GPU object */
    FlatSegmentation flat_segmentation;

    /** \brief Non-flat segmentation GPU object */
    NonflatSegmentation nonflat_segmentation;

    /** \brief GPU timer events */
    cudaEvent_t start_all, stop_all;

    /** \brief Selected segment indices */
    AnnotationPtr selected_annotation;

    /** \brief Selected segments history */
    std::vector<AnnotationPtr> selection_history;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Calculate flat features using OpenCV on CPU */
    void flatDimensionsCPU();

};

}

#endif
