#include <segmentation/flat_segmentation.h>
#include <segmentation/floodfill.h>
#include <stdio.h>
#include <iomanip>


using namespace std;
using namespace pcl::gpu;


namespace heuros
{

// CTR_RESOLUTION IS IN THE H FILE

// CONNECTION THRESHOLDS
#define THRESH_ANG 0.08 // radians
#define THRESH_HEIGHT 0.02 // meters

// FLOODFILL PARAMETERS
#define NUM_FLAT_FLOODS 15
#define MIN_FLAT_AREA 0.0025 // square meters

// SEGMENTS EXPANSION PARAMETERS
#define EXP_DIST_2_PLANE 0.002
#define NUM_EXP_ITERATIONS 2

//---------------------------------------------------------

// Works for angles from -pi to pi
__device__ float angAbsDiff( float ang_a, float ang_b )
{
    float absdiff = fabs( ang_a - ang_b );
    if( absdiff > F_PI )
        absdiff = F_2_PI - absdiff;
    return absdiff;
}

__device__ inline void rotateVector( const float mat[9], const float4 &vec, float4 &result )
{
    result.x = mat[0]*vec.x + mat[1]*vec.y + mat[2]*vec.z;
    result.y = mat[3]*vec.x + mat[4]*vec.y + mat[5]*vec.z;
    result.z = mat[6]*vec.x + mat[7]*vec.y + mat[8]*vec.z; // LATER TRY IF THIS GIVES US ANYTHING
}

__device__ float atomicCASFloat( float* address, float compare, float val )
{
    int* address_as_int = (int*)address;
    int ret = atomicCAS( address_as_int, __float_as_int(compare), __float_as_int(val) );
    return __int_as_float(ret);
}

__device__ inline void calcNormalRotation( const DNormal &normal, float rot_matrix[9] )
{
    float S1, C1, S2, C2;
    C2 = normal.z;
    if( C2>1 ) C2 = 1;
    else if( C2<-1 ) C2 = -1;
    S2 = sqrt( 1 - C2*C2 );
    if( S2 == 0 ){
        S1 = 0;
        C1 = 1;
    }else{
        C1 = normal.x / S2;
        S1 = normal.y / S2;
    }

    rot_matrix[0] = C1*C2;
    rot_matrix[1] = S1*C2;
    rot_matrix[2] = -S2;
    rot_matrix[3] = -S1;
    rot_matrix[4] = C1;
    rot_matrix[5] = 0;
    rot_matrix[6] = normal.x;
    rot_matrix[7] = normal.y;
    rot_matrix[8] = C2;
}

__global__ void floodInitialize( const DPoint *cloud_begin,
                                 const DNormal *normals_begin,
                                 int *point_labels,
                                 const int *neighbors,
                                 const int *neighbors_sizes,
                                 int *connections,
                                 int *connections_sizes,
                                 int max_neighbors,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Get the point and normal
        const DPoint point = *(cloud_begin + idx);
        const DNormal normal = *(normals_begin + idx);

        if( normal.x != normal.x ){
            *(point_labels+idx) = -1;
            *(connections_sizes+idx) = 0;
            return;
        }

        // Find rotation matrix so that R*normal = (0,0,1)
        float rot_2_local[9];
        calcNormalRotation( normal, rot_2_local );

        *(point_labels+idx) = idx;

        // Go over the neighbors
        const int neighbors_size = *(neighbors_sizes + idx);
        int num_connections = 0;
        for( int i=0; i<neighbors_size; i++ ){

            // Get and check neighbor index
            int nbr_idx = neighbors[ idx*max_neighbors+i ];
            if( nbr_idx == idx ) continue; // no auto-connection

            // Project neighbor point on a plane perpendicular to the point's normal
            DPoint neighbor_unrotated_point, neighbor_point;
            neighbor_unrotated_point.x = cloud_begin[ nbr_idx ].x - point.x;
            neighbor_unrotated_point.y = cloud_begin[ nbr_idx ].y - point.y;
            neighbor_unrotated_point.z = cloud_begin[ nbr_idx ].z - point.z;
            rotateVector( rot_2_local, neighbor_unrotated_point, neighbor_point );

            // Height along the local z axis
            float neighbor_height = neighbor_point.z;

            // Project) normal
            DNormal neighbor_normal;
            rotateVector( rot_2_local, normals_begin[ nbr_idx ], neighbor_normal );

            // Test cooplanarity
            float neighbor_theta = acosf( neighbor_normal.z );
            if( neighbor_theta < THRESH_ANG && neighbor_height < THRESH_HEIGHT ){
                // Create connection
                connections[ idx*max_neighbors + num_connections ] = nbr_idx;
                num_connections++;
            }
        }
        *(connections_sizes+idx) = num_connections;
    }
}

__global__ void avgClusters ( const DPoint *cloud,
                              const DNormal *normals,
                              DPoint *centroids,
                              DNormal *avg_normals,
                              const int *point_labels,
                              const int *cluster_counts,
                              int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int point_label = point_labels[idx];
        if( point_label >= 0 ){

            int count = cluster_counts[point_label];

            DNormal normal = normals[idx];
            if( normal.x != normal.x ) return;
            atomicAdd( &(avg_normals[point_label].x), normal.x / count );
            atomicAdd( &(avg_normals[point_label].y), normal.y / count );
            atomicAdd( &(avg_normals[point_label].z), normal.z / count );

            DPoint point = cloud[idx];
            atomicAdd( &(centroids[point_label].x), point.x / count );
            atomicAdd( &(centroids[point_label].y), point.y / count );
            atomicAdd( &(centroids[point_label].z), point.z / count );
        }
    }
}

__global__ void avgNormals2 ( const DNormal *normals,
                              DNormal *avg_normals,
                              int *nonexp_segment_counts,
                              const int *point_labels,
                              const int *pre_exp_labels,
                              int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int point_label = point_labels[idx];
        int pre_exp_label = pre_exp_labels[idx];
        if( point_label >= 0 && pre_exp_label >= 0 ){

            atomicAdd( nonexp_segment_counts+point_label, 1 );

            DNormal normal = normals[idx];
            if( normal.x != normal.x ) return;
            atomicAdd( &(avg_normals[point_label].x), normal.x );
            atomicAdd( &(avg_normals[point_label].y), normal.y );
            atomicAdd( &(avg_normals[point_label].z), normal.z );
        }
    }
}

__global__ void expandFlatSegments( const DPoint *cloud,
                                    const DNormal *avg_normals,
                                    int *point_labels,
                                    int *cluster_counts,
                                    const int *neighbors,
                                    const int *neighbors_sizes,
                                    int max_neighbors,
                                    int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Get the point
        DPoint point = cloud[idx];
        // Check if point isn't already clustered
        if( point_labels[idx] >= 0 ) return;

        // Loop over neighbors
        const int neighbors_size = *(neighbors_sizes + idx);
        for( int i=0; i<neighbors_size; i++ ){
            // Get and check neighbor index
            int nbr_idx = neighbors[ idx*max_neighbors+i ];
            if( nbr_idx == idx ) continue; // no auto-connection
            // Get and check the neighbor label
            int nbr_label = point_labels[nbr_idx];
            if( nbr_label < 0 ) continue;
            // Get the segment avg_normal
            DPoint normal = avg_normals[nbr_label];
            // Get the neighbor point
            DPoint nbr_point = cloud[nbr_idx];
            // Calculate distance form plane
            float distance = fabs( normal.x*(point.x-nbr_point.x) +
                                   normal.y*(point.y-nbr_point.y) +
                                   normal.z*(point.z-nbr_point.z) );
            if( distance <= EXP_DIST_2_PLANE ){
                point_labels[idx] = nbr_label;
                atomicAdd( cluster_counts+nbr_label, 1 );
                break;
            }
        }
    }
}

__global__ void connectCooplanarSegments( const DPoint *cloud_begin,
                                          const DNormal *avg_normals,
                                          int *point_labels,
                                          int *connections,
                                          int *connections_sizes,
                                          const int *neighbors,
                                          const int *neighbors_sizes,
                                          int max_neighbors,
                                          int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Get the point label and check if it belongs to a cluster
        int point_label = point_labels[idx];
        if( point_label < 0 ) return;

        // Get the point and segment avg normal
        const DPoint point = cloud_begin[idx];
        const DNormal normal = avg_normals[point_label];

        // Find rotation matrix so that R*normal = (0,0,1)
        float rot_2_local[9];
        calcNormalRotation( normal, rot_2_local );

        // Go over the neighbors
        const int neighbors_size = *(neighbors_sizes + idx);
        int num_connections = 0;
        for( int i=0; i<neighbors_size; i++ ){

            // Get and check neighbor index
            int nbr_idx = neighbors[ idx*max_neighbors+i ];
            if( nbr_idx == idx ) continue; // no auto-connection

            // Get and check the neighbor label
            int nbr_label = point_labels[nbr_idx];
            if( nbr_label < 0 ) continue;

            // Project neighbor point on a plane perpendicular to the point's normal
            DPoint neighbor_unrotated_point, neighbor_point;
            neighbor_unrotated_point.x = cloud_begin[ nbr_idx ].x - point.x;
            neighbor_unrotated_point.y = cloud_begin[ nbr_idx ].y - point.y;
            neighbor_unrotated_point.z = cloud_begin[ nbr_idx ].z - point.z;
            rotateVector( rot_2_local, neighbor_unrotated_point, neighbor_point );

            // Height along the local z axis
            float neighbor_height = neighbor_point.z;

            // Project) normal
            DNormal neighbor_normal;
            rotateVector( rot_2_local, avg_normals[ nbr_label ], neighbor_normal );

            // Test cooplanarity
            float neighbor_theta = acosf( neighbor_normal.z );
            if( neighbor_theta < THRESH_ANG && neighbor_height < THRESH_HEIGHT ){
                // Create connection
                connections[ idx*max_neighbors + num_connections ] = nbr_idx;
                num_connections++;
            }
        }
        if( num_connections > 0 )
            connections_sizes[idx] = num_connections;
    }
}

__global__ void convert2FlatCS( const DPoint *cloud,
                                DPoint *centroids,
                                const DNormal *avg_normals,
                                int *point_labels,
                                float2 *flat_points,
                                int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        int point_label = point_labels[idx];
        if( point_label < 0 ) return;

        DPoint centroid = centroids[point_label];
        DPoint normal = avg_normals[point_label];

        // Find rotation matrix so that R*normal = (0,0,1)
        float rot_2_local[9];
        calcNormalRotation( normal, rot_2_local );

        // Calculate the point in the local cs
        DPoint global_point = cloud[idx];
        DPoint unrot_point, rot_point;
        unrot_point.x = global_point.x - centroid.x;
        unrot_point.y = global_point.y - centroid.y;
        unrot_point.z = global_point.z - centroid.z;
        rotateVector( rot_2_local, unrot_point, rot_point );
        float2 point2D;
        point2D.x = rot_point.x;
        point2D.y = rot_point.y;

        // Write to global
        flat_points[idx] = point2D;
    }
}

__global__ void getContour( int *point_labels,
                            float2 *flat_points,
                            int *contours_indices,
                            int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        int point_label = point_labels[idx];
        if( point_label < 0 ) return;

        // Get the flat point
        float2 point2D = flat_points[idx];

        // Calculate contour bin
        float fi = atan2( point2D.y, point2D.x );
        int ctr_bin = (point_label + (fi/F_2_PI+0.5)*0.9999) * CTR_RESOLUTION;

        // Calculate distance from centroid
        float dist_2_centroid = sqrt(point2D.x*point2D.x + point2D.y*point2D.y);

        // Write to contours concurrently
        bool comparing = true;
        while( comparing ){
            // Get the index of the current outermost bin point
            int curr_idx = contours_indices[ctr_bin];
            // Calculate the distance
            float curr_dist;
            if( curr_idx < 0 )
                curr_dist = 0;
            else{
                float2 curr_point = flat_points[curr_idx];
                curr_dist = sqrt(curr_point.x*curr_point.x + curr_point.y*curr_point.y);
            }

            //printf("NAN DETECTED! \n");
            if( dist_2_centroid > curr_dist ){
                int val = atomicCAS( contours_indices+ctr_bin, curr_idx, idx );
                // If write was successful
                if( val == curr_idx )
                    comparing = false;
                // Else wait
            }else{
                comparing = false;
            }
        }
    }
}

FlatSegmentation::FlatSegmentation()
{
    cudaEventCreate( &start );
    cudaEventCreate( &stop );
}

vector<int> FlatSegmentation::allocFeatures( int num_features )
{
    vector<int> ftr_indices;
    for( int i=0; i<num_features; i++ ){
        ftr_indices.push_back( metacloud->segment_indices->countUp() );
    }
    return ftr_indices;
}

void FlatSegmentation::appendNames( vector<string> &names )
{
   names.push_back("Flat segments");
}

void FlatSegmentation::calculate( MetacloudPtr _metacloud )
{
    // Initialization and allocation
    timerStart( start );
    metacloud = _metacloud;
    vector<int> ftr_indices = allocFeatures(1);

    /////////////
    // Flood-fill
    /////////////

    // Set up kernel params
    int N = metacloud->cloud->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Initialize data structures
    DIntArrPtr point_labels = DIntArrPtr( new DeviceArray<int>(metacloud->size()) );
    DeviceArray<int> connections( metacloud->neighbors_indices_2.data.size() );
    DeviceArray<int> connections_sizes( metacloud->size() );
    cudaMemset( connections_sizes, 0, connections_sizes.sizeBytes() );
    int max_neighbors = metacloud->neighbors_indices_2.max_elems;

    // Set seeds and connections
    floodInitialize <<< nBlocks, blockSize >>>( metacloud->cloud->ptr(),
                                                metacloud->normals->ptr(),
                                                point_labels->ptr(),
                                                metacloud->neighbors_indices_2.data.ptr(),
                                                metacloud->neighbors_indices_2.sizes.ptr(),
                                                connections.ptr(),
                                                connections_sizes.ptr(),
                                                max_neighbors,
                                                N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Create arrays for the floodfill results
    DIntArrPtr label_sources;
    DIntArrPtr cluster_counts;

    // Run the floodfill segmentation algorithm
    FloodFill::ffSegmentation( connections,
                               connections_sizes,
                               point_labels,
                               label_sources,
                               cluster_counts,
                               num_clusters,
                               NUM_FLAT_FLOODS,
                               MIN_FLAT_AREA*(1.0/NBR_R_2)*(1.0/NBR_R_2)*metacloud->avg_neighbors_2/F_PI );

    //////////////////////////////// 
    // Expand the resulting segments
    //////////////////////////////// 

    // Average the clusters (centroid and normals)
    DeviceArray<DPoint> centroids( num_clusters );
    cudaMemset( centroids, 0, centroids.sizeBytes() );
    DeviceArray<DNormal> avg_normals( num_clusters );
    cudaMemset( avg_normals, 0, avg_normals.sizeBytes() );    

    avgClusters <<< nBlocks, blockSize >>>( metacloud->cloud->ptr(),
                                            metacloud->normals->ptr(),
                                            centroids.ptr(),
                                            avg_normals.ptr(),
                                            point_labels->ptr(),
                                            cluster_counts->ptr(),
                                            N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Expand the flat segments to include cooplanar neighboring points
    DIntArrPtr pre_exp_labels( new DeviceArray<int>(point_labels->size()) );
    cudaMemcpy( pre_exp_labels->ptr(), point_labels->ptr(), point_labels->sizeBytes(), cudaMemcpyDeviceToDevice );
    for( int i=0; i<NUM_EXP_ITERATIONS; i++ ){
        expandFlatSegments <<< nBlocks, blockSize >>>( metacloud->cloud->ptr(),
                                                       avg_normals.ptr(),
                                                       point_labels->ptr(),
                                                       cluster_counts->ptr(),
                                                       metacloud->neighbors_indices_2.data.ptr(),
                                                       metacloud->neighbors_indices_2.sizes.ptr(),
                                                       max_neighbors,
                                                       N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());
    }

    /////////////////////////////////// 
    // Join adjacent cooplanar segments 
    /////////////////////////////////// 

    // Replace connections: only for cooplanar neighboring segments
    cudaMemset( connections_sizes, 0, connections_sizes.sizeBytes() );
    connectCooplanarSegments<<< nBlocks, blockSize >>>( metacloud->cloud->ptr(),
                                                        avg_normals.ptr(),
                                                        point_labels->ptr(),
                                                        connections.ptr(),
                                                        connections_sizes.ptr(),
                                                        metacloud->neighbors_indices_2.data.ptr(),
                                                        metacloud->neighbors_indices_2.sizes.ptr(),
                                                        max_neighbors,
                                                        N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());


    // Single step floodfill
    FloodFill::ffSegmentation( connections,
                               connections_sizes,
                               point_labels,
                               label_sources,
                               cluster_counts,
                               num_clusters,
                               5,
                               MIN_FLAT_AREA*(1.0/NBR_R_2)*(1.0/NBR_R_2)*metacloud->avg_neighbors_2/F_PI );

    ////////////////////////
    // Flat segment contours  
    ////////////////////////

    // Average the clusters (centroid and normals)
    centroids.create( num_clusters );
    cudaMemset( centroids.ptr(), 0, centroids.sizeBytes() );
    avg_normals.create( num_clusters );
    cudaMemset( avg_normals.ptr(), 0, avg_normals.sizeBytes() );

    avgClusters <<< nBlocks, blockSize >>>( metacloud->cloud->ptr(),
                                            metacloud->normals->ptr(),
                                            centroids.ptr(),
                                            avg_normals.ptr(),
                                            point_labels->ptr(),
                                            cluster_counts->ptr(),
                                            N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Average cluster normals without counting expanded points
    cudaMemset( avg_normals, 0, avg_normals.sizeBytes() );
    DeviceArray<int> nonexp_segment_counts( num_clusters );
    cudaMemset( nonexp_segment_counts, 0, nonexp_segment_counts.sizeBytes() );

    avgNormals2 <<< nBlocks, blockSize >>>( metacloud->normals->ptr(),
                 avg_normals.ptr(),
                 nonexp_segment_counts.ptr(),
                 point_labels->ptr(),
                 pre_exp_labels->ptr(),
                 N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    vector<float4> h_avg_normals;
    vector<int> h_nonexp_segment_counts;
    avg_normals.download( h_avg_normals );
    nonexp_segment_counts.download( h_nonexp_segment_counts );
    vector<DPoint> h_centroids;
    centroids.download( h_centroids );
    for( int i=0; i<num_clusters; i++ ){
        h_avg_normals[i].x /= h_nonexp_segment_counts[i];
        h_avg_normals[i].y /= h_nonexp_segment_counts[i];
        h_avg_normals[i].z /= h_nonexp_segment_counts[i];
    }
    avg_normals.upload( h_avg_normals );

    // Convert point cloud to flat coordinate systems
    DeviceArray<float2> d_flat_points( metacloud->size() );
    cudaMemset( d_flat_points, 0, d_flat_points.sizeBytes() );

    convert2FlatCS <<< nBlocks, blockSize >>>( metacloud->cloud->ptr(),
                                               centroids.ptr(),
                                               avg_normals.ptr(),
                                               point_labels->ptr(),
                                               d_flat_points.ptr(),
                                               N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Check if there are any flat segments
    if( num_clusters > 0 ){
        // Get the countour indices
        DeviceArray<int> d_ctr_indices( num_clusters*CTR_RESOLUTION );
        // Set up kernel params
        N = d_ctr_indices.size();
        blockSize = 512;
        nBlocks = N/blockSize + (N%blockSize == 0?0:1);

        FloodFill::cudaSetIntsTo( d_ctr_indices, -1 );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());

        // Set up kernel params
        N = metacloud->cloud->size();
        blockSize = 512;
        nBlocks = N/blockSize + (N%blockSize == 0?0:1);

        getContour <<< nBlocks, blockSize >>>( point_labels->ptr(),
                                               d_flat_points.ptr(),
                                               d_ctr_indices.ptr(),
                                               N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());

        // Download the flat point cloud and the contour indices for features calculation
        d_ctr_indices.download( h_ctr_indices );
        d_flat_points.download( h_flat_points );
    }

    //////////////////////////
    // Download results to CPU 
    //////////////////////////

    // Save the point labels
    cudaMemcpy( metacloud->segment_indices->getFeaturePtr(ftr_indices[0]),
                point_labels->ptr(),
                point_labels->sizeBytes(),
                cudaMemcpyDeviceToDevice );

    // Append vector of segmented features for the segment set
    vector<Metacloud::SegmentedFloatArrPtr> new_seg_set;
    metacloud->segmented_features.push_back( new_seg_set );

    // Save the cluster counts
    vector<int> h_int_counts;
    cluster_counts->download( h_int_counts );
    Metacloud::SegmentedFloatArrPtr seg_ftr( new Metacloud::SegmentedFloatArr );
    seg_ftr->name = "Flat cluster counts";
    seg_ftr->values.resize( cluster_counts->size() );
    for( int i=0; i<cluster_counts->size(); i++ )
        seg_ftr->values[i] = h_int_counts[i];
    metacloud->segmented_features.back().push_back( seg_ftr );

    cout << "Flat segmentation performed in " << fixed << timerStop( start, stop ) << " ms." << endl;
    cout << "Found " << num_clusters << " flat clusters" << endl;
}

}
