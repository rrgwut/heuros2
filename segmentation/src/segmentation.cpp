#include <segmentation/segmentation.h>
#include <boost/chrono.hpp>


using namespace std;


namespace heuros
{

#define HISTORY_MAX_LENGTH 10

Segmentation::Segmentation()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_all );

    flat_segmentation.appendNames( feature_names );
    nonflat_segmentation.appendNames( feature_names );
}

Segmentation::~Segmentation()
{
    cout << "[Heuros::Segmentation] Destructor" << endl;
    cudaEventDestroy(start_all);
    cudaEventDestroy(stop_all);
}

void Segmentation::process( const MetacloudPtr &_metacloud )
{
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();

    resetSelection();
    metacloud = _metacloud;
    assert(metacloud->cloud );
    const int num_segment_sets = 2;
    metacloud->segment_indices->create( num_segment_sets, metacloud->size());

    // Flat areas segmentation
    flat_segmentation.calculate(metacloud);
    flatDimensionsCPU();

    // Non-flat clusters
    nonflat_segmentation.calculate(metacloud);

    // Download indices to host
    //downloaDFloatArrs();

    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
    cout << "\033[31m" << "Segmentation: Processing time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
}

void Segmentation::flatDimensionsCPU()
{
    // Cleare the results vector
    Metacloud::SegmentedFloatArrPtr seg_side_a( new Metacloud::SegmentedFloatArr );
    Metacloud::SegmentedFloatArrPtr seg_side_b( new Metacloud::SegmentedFloatArr );
    seg_side_a->name = "Side A";
    seg_side_b->name = "Side B";

    // Loop ver each contour
    for( int i=0; i<flat_segmentation.num_clusters; i++ ){

        // Create the compacted contour vector
        vector<cv::Point2f> contour;
        for( int j=0; j<CTR_RESOLUTION; j++ ){
            int point_index = flat_segmentation.h_ctr_indices[ i*CTR_RESOLUTION + j ];
            //cout << "I: " << i << " :: J: " << j << " :: x: " << h_flat_points[ point_index ].x << endl;
            if( point_index > 0 ){
                float2 _point = flat_segmentation.h_flat_points[ point_index ];
                cv::Point2f point( _point.x, _point.y );
                contour.push_back( point );
            }
        }

        // Measure sides of the min area enclosing rect
        float side_a, side_b;
        if( contour.size() < 3 ){
            side_a = F_NAN; side_b = F_NAN;
        }else{
            cv::RotatedRect rect;
            rect = cv::minAreaRect( contour );
            side_a = max( rect.size.width, rect.size.height );
            side_b = min( rect.size.width, rect.size.height );
        }
        seg_side_a->values.push_back( side_a );
        seg_side_b->values.push_back( side_b );
    }

   metacloud->segmented_features.back().push_back(seg_side_a);
   metacloud->segmented_features.back().push_back(seg_side_b);
}

void Segmentation::updateSelection( int seed_idx, int seg_set_idx, bool multiple )
{
    // Copy and save previous cluster to history
    if( selection_history.size() > HISTORY_MAX_LENGTH )
        selection_history.erase( selection_history.begin() );
    AnnotationPtr prev_annotation( new Annotation );
    if( selected_annotation )
        *prev_annotation = *selected_annotation;
    selection_history.push_back( prev_annotation );

    // Create the cluster
    if( !selected_annotation || !multiple )
        resetSelection();

    // Single point case
    if( seg_set_idx < 0 ){
        selected_annotation->push_back(seed_idx);
        return;
    }

    // Cluster case
    IndicesPtr point_labels;
    metacloud->segment_indices->downloadFeature( point_labels, seg_set_idx );

    // Get the selected point's label
    int selected_label = point_labels->at(seed_idx);
    if( selected_label == -1 ){
        cout << "ERROR: The picked point does not belong to a valid segment" << endl;
        return;
    }

    // Add to the combined selected cluster
    for( int i=0; i<point_labels->size(); i++ ){
        if( point_labels->at(i) == selected_label )
            selected_annotation->push_back(i);
    }
}

void Segmentation::resetSelection()
{
    // Copy and save previous cluster to history
    if( selection_history.size() > HISTORY_MAX_LENGTH )
        selection_history.erase( selection_history.begin() );
    AnnotationPtr prev_annotation( new Annotation );
    if( selected_annotation )
        *prev_annotation = *selected_annotation;
    selection_history.push_back( prev_annotation );

    // Reset
    selected_annotation = AnnotationPtr( new Annotation() );
}

void Segmentation::undoSelection()
{
    if( selection_history.size() == 0 ) return;
    selected_annotation = selection_history.back();
    selection_history.pop_back();
}

AnnotationPtr Segmentation::getSelectedAnnotation()
{
    return selected_annotation;
}

void Segmentation::setSelection( const AnnotationPtr &selection )
{
    selected_annotation = selection;
}

vector<string> Segmentation::getFeatureNames()
{
    return feature_names;
}

}
