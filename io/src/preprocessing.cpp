#include <io/preprocessing.h>
#include <pcl/gpu/features/features.hpp>
#include <pcl/io/pcd_io.h>
#include <core/impl/knowledge_rw.hpp>
#include <core/utils_gpu.h>


using namespace std;
using namespace pcl;
using namespace pcl::gpu;


namespace heuros
{

#define WIDE_NBRS_THRESH 50

Preprocessing::Preprocessing()
{
}

void Preprocessing::createMetacloud( const PointCloud<PointXYZRGB>::Ptr &h_in_cloud )
{
    // Upload to device
    DPointCloudXYZRGBPtr d_in_cloud( new DPointCloudXYZRGB(h_in_cloud->size()) );
    d_in_cloud->upload( h_in_cloud->points.data(), h_in_cloud->size() );

    // Voxel grid
    //cout << "Input points: " << d_in_cloud->size() << endl;
    //DPointCloudXYZRGBPtr d_downsampled = UtilsGPU::voxelGridFilter( d_in_cloud, 0.01 );
    //cout << "Downsampled points: " << d_downsampled << endl;

    // Split points and colors
    metacloud->cloud->create( d_in_cloud->size() );
    metacloud->colors->create( d_in_cloud->size() );
    UtilsGPU::splitPointColor( *d_in_cloud, *metacloud->cloud, *metacloud->colors );

    // Calc neighborhoods
    calculateNeighborhoods();
}

float Preprocessing::calcAvgNeighbors( NeighborIndices neighbors ) const
{
    int q_step = 10;

    vector<int> sizes;
    neighbors.sizes.download(sizes);

    int sum = 0;
    for( int i=0; i<metacloud->size(); i+= q_step ){
        sum += sizes[i];
    }
    return float(sum)/(metacloud->size()/q_step);
}

void Preprocessing::calculateNeighborhoods()
{
    // Create structures
    metacloud->octree = gpu::Octree::Ptr( new gpu::Octree );
    gpu::Octree::PointCloud &octree_cloud = (gpu::Octree::PointCloud&)(*metacloud->cloud);
    metacloud->octree->setCloud( octree_cloud );
    vector<PointXYZ> h_cloud( octree_cloud.size() );
    octree_cloud.download(( h_cloud.data() ));
    for( int i=0; i<100; i++ )
    {
        float x =  h_cloud[i].x;
        float y =  h_cloud[i].y;
        float z =  h_cloud[i].z;
        if( x != x || y != y || z != z || fabs(x) > 99 || fabs(y) > 99 || fabs(z) > 99 )
            printf("%f :: %f :: %f\n", x, y, z );
    }

    metacloud->octree->build();

    // Perform search 1
    metacloud->neighbors_indices.create( octree_cloud.size(), MAX_NEIGHBORS );
    metacloud->octree->radiusSearch( octree_cloud, NBR_R_1, MAX_NEIGHBORS, metacloud->neighbors_indices ); // for 1 cm radius up to 30 neighbors
    metacloud->avg_neighbors = calcAvgNeighbors( metacloud->neighbors_indices );
    cout << "Calculated r=" << NBR_R_1 << " neighborhoods. Avg nbrs: " << metacloud->avg_neighbors << endl;

    // Perform second search
    if( metacloud->avg_neighbors < WIDE_NBRS_THRESH ){
        metacloud->neighbors_indices_2.create( octree_cloud.size(), MAX_NEIGHBORS );
        metacloud->octree->radiusSearch( octree_cloud, NBR_R_2, MAX_NEIGHBORS, metacloud->neighbors_indices_2 );
        metacloud->avg_neighbors_2 = calcAvgNeighbors( metacloud->neighbors_indices_2 );
        cout << "Calculated r=" << NBR_R_2 << " neighborhoods. Avg nbrs: " << metacloud->avg_neighbors_2 << endl;
    }else{
        metacloud->neighbors_indices_2 = metacloud->neighbors_indices;
        metacloud->avg_neighbors_2 = metacloud->avg_neighbors;
    }
}

void Preprocessing::process( const PointCloud<PointXYZRGB>::Ptr &point_cloud,
                             const MetacloudPtr &_metacloud )
{
    metacloud = _metacloud;
    createMetacloud( point_cloud );

    // Get sensor rotation matrix
    Eigen::Matrix3f rotation_matrix = point_cloud->sensor_orientation_.toRotationMatrix();
    metacloud->sensor_orientation->upload( rotation_matrix.data(), 9 );

    // Calc and save normals
    gpu::NormalEstimation::PointCloud &cloud = (NormalEstimation::PointCloud&)(*metacloud->cloud);
    gpu::NormalEstimation::Normals normals;
    gpu::NormalEstimation::computeNormals( cloud, metacloud->neighbors_indices, normals );
    gpu::NormalEstimation::flipNormalTowardsViewpoint(cloud, 0.f, 0.f, 0.f, normals);
    DPointCloud &tmp_normals = (DPointCloud&)(normals);
    tmp_normals.copyTo( *metacloud->normals );
}

bool Preprocessing::fileExists( string file_name_abs )
{
    ifstream infile( file_name_abs.c_str() );
    return infile.good();
}

}
