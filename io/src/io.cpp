// TODO
// Reactivate detection

#include <io/io.h>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <boost/filesystem.hpp>
#include <boost/chrono.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <yaml-cpp/yaml.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/voxel_grid_label.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <Eigen/Geometry>


using namespace cv;
using namespace std;
using namespace pcl;
using namespace sensor_msgs;
namespace fs = boost::filesystem;


namespace heuros
{

#define VG_LEAF_SIZE 0.005
#define MAX_METRIC_Z 8.0 // MAX_X = MAX_Z * tg(alfa/2)
     
IO::IO()
{
    // Variables initialization
    capturing = true;
    online = true;

    ros::NodeHandle n;

    // Subscribers
    sub_sensor = n.subscribe( "/kinect2/depth_lowres/points", 1, &IO::sensorCb, this );
}

IO::~IO()
{
    cout << "[Heuros::IO] Destructor" << endl;
}

void IO::start()
{
    capturing = true;
}

void IO::stop()
{
    capturing = false;
}

void IO::reprocessIfOffline()
{
    //if( !online )
    //    process( input_msg );
}

void IO::saveScene( string file_path ){}

void IO::saveCluster( string dir_path, string obj_class ){}

void IO::saveClusterAsCloud()
{
    //Extract point cloud
    AnnotationPtr annotationPtr = segmentation->getSelectedAnnotation();
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr scene_cloud = Conversions::downloadXYZRGB(metacloud);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cluster_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    for (int i=0; i<annotationPtr->indices.size(); i++)
    {
        pcl::PointXYZRGB point = scene_cloud->at(annotationPtr->indices.at(i));
        cluster_cloud->push_back(point);
    }

    // Write the file
    string path = ros::package::getPath("heuros") + "/data/scenes/subclouds/" + curr_scene_name;
    pcl::io::savePCDFile(path, *cluster_cloud);
}

bool IO::selectAnnotation( int annot_idx )
{
    segmentation->setSelection( metacloud->annotations[annot_idx] );
    visualization->waitForSceneProc();
    visualization->displayCluster();
}

void IO::sensorCb( const sensor_msgs::PointCloud2ConstPtr &msg ){}

bool IO::loadVector( const string &file_path, vector<double> &out_vec )
{
    ifstream file( file_path.c_str() );
    if( !file.is_open() ) return false;
    while( !file.eof() ){
        double val;
        file >> val;
        out_vec.push_back(val);
    }
    return true;
}

bool IO::loadAnnotations( const string &file_path,
                          const PointCloud<PointXYZRGB>::ConstPtr &cloud,
                          const vector<double> &intrinsics )
{
    ifstream in_file( file_path.c_str(), std::ios_base::in | std::ios_base::binary );
    if( !in_file.is_open() ) return false;

    // Read structure
    vector<AnnotationPtr> img_annotations;
    { // Use scope to ensure archive and filtering stream buffer go out of scope before stream
        boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
        in.push(boost::iostreams::zlib_decompressor());
        in.push(in_file);
        boost::archive::binary_iarchive ia(in);
        ia >> img_annotations;
    }
    in_file.close();

    // Reproject cloud to get img indices
    double cx = intrinsics[2];
    double cy = intrinsics[5];
    double fx = intrinsics[0];
    double fy = intrinsics[4];
    vector<int> reproj_indices( cloud->size() );
    for( int i=0; i<cloud->size(); i++ ){
        const PointXYZRGB &pt = cloud->points[i];
        int x = pt.x*fx/pt.z + cx;
        int y = pt.y*fy/pt.z + cy;
        reproj_indices[i] = y*metacloud->color_img.cols + x;
    }

    // Create pointcloud-indexed annotations
    vector<AnnotationPtr> cloud_annotations( img_annotations.size() );
    for( int i=0; i<img_annotations.size(); i++ ){
        Mat annot_mask = Mat::zeros( metacloud->color_img.rows, metacloud->color_img.cols, CV_8UC1 );
        AnnotationPtr &img_annot = img_annotations[i];
        for( int j=0; j<img_annot->indices.size(); j++ )
            annot_mask.at<uchar>(img_annot->indices[j]) = 255;
        AnnotationPtr &cloud_annot = cloud_annotations[i];
        cloud_annot = AnnotationPtr( new Annotation );
        cloud_annot->name = img_annotations[i]->name;
        for( int j=0; j<cloud->size(); j++ ){
            if( annot_mask.at<uchar>(reproj_indices[j]))
                cloud_annot->indices.push_back(j);
        }
    }

    cout << "LOADING ANNO..." << endl;
    cout << cloud->size() << endl;

    metacloud->annotations = cloud_annotations;

    return true;
}

bool IO::loadScene( string path )
{
    cout << "Loading scene: " << path << endl;
    if( !fs::exists(path) ) return false;
    online = false;

    // Create the metacloud
    metacloud = MetacloudPtr( new Metacloud );

    //=============== SUN input =================
    if (fs::exists(path + "/info.yaml"))
    {
        // Input paths
        string depth_path = path + "/depth.png";
        string color_path = path + "/color.jpg";
        string intrinsics_path = path + "/intrinsics.txt";
        string extrinsics_path = path + "/extrinsics.txt";
        string annotation_path = path + "/annotation.dat";
        string info_path = path + "/info.yaml";

        // Read images
        metacloud->depth_img = imread( depth_path, CV_LOAD_IMAGE_ANYDEPTH );
        metacloud->color_img = imread( color_path );

        // Read intrinsics and extrinsics
        vector<double> intrinsics, extrinsics;
        if( !loadVector( intrinsics_path, intrinsics ) ){
            cout << "ERROR: Couldn't read intrinsics from file" << endl;
            assert(false);
        }
        loadVector( extrinsics_path, extrinsics );

        // Concatenate and filter pointcloud
        PointCloud<PointXYZRGB>::Ptr cloud_unfiltered =
                buildPointCloud( metacloud->depth_img, metacloud->color_img,
                             intrinsics, extrinsics, true );
        PointCloud<PointXYZRGB>::Ptr cloud_filtered =
                filterPointCloud( cloud_unfiltered );

        // Load annotations
        loadAnnotations( annotation_path, cloud_filtered, intrinsics );

        // Load scene name
        if( boost::filesystem::exists(info_path) ){
            YAML::Node node = YAML::LoadFile( info_path );
            metacloud->scene_name = node["scene"].as<string>();
        }

        //string out_file_path = path + "/cloud.pcd";
        //io::savePCDFile( out_file_path, *pointcloud, true );
        process( cloud_filtered );

    //=============== PCD input =================
    } else
    {
        // Load the scene
        curr_scene_name = fs::path(path).filename().string();
        PointCloud<PointXYZRGB>::Ptr cloud(new PointCloud<PointXYZRGB>);
        pcl::io::loadPCDFile(path.c_str(), *cloud);
        cout << "GO TO THE PROCESS" << endl;
        process(cloud);
    }

    return true;
}

bool IO::loadModel(string path)
{
    cout << "Loading model: " << path << endl;
    if (!fs::exists(path)) return false;

    // Create the metacloud
    model_metacloud = MetacloudPtr(new Metacloud);

    //=============== PCD input =================
    if (!fs::exists(path + "/info.yaml"))
    {
        // Load the model
        model_metacloud->scene_name = fs::path(path).filename().string();
        PointCloud<PointXYZRGB>::Ptr cloud(new PointCloud<PointXYZRGB>);
        pcl::io::loadPCDFile(path.c_str(), *cloud);

        // Not enough points
        if(cloud->size() < 100) return false;

        // Upload to GPU, calculate neighborhoods and normals
        boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();
        Preprocessing model_preprocessing;
        model_preprocessing.process(cloud, model_metacloud);
        boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
        cout << "\033[31m" << "IO: Processing model time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
    }

    return true;
}
bool IO::registerModel()
{
    // Assert
    if (!metacloud)
    {
        cout << "\x1B[32m" << "[Heuros::IO] There is no target point cloud to register to." << "\x1B[0m" << endl;
        return false;
    } else if (!model_metacloud)
    {
        cout << "\x1B[32m" << "[Heuros::IO] There is no model point cloud to register." << "\x1B[0m" << endl;
        return false;
    }

    // Register
    agent->processRegistration(metacloud, model_metacloud);
    return true;
}

PointCloud<PointXYZRGB>::Ptr
IO::buildPointCloud( const Mat& depth,
                     const Mat& color,
                     const vector<double>& intrinsics,
                     const vector<double>& extrinsics,
                     bool shift )
{
    int w = color.cols;
    int h = color.rows;
    double cx = intrinsics[2];
    double cy = intrinsics[5];
    double fx_inv = 1.0 / intrinsics[0];
    double fy_inv = 1.0 / intrinsics[4];

    // Create output cloud
    PointCloud<PointXYZRGB>::Ptr out_cloud( new PointCloud<PointXYZRGB> );
    out_cloud->resize(w*h);
    out_cloud->width = w;
    out_cloud->height = h;
    out_cloud->is_dense = false;
    out_cloud->sensor_origin_.setZero();

    // Set sensor rotation
    if( extrinsics.size() == 0 ){
        out_cloud->sensor_orientation_.setIdentity();
    }else{
        Eigen::Matrix3f R;
        R << extrinsics[0], extrinsics[1], extrinsics[2],
             extrinsics[4], extrinsics[5], extrinsics[6],
             extrinsics[8], extrinsics[9], extrinsics[10];
        out_cloud->sensor_orientation_ = R;
    }

    // Calculate metric coordinates
    for( int u = 0; u < w; ++u )
    {
        for( int v = 0; v < h; ++v ){
            uint16_t z = depth.at<uint16_t>(v, u);
            if( shift )
                z = (z >> 3) | (z << 13);
            const cv::Vec3b& c = color.at<cv::Vec3b>(v, u);

            PointXYZRGB& pt = out_cloud->points[v*w + u];

            if( z != 0 ){
                double z_metric = z * 0.001;
                pt.x = z_metric * ((u - cx) * fx_inv);
                pt.y = z_metric * ((v - cy) * fy_inv);
                pt.z = z_metric;
                pt.r = c[2];
                pt.g = c[1];
                pt.b = c[0];
            }
            else{
                pt.x = pt.y = pt.z = std::numeric_limits<float>::quiet_NaN();
            }
        }
    }
    return out_cloud;
}

PointCloud<PointXYZRGB>::Ptr
IO::filterPointCloud( const PointCloud<PointXYZRGB>::Ptr &in_cloud )
{
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();

    cout << "IO: Unfiltered cloud points: " << in_cloud->size() << endl;

    // Cuboid filter
    PointCloud<PointXYZRGB>::Ptr tmp_cloud( new PointCloud<PointXYZRGB> );
    tmp_cloud->points.reserve( in_cloud->size() );
    tmp_cloud->header = in_cloud->header;
    tmp_cloud->sensor_origin_ = in_cloud->sensor_origin_;
    tmp_cloud->sensor_orientation_ = in_cloud->sensor_orientation_;
    tmp_cloud->height = 1;
    tmp_cloud->is_dense = true;
    for( int i=0; i<in_cloud->size(); i++ ){
        PointXYZRGB &pt = in_cloud->points[i];
        if( pt.z > MAX_METRIC_Z )
            continue;
        if ( !pcl_isfinite(pt.x) || !pcl_isfinite(pt.y) || !pcl_isfinite(pt.z) )
            continue;
        tmp_cloud->push_back(pt);
    }

    // Voxel grid
    PointCloud<PointXYZRGB>::Ptr out_cloud( new PointCloud<PointXYZRGB> );
    VoxelGrid<PointXYZRGB> vg;
    vg.setInputCloud(tmp_cloud);
    vg.setLeafSize( VG_LEAF_SIZE, VG_LEAF_SIZE, VG_LEAF_SIZE );
    vg.filter(*out_cloud);
    cout << "IO: Filtered cloud points: " << out_cloud->size() << endl;

    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
    cout << "\033[31m" << "IO: FIltering time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;

    return out_cloud;
}

void IO::process( const PointCloud<PointXYZRGB>::Ptr &in_cloud )
{
    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();

    //in_cloud = in_cloud;
    //Eigen::Affine3f transform;
    //transform = in_cloud->sensor_orientation_;
    //pcl::transformPointCloud ( *in_cloud, *in_cloud, transform );

    if( in_cloud->size() < 100 ) return;

    // Upload to GPU, calculate neighborhoods and normals
    preprocessing.process( in_cloud, metacloud );

    // Get elapsed time
    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
    cout << "\033[31m" << "IO: Processing time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;

    // Send scene to agent
    agent->process( metacloud );

    // Measure the full scene loop duration
    boost::chrono::high_resolution_clock::time_point loop_time = boost::chrono::high_resolution_clock::now();
    static boost::chrono::high_resolution_clock::time_point prev_loop;
    static bool initialized = false;
    if( initialized ){
        boost::chrono::nanoseconds full = loop_time - prev_loop;
        cout << "\033[31m" << "FULL LOOP: " << full.count()/1000000 << " ms" << "\033[0m" << endl;
    }
    prev_loop = loop_time;
    initialized = true;
}

}
