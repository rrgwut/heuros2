#ifndef PREPROCESSING_H
#define PREPROCESSING_H


#include <iostream>
#include <boost/algorithm/string.hpp>
#include <cuda_runtime.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <core/metacloud.h>


namespace heuros
{

typedef pcl::gpu::DeviceArray<pcl::PointXYZRGB> DPointCloudXYZRGB;
typedef boost::shared_ptr<DPointCloudXYZRGB> DPointCloudXYZRGBPtr;

class Preprocessing
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    Preprocessing();

    /** \brief Process sensor input */
    void process( const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &point_cloud,
                  const MetacloudPtr &_metacloud );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud pointer */
    MetacloudPtr metacloud;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Create the scene Metacloud object used by other modules */
    void createMetacloud( const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &h_in_cloud );

    /** \brief Check whether file exists */
    static bool fileExists(std::string file_name_abs );

    /** \brief Calculate the point neighborhoods */
    void calculateNeighborhoods();

    /** \brief Calculate the average number of neighbors */
    float calcAvgNeighbors( pcl::gpu::NeighborIndices neighbors ) const;
};

}

#endif
