#ifndef IO_H
#define IO_H


#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Imu.h>
#include <core/metacloud.h>
#include <agent/agent.h>
#include <segmentation/segmentation.h>
#include <visualization/visualization.h>
#include <io/preprocessing.h>


namespace heuros
{

class Agent;
class Segmentation;
class Visualization;

class IO
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Kinfu cloud subscriber */
    ros::Subscriber sub_sensor;

    /** \brief Pointers to the other modules instances */
    // @{
    Agent *agent;
    Segmentation *segmentation;
    Visualization *visualization;
    // }@
    
    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    IO();
    ~IO();

    /** \brief Start capturing */
    void start();

    /** \brief Stop capturing */
    void stop();

    /** \brief Reprocess the scene if it was obtained offline */
    void reprocessIfOffline();

    /** \brief Save XYZRGB point cloud */
    void saveScene( std::string file_path );

    /** \brief Load point cloud data from a file */
    bool loadScene( std::string path );

    /** \brief  */


    /**
     * @brief loadModel             Load point cloud data of the model to register
     *                              from a file.
     * @param[in] path              Path to the model cloud (only pcd so far).
     * @return                      True if succeed.
     */
    bool loadModel(std::string path);
    bool registerModel();


    pcl::PointCloud<pcl::PointXYZRGB>::Ptr
    buildPointCloud( const cv::Mat& depth,
                     const cv::Mat& color,
                     const std::vector<double>& intrinsics,
                     const std::vector<double>& extrinsics,
                     bool shift=true );

    /** \brief Save cluster indices to file */
    //void saveCluster( std::string cloud_name, const IndicesConstPtr &indices );
    void saveCluster( std::string dir_path, std::string obj_class );

    /**
     * @brief saveClusterAsCloud            Save current cluster as a standalone subcloud with the
     *                                      same name as current cloud, but in the scenes/subclouds
     *                                      directory.
     */
    void saveClusterAsCloud();

    /** \brief Select scene object annotation */
    bool selectAnnotation( int annot_idx );

    /** \brief Load a double precission std::vector from file */
    bool loadVector( const std::string &file_path, std::vector<double> &out_vec );

    /** \brief Load scene object annotations from file */
    bool loadAnnotations( const std::string &file_path,
                          const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud,
                          const std::vector<double> &intrinsics );
protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud pointer */
    MetacloudPtr metacloud;

    /** \brief Metacloud pointer */
    MetacloudPtr model_metacloud;
    
    /** \brief Input ros msg (full scene) */
    sensor_msgs::PointCloud2::ConstPtr input_msg;

    /** \brief Stable cluster pointer */
    IndicesPtr cluster;

    /** \brief Whether capturing */
    volatile bool capturing;

    /** \brief Whether the last scene was obtained online */
    volatile bool online;

    /** \brief Currently loaded scene path (offline) */
    std::string curr_scene_name;

    /** \brief Preprocessing algorithms object */
    Preprocessing preprocessing;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Sensor cloud received callback */
    void sensorCb( const sensor_msgs::PointCloud2ConstPtr &msg );

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr
    filterPointCloud( const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &in_cloud );

    void process( const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &in_cloud );
};

}

#endif
