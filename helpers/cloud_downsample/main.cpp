#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>


using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

int main(int argc, char* argv[])
{
    //===============================================================================
    //============================== PROGRAM OPTIONS ================================
    //===============================================================================

    po::options_description desc("Options");
    desc.add_options()
            ("help,h", "Print help.")
            ("input,i", po::value<string>()->required(),"Input file or directory")
            ("output,o", po::value<string>()->required(),"Output file or directory")
            ("leaf,l", po::value<double>()->required(),"Leaf size for filtering (meters)");

    po::variables_map vm;
    try{
        po::store(po::parse_command_line(argc, argv, desc), vm);
        if ( vm.count("help")  )
        {
            cout << endl;
            cout << "======================================================" << endl;
            cout << "============== cloud downsample program ==============" << endl;
            cout << "======================================================" << endl;
            cout << endl << desc << endl;
            return 0;
        }
        po::notify(vm);
    }
    catch( po::error& e )
    {
        cerr << endl << "ERROR: " << e.what() << endl << endl;
        cerr << desc << endl;
        return -1;
    }
    string input = vm["input"].as<std::string>();
    string output = vm["output"].as<std::string>();
    double leaf = vm["leaf"].as<double>();

    //===============================================================================
    //================================ FILE TO FILE =================================
    //===============================================================================

    fs::path input_path(input), output_path(output);
    if (!fs::exists(input_path))
    {
        cerr << "ERROR: input file does not exist" << endl;
        return -1;
    }

    if (fs::is_regular_file(input_path))
    {
        if (fs::is_regular_file(output_path))
        {
            cerr << "ERROR: output file already exists" << endl;
            return -1;
        }

        if (fs::is_directory(output_path))
        {
            cerr << "ERROR: wrong usage, see --help for more details" << endl;
            return -1;
        }

        // Clouds
        pcl::PCLPointCloud2::Ptr cloud (new pcl::PCLPointCloud2 ());
        pcl::PCLPointCloud2::Ptr cloud_filtered (new pcl::PCLPointCloud2 ());

        // Fill in the cloud data
        pcl::PCDReader reader;
        reader.read(input, *cloud);

        std::cout << "PointCloud before filtering: " << cloud->width * cloud->height
             << " data points (" << pcl::getFieldsList (*cloud) << ")." << endl;

        // Create the filtering object
        pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
        sor.setInputCloud(cloud);
        sor.setLeafSize(leaf, leaf, leaf);
        sor.filter(*cloud_filtered);

        std::cerr << "PointCloud after filtering: " << cloud_filtered->width * cloud_filtered->height
             << " data points (" << pcl::getFieldsList (*cloud_filtered) << ")." << endl;

        pcl::PCDWriter writer;
        writer.write (output, *cloud_filtered, Eigen::Vector4f::Zero (), Eigen::Quaternionf::Identity (), false);

    }

    return 0;
}
