#include <core/impl/manipulations.cuh>

namespace heuros
{


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// UTILS
////////////////////////////////////////////////////////////////////

__global__ void proMemcpy( unsigned char *dst, int dpitch, int doffset,
                                           const unsigned char *src, int spitch, int soffset,
                                           int width, int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        int y = idx/width;
        int x = idx%width;
        dst[ y*dpitch + doffset + x ] = src[ y*spitch + soffset + x ];
    }
}

__global__ void addColorKernel( DPoint *dst,
                                                const DPoint *src_cloud,
                                                const float *src_color,
                                                int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        unsigned int rgb = *(int*)(src_color+idx);

        int R = (rgb >> 16) & 0x0000ff;
        int G = (rgb >> 8)  & 0x0000ff;
        int B = (rgb) & 0x0000ff;

        int r = max(1, min(255, R));
        int g = max(1, min(255, G));
        int b = max(1, min(255, B));
        float color = (b << 16) + (g << 8) + r;

        DPoint point = src_cloud[idx];
        point.w = color;
        dst[idx] = point;
    }
}

__global__ void splitXYZRGB( const pcl::PointXYZRGB *xyzrgb,
                                             DPoint *xyz,
                                             float *rgb,
                                             int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        pcl::PointXYZRGB in_point = xyzrgb[idx];
        DPoint xyz_out;
        xyz_out.x = in_point.x;
        xyz_out.y = in_point.y;
        xyz_out.z = in_point.z;
        xyz[idx] = xyz_out;
        rgb[idx] = in_point.rgb;
    }
}

__global__ void mergeXYZRGB( pcl::PointXYZRGB *xyzrgb,
                                             const DPoint *xyz,
                                             const float *rgb,
                                             int N,
                                             pcl::PointXYZRGB out_point)
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        DPoint xyz_in = xyz[idx];
        //float rgb_in = rgb[idx];
        out_point.x = xyz_in.x;
        out_point.y = xyz_in.y;
        out_point.z = xyz_in.z;
        out_point.rgb = rgb[idx];
        xyzrgb[idx] = out_point;
    }
}

__global__ void setIntsTo( int *data,
                                           int value,
                                           int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        data[idx] = value;
    }
}

__global__ void copySelected( const float *in_data,
                                              float *out_data,
                                              const int *cpy_indices,
                                              int hist_size,
                                              int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        int hist_idx = idx / hist_size;
        int src_hist = cpy_indices[hist_idx];
        int src_idx = src_hist*hist_size + idx%hist_size;
        out_data[idx] = in_data[src_idx];
    }
}

//////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// GENERAL
//////////////////////////////////////////////////////////////////////

__global__ void fetchClusters( const int *neighbors,
                                               int *cluster_sizes,
                                               int *start_indices,
                                               int *point_indices,
                                               int *cluster_indices,
                                               int *new_2_old,
                                               int max_neighbors,
                                               int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if( idx < N ){
        int cluster_idx = idx / max_neighbors;
        int old_cluster_idx = new_2_old[cluster_idx];
        int in_cluster_idx = idx % max_neighbors;

        if( in_cluster_idx < cluster_sizes[cluster_idx] ){
            int cluster_start = start_indices[cluster_idx];
            int pos = cluster_start + in_cluster_idx;
            int nbrs_pos = old_cluster_idx*max_neighbors + in_cluster_idx;
            point_indices[pos] = neighbors[nbrs_pos];
            cluster_indices[pos] = cluster_idx;
        }
    }
}

__global__ void compactCentroids( const float *sums,
                                                  const int *counts,
                                                  DPoint *out_cloud,
                                                  int *global_idx,
                                                  int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        int num_points = counts[idx];

        if( num_points > 0 ){
            int out_idx = atomicAdd( global_idx, 1 ); // Atomic add of all non-empty voxels
            DPoint point;
            point.x = sums[3*idx+0] / num_points;
            point.y = sums[3*idx+1] / num_points;
            point.z = sums[3*idx+2] / num_points;
            out_cloud[out_idx] = point;
        }
    }
}

__global__ void compactCentroids( const float *sums,
                                                  const int *counts,
                                                  pcl::PointXYZRGB *out_cloud,
                                                  int *global_idx,
                                                  int N,
                                                  pcl::PointXYZRGB point)
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        int num_points = counts[idx];

        if( num_points > 0 ){
            int out_idx = atomicAdd( global_idx, 1 ); // Atomic add of all non-empty voxels
            // Coordinates
            point.x = sums[6*idx+0] / num_points;
            point.y = sums[6*idx+1] / num_points;
            point.z = sums[6*idx+2] / num_points;
            // Colors
            uint32_t r = (uint32_t)(sums[6*idx+3]) / num_points;
            uint32_t g = (uint32_t)(sums[6*idx+4]) / num_points;
            uint32_t b = (uint32_t)(sums[6*idx+5]) / num_points;
            uint32_t rgb = (r << 16 | g << 8 | b);
            point.rgb = *reinterpret_cast<float*>(&rgb);
            // Write output
            out_cloud[out_idx] = point;
        }
    }
}

__global__ void voxelAddAndCount( const DPoint *cloud,
                                                  float *sums,
                                                  int *counts,
                                                  float *min_corner,
                                                  float voxel_sz,
                                                  int3 grid_bins,
                                                  int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        DPoint point = cloud[idx];

        // Calculate bin coordinates and voxel index
        int bin_x = (point.x - min_corner[0]) / voxel_sz;
        int bin_y = (point.y - min_corner[1]) / voxel_sz;
        int bin_z = (point.z - min_corner[2]) / voxel_sz;
        int voxel_idx = ( bin_z*grid_bins.y + bin_y ) * grid_bins.x + bin_x;

        // Increment count and sum
        atomicAdd( counts+voxel_idx, 1 );
        atomicAdd( sums+3*voxel_idx+0, point.x );
        atomicAdd( sums+3*voxel_idx+1, point.y );
        atomicAdd( sums+3*voxel_idx+2, point.z );
    }
}

__global__ void voxelAddAndCount( const pcl::PointXYZRGB *cloud,
                                                  float *sums,
                                                  int *counts,
                                                  float *min_corner,
                                                  float voxel_sz,
                                                  int3 grid_bins,
                                                  int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        pcl::PointXYZRGB point = cloud[idx];

        // Calculate bin coordinates and voxel index
        int bin_x = (point.x - min_corner[0]) / voxel_sz;
        int bin_y = (point.y - min_corner[1]) / voxel_sz;
        int bin_z = (point.z - min_corner[2]) / voxel_sz;
        int voxel_idx = ( bin_z*grid_bins.y + bin_y ) * grid_bins.x + bin_x;

        // Increment count
        atomicAdd( counts+voxel_idx, 1 );
        // Sum coordinates
        atomicAdd( sums+6*voxel_idx+0, point.x );
        atomicAdd( sums+6*voxel_idx+1, point.y );
        atomicAdd( sums+6*voxel_idx+2, point.z );
        // Sum colors
        int rgb = *reinterpret_cast<int*>(&point.rgb);
        int r = (rgb >> 16) & 0x0000ff;
        int g = (rgb >> 8)  & 0x0000ff;
        int b = (rgb)       & 0x0000ff;
        atomicAdd( (int*)(sums+6*voxel_idx+3), r );
        atomicAdd( (int*)(sums+6*voxel_idx+4), g );
        atomicAdd( (int*)(sums+6*voxel_idx+5), b );
    }
}

__global__ void setupRandom( curandState* states, int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        curand_init( idx, 0, 0, states+idx );
    }
}

//////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// PEARSON
//////////////////////////////////////////////////////////////////////

__global__ void pearsonOnStd( const float *hists_a, // moar here
                                              const float *hists_b,
                                              float *correlations,
                                              const int hists_total_step,
                                              const int single_hist_step,
                                              const int num_a_hists,
                                              const int num_b_hists,
                                              const int ftrs_offset,
                                              const int corr_step,
                                              const int corr_offset,
                                              int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        // Get output X, Y
        int out_size = num_a_hists*num_b_hists;
        int ftr_idx = idx / out_size;
        int pair_idx = idx % out_size;
        int a_idx = pair_idx / num_b_hists;
        int b_idx = pair_idx % num_b_hists;
        int a_offset = a_idx*hists_total_step + ftrs_offset + ftr_idx*single_hist_step;
        int b_offset = b_idx*hists_total_step + ftrs_offset + ftr_idx*single_hist_step;

        float sum = 0;
        for( int bin=0; bin < single_hist_step; bin++ )
            sum += hists_a[a_offset+bin]*hists_b[b_offset+bin];

        float correlation = sum / single_hist_step;
        int corr_idx = (a_idx*num_b_hists+b_idx)*corr_step +
                corr_offset + ftr_idx;
        correlations[ corr_idx ] = correlation;
    }
}

__global__ void L2Kernel( const float *hists_a, // moar here
                                              const float *hists_b,
                                              float *scores,
                                              const int hists_total_step,
                                              const int single_hist_step,
                                              const int num_a_hists,
                                              const int num_b_hists,
                                              const int ftrs_offset,
                                              const int out_step,
                                              const int out_offset,
                                              int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        // Get output X, Y
        int out_size = num_a_hists*num_b_hists;
        int ftr_idx = idx / out_size;
        int pair_idx = idx % out_size;
        int a_idx = pair_idx / num_b_hists;
        int b_idx = pair_idx % num_b_hists;
        int a_offset = a_idx*hists_total_step + ftrs_offset + ftr_idx*single_hist_step;
        int b_offset = b_idx*hists_total_step + ftrs_offset + ftr_idx*single_hist_step;

        float sum = 0;
        for( int bin=0; bin < single_hist_step; bin++ )
            sum += (hists_a[a_offset+bin] - hists_b[b_offset+bin]) * (hists_a[a_offset+bin] - hists_b[b_offset+bin]);

        float dist = sqrt(sum);
        float score = 2.0-dist;
        int out_idx = (a_idx*num_b_hists+b_idx)*out_step +
                out_offset + ftr_idx;
        scores[ out_idx ] = score;
    }
}

}
