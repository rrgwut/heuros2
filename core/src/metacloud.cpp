#include <core/metacloud.h>


using namespace std;


namespace heuros
{

Metacloud::Metacloud()
{
    cloud = DPointCloudPtr( new DPointCloud );
    normals = DNormalsPtr( new DNormals );
    colors = DFloatArrPtr( new DFloatArr );
    sensor_orientation = DFloatArrPtr( new DFloatArr );
    features = Features<float>::Ptr( new Features<float> );
    segment_indices = Features<int>::Ptr( new Features<int> );
    segmented_points = SegmentedPointsPtr( new vector<DIntArrPtr> );
    props_clusters = ClustersPtr( new Clusters );
    avg_neighbors = -1;
}

size_t Metacloud::numFeatures() const
{
    return features->num_features;
}

size_t Metacloud::numSegmentSets() const
{
    return segment_indices->num_features;
}

size_t Metacloud::size() const
{
    return cloud->size();
}

Metacloud::~Metacloud()
{
    release();
}

void Metacloud::release()
{
}

}
