#include <core/utils_gpu.h>
#include <core/impl/manipulations.cuh>
#include <stdio.h>
#include <tr1/type_traits>
#include <algorithm>
#include <iomanip>


using namespace std;
using namespace pcl::gpu;


namespace heuros
{

template<class T>
void UtilsGPU::manualMemcpy( T* dst, const T *src, int elems )
{
    if( elems == 0 ) return;
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = elems;
    proMemcpy <<< nBlocks, blockSize >>> ( dst, src, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}
template void UtilsGPU::manualMemcpy<int>( int*, const int*, int elems );
template void UtilsGPU::manualMemcpy<float>( float*, const float*, int elems );

void UtilsGPU::manualMemcpy( unsigned char *dst, int dpitch, int doffset,
                             const unsigned char *src, int spitch, int soffset,
                             int width, int height )
{
    if( width == 0 || height == 0 ) return;
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = width*height;
    proMemcpy <<< nBlocks, blockSize >>> ( dst, dpitch, doffset,
                                           src, spitch, soffset,
                                           width, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void UtilsGPU::cudaSetIntsTo( int *data, int value, int N )
{
    // Set up kernel params
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);

    // Call kernel
    setIntsTo <<< nBlocks, blockSize >>>( data, value, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void UtilsGPU::splitPointColor( const DeviceArray<pcl::PointXYZRGB> &xyzrgb_cloud,
                                DeviceArray<DPoint> &xyz_cloud,
                                DeviceArray<float> &rgb_cloud )
{
    // Set up kernel params
    int N = xyzrgb_cloud.size();
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);

    // Call kernel
    splitXYZRGB <<< nBlocks, blockSize >>>( xyzrgb_cloud.ptr(), xyz_cloud.ptr(), rgb_cloud.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

DPointCloudXYZRGBPtr UtilsGPU::mergePointColor( DeviceArray<DPoint> &xyz_cloud,
                                                DeviceArray<float> &rgb_cloud )
{
    DPointCloudXYZRGBPtr out_cloud( new DeviceArray<pcl::PointXYZRGB>(xyz_cloud.size()) );
    // Set up kernel params
    int N = out_cloud->size();
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);

    // Call kernel
    mergeXYZRGB <<< nBlocks, blockSize >>>( out_cloud->ptr(), xyz_cloud.ptr(), rgb_cloud.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return out_cloud;
}

DPointCloudPtr UtilsGPU::extractCluster( const DPointCloudConstPtr &cloud,
                                         const IndicesConstPtr &indices )
{
    int cluster_sz = indices->size();
    DeviceArray<int> d_indices;
    d_indices.upload( *indices );
    DPointCloudPtr out_cloud( new DPointCloud(cluster_sz) );

    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = cluster_sz;
    extractClusterKernel <<< nBlocks, blockSize >>> ( cloud->ptr(), d_indices.ptr(), out_cloud->ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return out_cloud;
}

DPointCloudPtr UtilsGPU::extractCluster( const DPointCloudConstPtr &cloud,
                                         const ClustersPtr &clusters,
                                         const int cluster_idx )
{
    clusters->downloadSizesAndStartInd();

    int cluster_sz = clusters->h_cluster_sizes[cluster_idx];
    DPointCloudPtr out_cloud( new DPointCloud(cluster_sz) );
    int cluster_start_idx = clusters->h_start_indices[cluster_idx];

    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = cluster_sz;
    extractClusterKernel <<< nBlocks, blockSize >>> ( cloud->ptr(), clusters->point_indices+cluster_start_idx, out_cloud->ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return out_cloud;
}

MetacloudPtr UtilsGPU::extractSubcloud( const MetacloudConstPtr &src,
                                        const IndicesConstPtr &indices,
                                        bool build_octree )
{
    // Create clusters structure


    vector<int> h_cluster_sizes( 1, indices->size() );
    vector<int> h_start_indices( 1, 0 );
    vector<int> h_cluster_indices( indices->size(), 0 );

    ClustersPtr clusters( new Clusters( 1, indices->size() ) );
    clusters->cluster_sizes.upload( h_cluster_sizes.data(), 1 );
    clusters->start_indices.upload( h_start_indices.data(), 1 );
    clusters->point_indices.upload( indices->data(), indices->size() );
    cudaSetIntsTo( clusters->cluster_indices.ptr(), 0, indices->size() );

    // Extract
    return extractSubcloud( src, clusters, build_octree );
}

MetacloudPtr UtilsGPU::extractSubcloud( const MetacloudConstPtr &src,
                                        const ClustersPtr &clusters,
                                        int cluster_idx,
                                        bool build_octree )
{
    vector<int> selected_indices;
    selected_indices.push_back(cluster_idx);
    return extractSubcloud( src, clusters, selected_indices, build_octree );
}

MetacloudPtr UtilsGPU::extractSubcloud( const MetacloudConstPtr &src,
                                        const ClustersPtr &clusters,
                                        bool build_octree )
{
    vector<int> selected_indices( clusters->num_clusters );
    for( int i=0; i<selected_indices.size(); i++ )
        selected_indices[i] = i;
    return extractSubcloud( src, clusters, selected_indices, build_octree );
}

MetacloudPtr UtilsGPU::extractSubcloud( const MetacloudConstPtr &src,
                                        const ClustersPtr &clusters,
                                        vector<int> selected_indices,
                                        bool build_octree )
{
    clusters->downloadSizesAndStartInd();
    int out_sz = 0;
    for( int i=0; i<selected_indices.size(); i++ )
        out_sz += clusters->h_cluster_sizes[ selected_indices[i] ];

    // Create output
    MetacloudPtr dst( new Metacloud );
    dst->cloud->create(out_sz);
    dst->colors->create(out_sz);
    dst->normals->create(out_sz);
    dst->features->create( src->features->num_features, out_sz );

    int copied_pts = 0;
    for( int i=0; i<selected_indices.size(); i++ ){

        int cluster_idx = selected_indices[i];
        int cluster_sz = clusters->h_cluster_sizes[cluster_idx];
        int cluster_start_idx = clusters->h_start_indices[cluster_idx];
        int blockSize = BLOCK_SZ;
        int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
        int N = cluster_sz;

        // Copy points
        extractClusterKernel <<< nBlocks, blockSize >>> ( src->cloud->ptr(),
                                                          clusters->point_indices.ptr() + cluster_start_idx,
                                                          dst->cloud->ptr() + copied_pts, N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());

        // Copy colors
        extractClusterKernel <<< nBlocks, blockSize >>> ( src->colors->ptr(),
                                                          clusters->point_indices.ptr() + cluster_start_idx,
                                                          dst->colors->ptr() + copied_pts, N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());

        // Copy normals
        extractClusterKernel <<< nBlocks, blockSize >>> ( src->normals->ptr(),
                                                          clusters->point_indices.ptr() + cluster_start_idx,
                                                          dst->normals->ptr() + copied_pts, N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());

        // Copy features
        for( int f=0; f<src->features->num_features; f++ ){
            extractClusterKernel <<< nBlocks, blockSize >>> ( src->features->data() + f*src->features->step,
                                                              clusters->point_indices.ptr()+cluster_start_idx,
                                                              dst->features->data() + f*dst->features->step + copied_pts,
                                                              N );
            cudaSafeCall(cudaGetLastError());
            cudaSafeCall(cudaDeviceSynchronize());
        }

        copied_pts += cluster_sz;
    }

    // Copy misc
    dst->sensor_orientation = DFloatArrPtr( new DFloatArr(src->sensor_orientation->size()) );
    src->sensor_orientation->copyTo( *dst->sensor_orientation );
    dst->avg_neighbors = src->avg_neighbors;

    // Build octree
    if( build_octree ){
        dst->octree = pcl::gpu::Octree::Ptr( new pcl::gpu::Octree );
        pcl::gpu::Octree::PointCloud &octree_cloud = (pcl::gpu::Octree::PointCloud&)(*dst->cloud);
        dst->octree->setCloud( octree_cloud );
        dst->octree->build();
    }

    return dst;
}

template<typename T, typename U>
struct is_same { static const bool value = false; };

template<typename T>
struct is_same<T,T> { static const bool value = true; };

template<class T>
boost::shared_ptr<DeviceArray<T> > UtilsGPU::voxelGridFilter( const boost::shared_ptr<DeviceArray<T> > &cloud,
                                                              const float voxel_sz )
{
    // Get minmax
    vector<float> h_min(3), h_max(3);
    for( int i=0; i<3; i++ ){
        h_min[i] = 9999;
        h_max[i] = -9999;
    }
    DFloatArr min(3), max(3);
    min.upload( h_min );
    max.upload( h_max );

    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = cloud->size();
    minMaxXYZ <<< nBlocks, blockSize >>> ( cloud->ptr(), min.ptr(), max.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    min.download( h_min.data() );
    max.download( h_max.data() );

    // Calculate grid params
    float3 grid_dims;
    grid_dims.x = h_max[0] - h_min[0] + 0.001;
    grid_dims.y = h_max[1] - h_min[1] + 0.001;
    grid_dims.z = h_max[2] - h_min[2] + 0.001;
    int3 grid_bins;
    grid_bins.x = grid_dims.x / voxel_sz + 1;
    grid_bins.y = grid_dims.y / voxel_sz + 1;
    grid_bins.z = grid_dims.z / voxel_sz + 1;

    // Create grid structures
    int num_bins = grid_bins.x * grid_bins.y * grid_bins.z;
    cout << "NUM BINS: " << num_bins << endl;
    DFloatArr sums;
    DIntArr counts(num_bins);
    cudaSetIntsTo( counts.ptr(), 0, counts.size() );
    if( is_same<T, DPoint>::value )
        sums.create(3*num_bins);
    else if( is_same<T, pcl::PointXYZRGB>::value )
        sums.create(6*num_bins);
    else cout << endl << "ERROR: INVALID POINT TYPE" << endl << endl;
    cudaMemset( sums.ptr(), 0, sums.size()*sizeof(float) );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Add coordinates and count points
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = cloud->size();
    voxelAddAndCount <<< nBlocks, blockSize >>> ( cloud->ptr(), sums.ptr(), counts.ptr(), min.ptr(), voxel_sz, grid_bins, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Compact and calculate centroids
    DeviceArray<T> large_cloud( num_bins );
    vector<int> h_global_idx(1,0);
    DIntArr global_idx(1);
    global_idx.upload( h_global_idx );
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_bins;
    compactCentroids <<< nBlocks, blockSize >>> ( sums.ptr(), counts.ptr(), large_cloud.ptr(), global_idx.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Copy data to the output cloud
    global_idx.download( h_global_idx );
    boost::shared_ptr<DeviceArray<T> > out_cloud( new DeviceArray<T>( h_global_idx[0]) );
    manualMemcpy<T>( out_cloud->ptr(), large_cloud.ptr(), out_cloud->size() );
    return out_cloud;
}

template boost::shared_ptr<DeviceArray<DPoint> >
UtilsGPU::voxelGridFilter( const boost::shared_ptr<DeviceArray<DPoint> > &cloud,
                           const float voxel_sz );

template boost::shared_ptr<DeviceArray<pcl::PointXYZRGB> >
UtilsGPU::voxelGridFilter( const boost::shared_ptr<DeviceArray<pcl::PointXYZRGB> > &cloud,
                           const float voxel_sz );

/*
DPointCloudPtr UtilsGPU::voxelGridFilter( const DPointCloudConstPtr &cloud,
                                          const float voxel_sz )
{
    // Get minmax
    ManagedFloatArr min(3), max(3);
    for( int i=0; i<3; i++ ){
        min.data[i] = 9999;
        max.data[i] = -9999;
    }
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = cloud->size();
    minMaxXYZ <<< nBlocks, blockSize >>> ( cloud->ptr(), min.data, max.data, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Calculate grid params
    float3 grid_dims;
    grid_dims.x = max.data[0] - min.data[0] + 0.001;
    grid_dims.y = max.data[1] - min.data[1] + 0.001;
    grid_dims.z = max.data[2] - min.data[2] + 0.001;
    int3 grid_bins;
    grid_bins.x = grid_dims.x / voxel_sz + 1;
    grid_bins.y = grid_dims.y / voxel_sz + 1;
    grid_bins.z = grid_dims.z / voxel_sz + 1;

    // Create grid structures
    int num_bins = grid_bins.x * grid_bins.y * grid_bins.z;
    ManagedFloatArr sums_xyz(3*num_bins);
    ManagedIntArr counts(num_bins);
    cudaSetIntsTo( counts.data, 0, counts.size );
    cudaMemset( sums_xyz.data, 0, sums_xyz.size*sizeof(float) );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Add coordinates and count points
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = cloud->size();
    voxelAddAndCount <<< nBlocks, blockSize >>> ( cloud->ptr(), sums_xyz.data, counts.data, min.data, voxel_sz, grid_bins, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Compact and calculate centroids
    DPointCloud large_cloud( num_bins );
    ManagedIntArr global_idx(1);
    global_idx.data[0] = 0;
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_bins;
    compactCentroids <<< nBlocks, blockSize >>> ( sums_xyz.data, counts.data, large_cloud.ptr(), global_idx.data, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Copy data to the output cloud
    DPointCloudPtr out_cloud( new DPointCloud( global_idx.data[0]) );
    manualMemcpy<DPoint>( out_cloud->ptr(), large_cloud.ptr(), out_cloud->size() );
    return out_cloud;
}
*/

/***** Not tested *****
DPointCloudPtr UtilsGPU::cpySelectedDev( const DPointCloudConstPtr &src, const vector<int> &indices )
{
    DPointCloudPtr dst( new DPointCloud(indices.size()) );
    DeviceArray<int> d_indices(indices.size());
    d_indices.upload( indices );
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    in N = sizeof(DPoint)*indices.size();
    copySelected <<< nBlocks, blockSize >>>( (float*)src->ptr(),
                                             (float*)dst->ptr(),
                                             d_indices.ptr(),
                                             sizeof(DPoint), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    return dst;
}*/

DPointCloudPtr UtilsGPU::cpySelectedHost( const DPointCloudConstPtr &src, const vector<int> &indices )
{
    vector<DPoint> h_src( src->size() );
    vector<DPoint> h_dst( indices.size() );
    src->download( h_src.data() );
    for( int i=0; i<indices.size(); i++ )
        h_dst[i] = h_src[indices[i]];
    DPointCloudPtr dst( new DPointCloud );
    dst->upload( h_dst );
    return dst;
}

IndicesPtr UtilsGPU::neighborhoodFilterIndices( const DPointCloudConstPtr &src,
                                                const DPointCloudConstPtr &check_points,
                                                float radius )
{
    if( !check_points )
        return IndicesPtr( new vector<int> );

    // Build octree from check points
    pcl::gpu::Octree octree;
    pcl::gpu::Octree::PointCloud &check_cloud = (pcl::gpu::Octree::PointCloud&)(*check_points);
    octree.setCloud( check_cloud );
    octree.build();

    // Search for neighbors at each src point
    int max_nbrs = 50; // Pretty irrelevant
    NeighborIndices neighbor_indices( src->size(), max_nbrs );
    Octree::PointCloud &src_cloud = (pcl::gpu::Octree::PointCloud&)(*src);
    octree.radiusSearch( src_cloud, radius, max_nbrs, neighbor_indices );

    // Filter by number of points
    vector<int> h_sizes( neighbor_indices.sizes.size() );
    neighbor_indices.sizes.download( h_sizes.data() );
    IndicesPtr indices( new vector<int> );
    for( int i=0; i<h_sizes.size(); i++ )
        if( h_sizes[i] > 0 )
            indices->push_back(i);

    return indices;
}

DPointCloudPtr UtilsGPU::neighborhoodFilter( const DPointCloudConstPtr &src_points,
                                             const DPointCloudConstPtr &check_points,
                                             float radius )
{
    IndicesPtr indices = neighborhoodFilterIndices( src_points, check_points, radius );
    return cpySelectedHost( src_points, *indices );
}

DPointCloudPtr UtilsGPU::neighborhoodFilter( const DPointCloudConstPtr &src_points,
                                             const DPointCloudConstPtr &check_points,
                                             vector<float> &in_out_scores,
                                             float radius )
{
    if( check_points->size() == 0 || src_points->size() == 0 ){
        in_out_scores = vector<float>(0);
        return DPointCloudPtr( new DPointCloud(0) );
    }

    IndicesPtr indices = neighborhoodFilterIndices( src_points, check_points, radius );

    vector<float> new_scores( indices->size() );
    for( int i=0; i<indices->size(); i++ )
        new_scores[i] = in_out_scores[indices->at(i)];
    in_out_scores = new_scores;

    return cpySelectedHost( src_points, *indices );
}


ClustersPtr UtilsGPU::neighbors2Clusters( pcl::gpu::NeighborIndices &neighbors )
{
    int num_clusters = neighbors.sizes.size();
    vector<int> sizes_cpu;
    neighbors.sizes.download( sizes_cpu );

    // Remove empty clusters
    vector<int> ex_scan;
    vector<int> compact_sizes;
    vector<int> new_2_old;
    for( int i=0; i<sizes_cpu.size(); i++ ){
        if( sizes_cpu[i] > MIN_CLUSTER_SZ ){
            int scan_val = 0;
            if( ex_scan.size() > 0 )
                scan_val = ex_scan.back() + compact_sizes.back();
            ex_scan.push_back( scan_val );
            compact_sizes.push_back( sizes_cpu[i] );
            new_2_old.push_back(i);
        }
    }
    DeviceArray<int> new_2_old_gpu( new_2_old.size() );
    new_2_old_gpu.upload( new_2_old );

    // Create clusters_structure
    int total_size = ex_scan.back() + compact_sizes.back();

    vector<int> h_start_indices( compact_sizes.size() );
    vector<int> h_cluster_sizes( compact_sizes.size() );
    for( int i=0; i<compact_sizes.size(); i++ ){
        h_start_indices[i] = ex_scan[i];
        h_cluster_sizes[i] = compact_sizes[i];
    }

    ClustersPtr clusters( new Clusters(compact_sizes.size(), total_size) );
    clusters->start_indices.upload( h_start_indices.data(), h_start_indices.size() );
    clusters->cluster_sizes.upload( h_cluster_sizes.data(), h_cluster_sizes.size() );

    // Copy data
    int N = compact_sizes.size()*neighbors.max_elems;
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);
    fetchClusters <<< nBlocks, blockSize >>>( neighbors.data.ptr(),
                                              clusters->cluster_sizes.ptr(),
                                              clusters->start_indices.ptr(),
                                              clusters->point_indices.ptr(),
                                              clusters->cluster_indices.ptr(),
                                              new_2_old_gpu.ptr(),
                                              neighbors.max_elems,
                                              N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return clusters;
}

}
