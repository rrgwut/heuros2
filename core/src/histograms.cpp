#include <core/histograms.h>


using namespace std;


namespace heuros
{

template<class T>
Histograms<T>::Histograms()
{
    cout << "EMPTY CONSTRUCTOR" << endl;
    num_clusters = 0;
    num_1d_ftrs = 0;
    num_h_ftrs = 0;
    num_2d_ftrs = 0;
    total_size = 0;
}

template<class T>
Histograms<T>::Histograms( int _num_clusters , int _num_1d_ftrs, int _num_h_ftrs, int _num_2d_ftrs )
{
    create( _num_clusters, _num_1d_ftrs, _num_h_ftrs, _num_2d_ftrs );
}

template<class T>
Histograms<T>::~Histograms()
{
    if( total_size <= 0 ) return;
}

template<class T>
void Histograms<T>::create( int _num_clusters, int _num_1d_ftrs, int _num_h_ftrs, int _num_2d_ftrs )
{
    num_clusters = _num_clusters;
    num_1d_ftrs = _num_1d_ftrs;
    num_h_ftrs = _num_h_ftrs;
    num_2d_ftrs = _num_2d_ftrs;
    if( num_2d_ftrs > 0 )
        lut_2d.create( 2*num_2d_ftrs );
    total_size = num_clusters * ( (num_1d_ftrs+num_h_ftrs)*HIST_RES + num_2d_ftrs*HIST_2D_STEP );
    data.create( total_size );
    class_indices.create( num_clusters );
}

template<class T>
int Histograms<T>::numFeatures()
{
    return num_1d_ftrs + num_h_ftrs + num_2d_ftrs;
}

template<class T>
int Histograms<T>::clusterSize()
{
    if( total_size == 0 ) return 0;
    return total_size / num_clusters;
}

template<class T>
int Histograms<T>::offsetHistFtrs()
{
    return num_1d_ftrs * HIST_RES;
}

template<class T>
int Histograms<T>::offset2D()
{
    return (num_1d_ftrs+num_h_ftrs) * HIST_RES;
}

// Explicit instantiation //////////
template class Histograms<int>;
template class Histograms<float>;
////////////////////////////////////

}
