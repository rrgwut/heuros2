#include <core/conversions.h>
#include <core/utils_gpu.h>


using namespace std;


namespace heuros
{

pcl::PointCloud<pcl::PointXYZRGB>::Ptr
Conversions::downloadXYZRGB( const MetacloudConstPtr &metacloud )
{
    DPointCloudXYZRGBConstPtr d_cloud = UtilsGPU::mergePointColor( *metacloud->cloud, *metacloud->colors );
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr h_cloud ( new pcl::PointCloud<pcl::PointXYZRGB> );
    h_cloud->resize( metacloud->cloud->size() );
    d_cloud->download( h_cloud->points.data() );
    return h_cloud;
}

pcl::PointCloud<pcl::Normal>::Ptr
Conversions::downloadNormal( const MetacloudConstPtr &metacloud )
{
    pcl::PointCloud<pcl::Normal>::Ptr h_cloud( new pcl::PointCloud<pcl::Normal> );
    h_cloud->resize( metacloud->normals->size() );
    metacloud->normals->download( (DPoint*)(h_cloud->points.data()) );
    return h_cloud;
}

vector<FeaturePtr>
Conversions::downloaDFloatArrs( const MetacloudConstPtr &metacloud )
{
    vector<FeaturePtr> h_features;
    for( int i=0; i<metacloud->numFeatures(); i++ ){
        FeaturePtr new_host_ftr( new vector<float>( metacloud->size() ) );
        metacloud->features->downloadFeature( new_host_ftr, i );
        h_features.push_back( new_host_ftr );
    }
    return h_features;
}

vector<IndicesPtr>
Conversions::downloadSegmentIndices( const MetacloudConstPtr &metacloud )
{
    vector<IndicesPtr> output;

    for( int i=0; i<metacloud->numSegmentSets(); i++ ){
        IndicesPtr h_indices( new vector<int> );
        metacloud->segment_indices->downloadFeature( h_indices, i );
        output.push_back( h_indices );
    }

    return output;
}

}
