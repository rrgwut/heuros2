#include <core/timers.h>

namespace heuros
{

void timerStart( cudaEvent_t _start )
{
    cudaEventRecord( _start, 0 );
}

float timerStop( cudaEvent_t _start, cudaEvent_t _stop )
{
    float time;
    cudaEventRecord( _stop, 0 );
    cudaEventSynchronize( _stop );
    cudaEventElapsedTime( &time, _start, _stop );
    return time;
}

}
