#ifndef CONVERSIONS_H
#define CONVERSIONS_H


#include <iostream>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <core/metacloud.h>


namespace heuros{

class Conversions
{
public:

    /** \brief Download to PointXYZRGB PCL point cloud */
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr
    static downloadXYZRGB( const MetacloudConstPtr &metacloud );

    /** \brief Download to Normal PCL point cloud */
    pcl::PointCloud<pcl::Normal>::Ptr
    static downloadNormal( const MetacloudConstPtr &metacloud );

    /** \brief Download the features */
    std::vector<FeaturePtr>
    static downloaDFloatArrs( const MetacloudConstPtr &metacloud );

    /** \brief Download segment indices */
    std::vector<IndicesPtr>
    static downloadSegmentIndices( const MetacloudConstPtr &metacloud );

};

}

#endif
