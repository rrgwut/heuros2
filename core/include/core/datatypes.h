#ifndef DATATYPES_H
#define DATATYPES_H

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <cuda_runtime.h>
#include <pcl/gpu/containers/device_array.h>
#include <pcl/gpu/octree/octree.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

#define MAP_FOR(mapname, keytype, valtype) \
    for(map<keytype, valtype>::iterator it=mapname.begin(); it!=mapname.end(); ++it)

#define MAP_FOR_CONST(mapname, keytype, valtype) \
    for(map<keytype, valtype>::const_iterator it=mapname.begin(); it!=mapname.end(); ++it)


namespace heuros
{

typedef boost::shared_ptr<std::vector<float> > FeaturePtr;
typedef boost::shared_ptr<const std::vector<float> > FeatureConstPtr;
typedef boost::shared_ptr<std::vector<int> > IndicesPtr;
typedef boost::shared_ptr<const std::vector<int> > IndicesConstPtr;

typedef float4 DPoint; // XYZ + RGB
typedef float4 DNormal;

typedef pcl::gpu::DeviceArray<int> DIntArr;
typedef pcl::gpu::DeviceArray<float> DFloatArr;
typedef pcl::gpu::DeviceArray<DPoint> DPointCloud;
typedef pcl::gpu::DeviceArray<DNormal> DNormals;

typedef boost::shared_ptr<DPointCloud> DPointCloudPtr;
typedef boost::shared_ptr<DNormals> DNormalsPtr;
typedef boost::shared_ptr<DFloatArr> DFloatArrPtr;
typedef boost::shared_ptr<DIntArr> DIntArrPtr;
typedef boost::shared_ptr<const DPointCloud> DPointCloudConstPtr;
typedef boost::shared_ptr<const DNormals> DNormalsConstPtr;

typedef boost::shared_ptr<std::vector<DIntArrPtr> > SegmentedPointsPtr;
typedef boost::shared_ptr<pcl::gpu::NeighborIndices> NeighborIndicesPtr;

struct Annotation
{
    std::string name;
    std::vector<int> indices;

    int size()
    {
        return indices.size();
    }

    void push_back( const int val )
    {
        indices.push_back(val);
    }

    /** \brief Serialization */
    template<class Archive>
    void serialize( Archive & ar, const unsigned int version )
    {
        ar & name;
        ar & indices;
    }
};

typedef boost::shared_ptr<Annotation> AnnotationPtr;

}

#endif
