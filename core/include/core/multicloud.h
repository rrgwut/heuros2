/*
* This is used for miscellaneous object detection results. The multiclouds in the vector
* can store results of separate, subsequent or alternative algorighms. Eeach multicloud
* can contain multiple named point clouds used for e.g. object classes
*/

#ifndef MULTICLOUD_H
#define MULTICLOUD_H


#include<map>
#include <iostream>
#include <core/datatypes.h>


namespace heuros
{

class Multicloud
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Named point clouds containing detections or other results */
    std::map<std::string, DPointCloudPtr> clouds;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Resize the clouds vector */
    void clear(){ clouds.clear(); }

    /** \brief Returns the number of clouds held */
    int size(){ return clouds.size(); }

    /** \brief Access to subclouds */
    DPointCloudPtr& operator[]( const std::string &key ){ return clouds[key]; }
};

typedef boost::shared_ptr<Multicloud> MulticloudPtr;
typedef boost::shared_ptr<const Multicloud> MulticloudConstPtr;

}

#endif
