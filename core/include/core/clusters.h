#ifndef CLUSTERS_H
#define CLUSTERS_H


#include <iostream>
#include <boost/shared_ptr.hpp>
#include <core/datatypes.h>


namespace heuros
{

class Clusters
{
public:

    /** \brief Number of stored clusters */
    int num_clusters;

    /** \brief Total structure size (number of indices) */
    int total_size;

    /** \brief Cluster sizes (num of points) */
    DIntArr cluster_sizes;

    /** \brief Starting cluster indices in the data array */
    DIntArr start_indices;

    /** \brief Indices of the point cloud points corresponding to the elements */
    DIntArr point_indices;

    /** \brief Indices of the elements' containing clusters */
    DIntArr cluster_indices;

    /** \brief Semantic labels */
    DIntArr labels;

    std::vector<int> h_cluster_sizes, h_start_indices, h_point_indices, h_cluster_indices, h_labels;

    /** \brief Empty constructor */
    Clusters(){
        total_size = 0;
        num_clusters = 0;
    }

    /** \brief Main constructor */
    Clusters( int _num_clusters, int _total_size )
    {
        create( _num_clusters, _total_size );
    }

    /** \brief Destructor */
    ~Clusters()
    {
    }

    /** \brief Data allocation method */
    void create( int _num_clusters, int _total_size )
    {
        num_clusters = _num_clusters;
        total_size = _total_size;
        cluster_sizes.create( num_clusters );
        start_indices.create( num_clusters );
        labels.create( num_clusters );
        point_indices.create( total_size );
        cluster_indices.create( total_size );
    }

    /** \brief Download the sizes and start indices to local cpu structures */
    void downloadSizesAndStartInd()
    {
        if( h_cluster_sizes.size() == cluster_sizes.size() ) return;

        h_cluster_sizes.resize( cluster_sizes.size() );
        h_start_indices.resize( start_indices.size() );

        cluster_sizes.download( h_cluster_sizes.data() );
        start_indices.download( h_start_indices.data() );
    }
};

typedef boost::shared_ptr<Clusters> ClustersPtr;
typedef boost::shared_ptr<const Clusters> ClustersConstPtr;

}

#endif
