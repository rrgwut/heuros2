#ifndef KNOWLDEGE_RW_H
#define KNOWLEDGW_RW_H


#include <iostream>


namespace heuros
{

class KnowledgeRW
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Write variable to a global knowledge yaml file */
    template<class T>
    static void toGlobalYaml( const std::string file_name,
                              const std::string field_name,
                              const T value );

    /** \brief Write variable to an object-specific yaml file */
    template<class T>
    static void toObjectYaml( const std::string obj_name,
                              const std::string file_name,
                              const std::string field_name,
                              const T value );

    /** \brief Read variable from a global knowledge yaml file */
    template<class T>
    static void fromGlobalYaml( const std::string file_name,
                             const std::string field_name,
                             T &value );

    /** \brief Read variable from an object-specific yaml file */
    template<class T>
    static void fromObjectYaml( const std::string obj_name,
                                const std::string file_name,
                                const std::string field_name,
                                T &value );

    //==================================================

    // USE ONLY WITH SHARED POINTERS!
    /** \brief Write serializable structure to a global file */
    template<class T>
    static void toGlobalFile( const std::string file_name,
                              const T structure );

    /** \brief Write serializable structure to an object-specific file */
    template<class T>
    static void toObjectFile( const std::string obj_name,
                              const std::string file_name,
                              const T structure );

    // USE ONLY WITH SHARED POINTERS!
    /** \brief Read serializable structure from a global file */
    template<class T>
    static void fromGlobalFile( const std::string file_name,
                                T &structure );

    /** \brief Read serializable structure from an object-specific file */
    template<class T>
    static void fromObjectFile( const std::string obj_name,
                                const std::string file_name,
                                T &structure );

    //==================================================

    /** \brief Set the knowledge directory path */
    static void initializePath( std::string dir_name );

    /** \brief Get the list of objet classes (unknown order) */
    static std::vector<std::string> getClassList();

    /** \brief Check file existence */
    static bool checkExistence( std::string path );

    /** \brief Create directory if it doesn't exist */
    static bool forceDirExistence( std::string path );
    
    /** \brief Get path of the knowledge directory */
    static std::string knowledgePath();

    /** \brief Get path of the objects directory */
    static std::string objectsPath();

    //==================================================

    /** \brief Write variable to yaml file */
    template<class T>
    static void writeYaml( const std::string dir,
                           const std::string file_name,
                           const std::string field_name,
                           const T value );

    /** \brief Read variable from yaml file */
    template<class T>
    static void readYaml( const std::string dir,
                          const std::string file_name,
                          const std::string field_name,
                          T &value );

    /** \brief Write serializable structure to file */
    template<class T>
    static void writeBoost( const std::string dir,
                            const std::string file_name,
                            const T value );

    /** \brief Read serializable structure from file */
    template<class T>
    static void readBoost( const std::string dir,
                           const std::string file_name,
                           T &structure );

};

}

#endif
