#ifndef UTILS_GPU_H
#define UTILS_GPU_H


#include <curand.h>
#include <curand_kernel.h>
#include <pcl/point_types.h>

#include <core/metacloud.h>


namespace heuros
{

#define BLOCK_SZ 512
#define NUM_SM 15
#define MAX_SM_THREADS 2048

typedef boost::shared_ptr<pcl::gpu::DeviceArray<pcl::PointXYZRGB> > DPointCloudXYZRGBPtr;
typedef boost::shared_ptr<const pcl::gpu::DeviceArray<pcl::PointXYZRGB> > DPointCloudXYZRGBConstPtr;

class UtilsGPU
{
public:

    /** \brief Explicit templated cuda memcpy */
    template<class T>
    static void manualMemcpy( T* dst, const T *src, int elems );

    /** \brief Explicit templated cuda memcpy */
    static void manualMemcpy( unsigned char *dst, int dpitch, int doffset,
                              const unsigned char *src, int spitch, int soffset,
                              int width, int height );

    /** \brief Set pcl::gpu::DeviceArray<int> values to value */
    static void cudaSetIntsTo( int *data, int value , int N );

    /** \brief Split cloud XYZRGB into XYZ and RGB */
    static void splitPointColor(const pcl::gpu::DeviceArray<pcl::PointXYZRGB> &xyzrgb_cloud,
                                pcl::gpu::DeviceArray<DPoint> &xyz_cloud,
                                pcl::gpu::DeviceArray<float> &rgb_cloud );

    /** \brief Merge xyz and rgb xyzrgb into single point cloud */
    static DPointCloudXYZRGBPtr mergePointColor( pcl::gpu::DeviceArray<DPoint> &xyz_cloud,
                                                 pcl::gpu::DeviceArray<float> &rgb_cloud );

    /** \brief Extract a given cluster as DPointCloudPtr */
    static DPointCloudPtr extractCluster( const DPointCloudConstPtr &cloud,
                                          const IndicesConstPtr &indices );

    /** \brief Extract a selected cluster as DPointCloudPtr */
    static DPointCloudPtr extractCluster( const DPointCloudConstPtr &cloud,
                                          const ClustersPtr &clusters,
                                          const int cluster_idx );

    /** \brief Create new Metacloud defined by point indices */
    static MetacloudPtr extractSubcloud( const MetacloudConstPtr &src,
                                         const IndicesConstPtr &indices,
                                         bool build_octree=true );

    /** \brief Create new Metacloud defined by a single cluster index */
    static MetacloudPtr extractSubcloud( const MetacloudConstPtr &src,
                                         const ClustersPtr &clusters,
                                         int cluster_idx,
                                         bool build_octree=true );

    /** \brief Create new Metacloud defind by the provided clusters (NO REP CHECK) */
    static MetacloudPtr extractSubcloud( const MetacloudConstPtr &src,
                                         const ClustersPtr &clusters,
                                         bool build_octree=true );

    /** \brief Create new Metacloud defind by the provided cluster indices (NO REP CHECK) */
    static MetacloudPtr extractSubcloud( const MetacloudConstPtr &src,
                                         const ClustersPtr &clusters,
                                         std::vector<int> selected_indices,
                                         bool build_octree=true );

    /** \brief Filter cloud with voxel centroids */
    template<class T>
    static boost::shared_ptr<pcl::gpu::DeviceArray<T> > voxelGridFilter( const boost::shared_ptr<pcl::gpu::DeviceArray<T> > &cloud,
                                                               const float voxel_sz );

    /** \brief Filter cloud with voxel centroids */
    //static DPointCloudPtr voxelGridFilter( const DPointCloudConstPtr &cloud,
    //                                       const float voxel_sz );

    //static DPointCloudPtr cpySelectedDev( const DPointCloudConstPtr &src, const std::vector<int> &indices );

    static DPointCloudPtr cpySelectedHost( const DPointCloudConstPtr &src, const std::vector<int> &indices );

    static IndicesPtr neighborhoodFilterIndices( const DPointCloudConstPtr &src,
                                                 const DPointCloudConstPtr &check_points,
                                                 float radius );

    static DPointCloudPtr neighborhoodFilter( const DPointCloudConstPtr &src_points,
                                              const DPointCloudConstPtr &check_points,
                                              float radius );

    static DPointCloudPtr neighborhoodFilter( const DPointCloudConstPtr &src_points,
                                              const DPointCloudConstPtr &check_points,
                                              std::vector<float> &in_out_scores,
                                              float radius );

    static ClustersPtr neighbors2Clusters( pcl::gpu::NeighborIndices &neighbors );
};

}

#endif
