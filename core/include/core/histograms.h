#ifndef HISTOGRAMS_H
#define HISTOGRAMS_H


#include <iostream>
#include <cmath>
#include <boost/shared_ptr.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <pcl/gpu/utils/safe_call.hpp>
#include <core/datatypes.h>


namespace heuros
{

#define HIST_RES 20
#define HIST_2D_STEP (HIST_RES*HIST_RES)

template<class T>
class Histograms
{
public:

    /** \brief Number of clusters whose histograms are stored */
    int num_clusters;

    /** \brief Nmber of 1D, histogram features and 2D features */
    int num_1d_ftrs, num_h_ftrs, num_2d_ftrs;

    /** \brief Total histogram data size (number of hist bins) */
    int total_size;

    /** \brief Histogram values data */
    pcl::gpu::DeviceArray<T> data;

    /** \brief 2D histograms lookup table. Has a pair of indexes for each hist (total 2*num_2d_ftrs) */
    DIntArr lut_2d;

    /** \brief Cluster classes (as classification result or labeling) */
    DIntArr class_indices;

    /** \brief Empty constructor */
    Histograms();

    /** \brief Main constructor */
    Histograms( int _num_clusters , int _num_1d_ftrs, int _num_h_ftrs, int _num_2d_ftrs );

    /** \brief Destructor */
    ~Histograms();

    /** \brief Data allocation method */
    void create( int _num_clusters , int _num_1d_ftrs, int _num_h_ftrs , int _num_2d_ftrs );

    /** \brief Total number of features */
    int numFeatures();

    /** \brief Number of bins (values) in a cluster */
    int clusterSize();

    /** \brief Histogram featres offset from cluster start */
    int offsetHistFtrs();

    /** \brief 2D features offset from cluster start */
    int offset2D();

    /** \brief Serialization */
    template<class Archive>
    void serialize( Archive & ar, const unsigned int version )
    {
        ar & num_clusters;
        ar & num_1d_ftrs;
        ar & num_h_ftrs;
        ar & num_2d_ftrs;
/*
        if (Archive::is_loading::value){
            create( num_clusters, num_1d_ftrs, num_h_ftrs, num_2d_ftrs );

        }

        for( int i=0; i<total_size; i++ )
            ar & data[i];

        for( int i=0; i<num_2d_ftrs*2; i++ )
            ar & lut_2d[i];

        for( int i=0; i<num_clusters; i++ )
            ar & class_indices[i];*/
    }

private:

    friend class boost::serialization::access;
};

typedef Histograms<int> HistogramsInt;
typedef Histograms<float> HistogramsFloat;
typedef boost::shared_ptr<HistogramsInt> HistogramsIntPtr;
typedef boost::shared_ptr<HistogramsFloat> HistogramsFloatPtr;

}

#endif
