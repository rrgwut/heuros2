#ifndef MANIPULATIONS_CUH
#define MANIPULATIONS_CUH

#include <core/metacloud.h>
#include <curand_kernel.h>

namespace heuros
{

#define MIN_CLUSTER_SZ 5

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// UTILS
////////////////////////////////////////////////////////////////////

__forceinline__ __device__ float fatomicMin( float* address, float val )
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    if( __int_as_float(old) <= val ) return old;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
                          __float_as_int(::fminf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

__forceinline__ __device__ float fatomicMax( float* address, float val )
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    if( __int_as_float(old) >= val ) return old;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
                          __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

template<class T>
__global__ void proMemcpy( T *dst,
                           const T *src,
                           int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        dst[idx] = src[idx];
    }
}

__global__ void proMemcpy( unsigned char *dst, int dpitch, int doffset,
                           const unsigned char *src, int spitch, int soffset,
                           int width, int N );

__global__ void addColorKernel( DPoint *dst,
                                const DPoint *src_cloud,
                                const float *src_color,
                                int N );

__global__ void splitXYZRGB( const pcl::PointXYZRGB *xyzrgb,
                             DPoint *xyz,
                             float *rgb,
                             int N );

__global__ void mergeXYZRGB( pcl::PointXYZRGB *xyzrgb,
                             const DPoint *xyz,
                             const float *rgb,
                             int N,
                             pcl::PointXYZRGB out_point = pcl::PointXYZRGB() );

__global__ void setIntsTo( int *data,
                           int value,
                           int N );

__global__ void copySelected( const float *in_data,
                              float *out_data,
                              const int *cpy_indices,
                              int hist_size,
                              int N );

//////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// GENERAL
//////////////////////////////////////////////////////////////////////

template<class T>
__global__ void extractClusterKernel( const T *src,
                                      const int *indices,
                                      T *dst,
                                      int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        int src_idx = indices[idx];
        dst[idx] = src[src_idx];
    }
}

template __global__ void extractClusterKernel<DPoint>( const DPoint*, const int*, DPoint*, int );
template __global__ void extractClusterKernel<float>( const float*, const int*, float*, int );

__global__ void fetchClusters( const int *neighbors,
                               int *cluster_sizes,
                               int *start_indices,
                               int *point_indices,
                               int *cluster_indices,
                               int *new_2_old,
                               int max_neighbors,
                               int N );

template<class T>
__global__ void minMaxXYZ( const T *cloud,
                           float *min,
                           float *max,
                           int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        T point = cloud[idx];

        fatomicMin( min+0, point.x );
        fatomicMin( min+1, point.y );
        fatomicMin( min+2, point.z );

        fatomicMax( max+0, point.x );
        fatomicMax( max+1, point.y );
        fatomicMax( max+2, point.z );
    }
}

__global__ void compactCentroids( const float *sums,
                                  const int *counts,
                                  DPoint *out_cloud,
                                  int *global_idx,
                                  int N );

__global__ void compactCentroids( const float *sums,
                                  const int *counts,
                                  pcl::PointXYZRGB *out_cloud,
                                  int *global_idx,
                                  int N,
                                  pcl::PointXYZRGB point=pcl::PointXYZRGB());

__global__ void voxelAddAndCount( const DPoint *cloud,
                                  float *sums,
                                  int *counts,
                                  float *min_corner,
                                  float voxel_sz,
                                  int3 grid_bins,
                                  int N );

__global__ void voxelAddAndCount( const pcl::PointXYZRGB *cloud,
                                  float *sums,
                                  int *counts,
                                  float *min_corner,
                                  float voxel_sz,
                                  int3 grid_bins,
                                  int N );

__global__ void setupRandom( curandState* states, int N );

//////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// PEARSON
//////////////////////////////////////////////////////////////////////


template<class T>
__global__ void standarize( const T *in_hists,
                            float *out_hists,
                            const int hists_total_step,
                            const int single_hist_step,
                            const int num_ftrs,
                            const int offset,
                            int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        int hist_idx = block_virtual_idx / num_ftrs;
        int ftr_idx = block_virtual_idx % num_ftrs;
        T in_value = in_hists[ hist_idx*hists_total_step + offset +
                ftr_idx*single_hist_step +
                threadIdx.x ];

        // Create shared var
        __shared__ int shared_sum[1];
        __shared__ float shared_sqdev_sum[1];
        if( threadIdx.x == 0 ){
            shared_sum[0] = 0;
            shared_sqdev_sum[0] = 0;
        }
        __syncthreads();

        // Bin sum
        atomicAdd( shared_sum, in_value );
        __syncthreads();

        // Shift val to zero mean
        float mean = float(shared_sum[0]) / blockDim.x;
        float shifted_in = in_value - mean;

        // Sum square devs
        float sqdev = shifted_in*shifted_in;
        atomicAdd( shared_sqdev_sum, sqdev );
        __syncthreads();

        // Scale stddev into output val
        float stddev = sqrt( shared_sqdev_sum[0] / blockDim.x );
        float out_value = stddev == 0 ? 0 : shifted_in / stddev;
        out_hists[ hist_idx*hists_total_step + offset +
                ftr_idx*single_hist_step +
                threadIdx.x ] = out_value;
    }
}
template __global__ void standarize<int>( const int*, float*, const int, const int, const int, const int, int );
template __global__ void standarize<float>( const float*, float*, const int, const int, const int, const int, int );

__global__ void pearsonOnStd( const float *hists_a, // moar here
                              const float *hists_b,
                              float *correlations,
                              const int hists_total_step,
                              const int single_hist_step,
                              const int num_a_hists,
                              const int num_b_hists,
                              const int ftrs_offset,
                              const int corr_step,
                              const int corr_offset,
                              int N );

__global__ void L2Kernel( const float *hists_a, // moar here
                          const float *hists_b,
                          float *scores,
                          const int hists_total_step,
                          const int single_hist_step,
                          const int num_a_hists,
                          const int num_b_hists,
                          const int ftrs_offset,
                          const int out_step,
                          const int out_offset,
                          int N );

}

#endif
