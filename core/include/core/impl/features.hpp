#ifndef FEATURES_HPP
#define FEATURES_HPP

#include <core/features.h>

namespace heuros
{

template <class T>
Features<T>::Features()
{
    init_counter = 0;
    num_features = 0;
    array = DFeaturePtr( new DFeature );
}

template <class T>
Features<T>::Features( int _num_features, int size )
{
    init_counter = 0;
    array = DFeaturePtr( new DFeature );
    create( _num_features, size );
}

template <class T>
Features<T>::~Features()
{
    release();
}

template <class T>
void Features<T>::create( int _num_features, int size )
{
    num_features = _num_features;
    step = size;

    array->create( num_features * size );
}

template <class T>
void Features<T>::release()
{
    //array->release();
}

template <class T>
void Features<T>::downloadFeature( FeaturePtr &host_feature, int ftr_idx )
{
    host_feature = FeaturePtr( new std::vector<T>(step) );
    cudaSafeCall( cudaMemcpy(host_feature->data(), getFeaturePtr(ftr_idx), step*sizeof(float), cudaMemcpyDeviceToHost) );
    cudaSafeCall( cudaDeviceSynchronize() );
}

template <class T>
T* Features<T>::getFeaturePtr( int ftr_idx )
{
    return array->ptr() + ftr_idx*step;
}

template <class T>
T* Features<T>::data() const
{
    return array->ptr();
}

template <class T>
int Features<T>::countUp()
{
    return init_counter++;
}

template <class T>
int Features<T>::numPoints() const
{
    return array->size()/num_features;
}

}

#endif
