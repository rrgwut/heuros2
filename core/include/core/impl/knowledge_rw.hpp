#ifndef KNOWLEDGE_RW_HPP
#define KNOWLEDGE_RW_HPP

#include <core/knowledge_rw.h>

#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <yaml-cpp/yaml.h>
#include <ros/ros.h>
#include <ros/package.h>

namespace fs = boost::filesystem;

// ------------------------
// ----- YAML FILE RW -----
// ------------------------

namespace heuros
{

template<class T>
void KnowledgeRW::writeYaml( const std::string dir,
                             const std::string file_name,
                             const std::string field_name,
                             const T value )
{
    // Check if filesystem exists and create if it doesn't 
    KnowledgeRW::forceDirExistence( KnowledgeRW::knowledgePath() );
    KnowledgeRW::forceDirExistence( KnowledgeRW::objectsPath() );
    
    // Check if directory exists and create if it doesn't
    KnowledgeRW::forceDirExistence(dir);

    // Check if file exists and create if it doesn't
    std::string file_path = dir + "/" + file_name;
    if( !fs::exists( file_path ) ){
        std::ofstream new_file( file_path.c_str() );
        if( !new_file.is_open() ){
            ROS_ERROR( "Couldn't create file: %s", file_path.c_str() );
            return;
        }
        new_file.close();
    }

    // Write parameter
    YAML::Node node = YAML::LoadFile( file_path );
    node[ field_name ] = value;
    std::ofstream file( file_path.c_str() );
    file << node;
    file.close();
}

template<class T>
void KnowledgeRW::readYaml( const std::string dir,
                            const std::string file_name,
                            const std::string field_name,
                            T &value )
{
    // Get directory
    if( !KnowledgeRW::checkExistence( dir ) ) return;
    
    // Get parameter
    std::string file_path = dir + "/" + file_name;
    YAML::Node node = YAML::LoadFile( file_path );
    value = node[ field_name ].as<T>();
}

// ------------------------------------
// ----- YAML INTERFACE FUNCTIONS -----
// ------------------------------------

template<class T>
void KnowledgeRW::toGlobalYaml( const std::string file_name,
                                const std::string field_name,
                                const T value )
{
    KnowledgeRW::writeYaml( KnowledgeRW::knowledgePath(), file_name, field_name, value );
}

template<class T>
void KnowledgeRW::toObjectYaml( const std::string obj_name,
                                const std::string file_name,
                                const std::string field_name,
                                const T value )
{
    KnowledgeRW::writeYaml( KnowledgeRW::objectsPath()+obj_name, file_name, field_name, value );
}

template<class T>
void KnowledgeRW::fromGlobalYaml( const std::string file_name,
                                  const std::string field_name,
                                  T &value )
{
    KnowledgeRW::readYaml( KnowledgeRW::knowledgePath(), file_name, field_name, value );
}

template<class T>
void KnowledgeRW::fromObjectYaml( const std::string obj_name,
                                  const std::string file_name,
                                  const std::string field_name,
                                  T &value )
{
    KnowledgeRW::readYaml( KnowledgeRW::objectsPath()+obj_name, file_name, field_name, value );
}

// ----------------------------------
// ----- BOOST SERIALIZATION RW -----
// ----------------------------------

template<class T>
void KnowledgeRW::writeBoost( const std::string dir,
                              const std::string file_name,
                              const T structure )
{
    // Check if filesystem exists and create if it doesn't 
    KnowledgeRW::forceDirExistence( KnowledgeRW::knowledgePath() );
    KnowledgeRW::forceDirExistence( KnowledgeRW::objectsPath() );

    // Check if directory exists and create if it doesn't
    KnowledgeRW::forceDirExistence(dir);

    // Check if file exists and create if it dcesn't
    std::string file_path = dir + "/" + file_name;
    std::ofstream ofs( file_path.c_str() );
    if( !ofs.is_open() ){
        ROS_ERROR( "Couldn't create file: %s", file_path.c_str() );
        return;
    }

    // Write the structure
    boost::archive::text_oarchive oa(ofs);
    oa << structure;
    ofs.close();
}

template<class T>
void KnowledgeRW::readBoost( const std::string dir,
                          const std::string file_name,
                          T &structure )
{
    // Get directory
    if( !KnowledgeRW::checkExistence( dir ) ) return;
    
    // Open the file
    std::string file_path = dir + "/" + file_name;
    std::ifstream ifs( file_path.c_str() );

    // Read the class state
    boost::archive::text_iarchive ia(ifs);
    ia >> structure;
    ifs.close();
}

// -----------------------------------------
// ----- BOOST SERIALIZATION INTERFACE -----
// -----------------------------------------

template<class T>
void KnowledgeRW::toGlobalFile( const std::string file_name,
                                const T structure )
{
    KnowledgeRW::writeBoost( KnowledgeRW::knowledgePath(), file_name,  structure );
}

template<class T>
void KnowledgeRW::toObjectFile( const std::string obj_name,
                                const std::string file_name,
                                const T structure )
{
    KnowledgeRW::writeBoost( KnowledgeRW::objectsPath()+obj_name, file_name, structure );
}

template<class T>
void KnowledgeRW::fromGlobalFile( const std::string file_name,
                                  T &structure )
{
    KnowledgeRW::readBoost( KnowledgeRW::knowledgePath(), file_name, structure );
}

template<class T>
void KnowledgeRW::fromObjectFile( const std::string obj_name,
                                  const std::string file_name,
                                  T &structure )
{
    KnowledgeRW::readBoost( KnowledgeRW::objectsPath()+obj_name, file_name, structure );
}

// --------------------------
// ----- MISC FUNCTIONS -----
// --------------------------

std::string KnowledgeRW::knowledgePath()
{
    ros::NodeHandle n;
    std::string knowledge_path;
    n.param( "heuros/knowledge_path", knowledge_path, std::string("") );
    return knowledge_path;
}

std::vector<std::string> KnowledgeRW::getClassList()
{
   std::string directory = KnowledgeRW::knowledgePath() + "objects";
   std::vector<std::string> dir_list;
   if( fs::exists( directory ) ){
       fs::directory_iterator end ;
       for( fs::directory_iterator iter(directory) ; iter != end ; ++iter )
           if ( fs::is_directory(*iter) ){
               dir_list.push_back( iter->path().filename().string() );
           }
    }
    sort( dir_list.begin(), dir_list.end() );
    return dir_list;
}

std::string KnowledgeRW::objectsPath()
{
    return knowledgePath() + "objects/";
}

void KnowledgeRW::initializePath( std::string dir_name )
{
    // Get directory
    std::string directory = ros::package::getPath("heuros_core") +
                       "/../data/knowledge/" + dir_name;
    // Save the full path to a parameter
    ros::NodeHandle n;
    n.setParam( "heuros/knowledge_path", directory + "/" );
}

bool KnowledgeRW::checkExistence( std::string path )
{
    if( !fs::exists( path ) ){
        ROS_ERROR( "%s does not exist", path.c_str() );
        return false;
    }
    return true;
}

bool KnowledgeRW::forceDirExistence( std::string path )
{
    if( !fs::exists( path ) ){
        fs::path boost_dir_path( path );
        if ( !fs::create_directory(boost_dir_path) ){
            ROS_ERROR( "Couldn't create dirctory: %s", path.c_str() );
            return false;
        }
    }
    return true;
}

}

#endif

