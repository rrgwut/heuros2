#ifndef METACLOUD_H
#define METACLOUD_H


#include <iostream>
#include <opencv2/opencv.hpp>
#include <core/impl/features.hpp>
#include <core/histograms.h>
#include <core/clusters.h>
#include <core/multicloud.h>
#include <core/datatypes.h>


namespace heuros
{

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

// Set to 1 when using very high resolution (>512)
#define ULTRA_HIGH_RES 0
// Max number of point neighbors
#define MAX_NEIGHBORS (150*(1+ULTRA_HIGH_RES))
// Size of the neighborhoods
#define NBR_R_1 0.01
#define NBR_R_2 0.02


class Metacloud
{
public:

    //==================================================
    //================= PUBLIC STRUCTURES ==============
    //==================================================

    /** \brief Structure holding individual segment features */
    struct SegmentedFloatArr
    {
        std::string name;
        std::vector<float> values;
    };
    typedef boost::shared_ptr<SegmentedFloatArr> SegmentedFloatArrPtr;

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    //---------- COPIED TO SUBCLOUDS

    /** \brief Pointer to the input point cloud (on device).*/
    DPointCloudPtr cloud;

    /** \brief Pointer to the input normals (on device) */
    DNormalsPtr normals;

    /** \brief Pointer to the input RGB colors (on device) */
    DFloatArrPtr colors;

    /** \brief Structure holding GPU point features */
    Features<float>::Ptr features;

    /** \brief 3x3 rotation matrix */
    DFloatArrPtr sensor_orientation;

    /** \brief Average number of point neighbors */
    float avg_neighbors;

    //---------- NOT COPIED TO SUBCLOUDS

    /** \brief Average number of farther away neighbors */
    float avg_neighbors_2;

    /** \brief Depth image form sensor */
    cv::Mat depth_img;

    /** \brief Color image form sensor */
    cv::Mat color_img;

    /** \brief Intrinsic sensor parameters */
    std::vector<double> intrinsics;

    /** \brief Extrinsic sensor parameters */
    std::vector<double> extrinsics;

    /** \brief Scene name */
    std::string scene_name;

    /** \brief std::vector of segment features */
    std::vector<std::vector<SegmentedFloatArrPtr> > segmented_features;

    /** \brief Each point's segment indices */
    Features<int>::Ptr segment_indices;

    /** \brief List of segemnts*/
    SegmentedPointsPtr segmented_points;

    /** \brief Clusters obtained from non-flat segmentation */
    ClustersPtr props_clusters;

    /** \brief Octree for neighbors search */
    pcl::gpu::Octree::Ptr octree;

    /** \brief Indices of each point's neighbors (on device) */
    pcl::gpu::NeighborIndices neighbors_indices;

    /** \brief Farther away neighbor indices */
    pcl::gpu::NeighborIndices neighbors_indices_2;

    /** \brief Map of Multicloud shared pointers
     * This is used for miscellaneous object detection results. The multiclouds in the std::vector
     * can store results of separate, subsequent or alternative algorighms. Eeach multicloud
     * can contain multiple named point clouds used for e.g. object classes
     */
    std::map<std::string, MulticloudPtr> multiclouds;

    /** \brief std::vector of object annotations (CPU) */
    std::vector<AnnotationPtr> annotations;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    Metacloud();

    /** \brief Destructor */
    ~Metacloud();

    /** \brief Get the number of features */
    size_t numFeatures() const;

    /** \brief Get the number of segment sets */
    size_t numSegmentSets() const;

    /** \brief Get the number points */
    size_t size() const;

    /** \brief Release device memory */
    void release();
};

typedef boost::shared_ptr<Metacloud> MetacloudPtr;
typedef boost::shared_ptr<const Metacloud> MetacloudConstPtr;

}

#endif
