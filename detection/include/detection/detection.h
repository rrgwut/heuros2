#ifndef DETECTION_H
#define DETECTION_H


#include <iostream>
#include <core/metacloud.h>
#include <detection/convolutive_detector.h>


namespace heuros
{

class Detection
{
public:

    //====================================================
    //================= PUBLIC METHODS =================
    //====================================================

    /** \brief Constructor. */
    Detection();

    /** \brief Destructor. */
    ~Detection();

    /** \brief Main scene processing method */
    void detectObjects( MetacloudPtr _metacloud );

    /** \brief Add model to the base. */
    void addModel(const MetacloudConstPtr &_metacloud, const AnnotationPtr &annotation );

    /** \brief Add antimodel to the base. */
    void addAntimodel( const MetacloudConstPtr &_metacloud, const AnnotationPtr &annotation );

    /** \brief Schedule the clear() method to reset training */
    void scheduleClear();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief Active cluster index - for user commands. */
    int active_idx;

    volatile bool scheduled_clear;

    /** \brief Convolutive object detector */
    ConvolutiveDetector conv_detector;

    /** \brief Models classes. */
    std::vector<std::string> model_names;

    /** \brief array of random number generator structures */
    pcl::gpu::DeviceArray<curandState> dev_states;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Clear the models. */
    void clear();

};

}

#endif
