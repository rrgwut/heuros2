#ifndef HISTOGRAM_CALCULATIONS_GPU_H
#define HISTOGRAM_CALCULATIONS_GPU_H


#include <curand.h>
#include <curand_kernel.h>

#include <core/metacloud.h>
#include <core/histograms.h>
#include <core/timers.h>
#include <core/utils_gpu.h>

#include <pcl/gpu/features/features.hpp>
#include <pcl/point_types.h>


namespace heuros
{

#define HIST_NORM_L1 1
#define HIST_NORM_L2 2

#define N_HISTS_1D 5
#define N_HIST_FTRS 3
#define N_HISTS_2D 4

#define N_D2_ITERATIONS 20
#define N_D3_ITERATIONS 20
#define N_D4_ITERATIONS 20
#define D234_RES 512 // MAX 512!


class HistogramCalculationsGPU
{
public:

    //======================================================
    //================= BASIC CALCULATIONS =================
    //======================================================


    /** \brief Calculate unnormalized histograms for all features. */
    static HistogramsIntPtr calcUnnormHistograms( const MetacloudConstPtr &metacloud_gpu,
                                                  const ClustersPtr &clusters,
                                                  pcl::gpu::DeviceArray<curandState>& dev_states,
                                                  int include_2d=1 );

    /** \brief Standarize the input histograms. */
    static HistogramsFloatPtr standarizeHistograms( const HistogramsIntPtr &in_hists );

    /** \brief Calculate Pearson's correlation for all pairs. */
    static DFloatArrPtr allPairsPearson( const HistogramsFloatPtr &hists_a,
                                        const HistogramsFloatPtr &hists_b );

    //=======================================================
    //================= HISTOGRAM REDUCTION =================
    //=======================================================

    /** \brief Compare all hists and remove repeating (single correlation kernel). */
    static HistogramsFloatPtr reduceSingleHistSet( HistogramsFloatPtr &histograms,
                                                      std::vector<float> &ftr_weights,
                                                      float threshold );

    /** \brief Compare all hists A and B and reduce A (single correlation kernel). */
    static HistogramsFloatPtr reduceHistSetAUsingB( HistogramsFloatPtr &hists_a,
                                                       HistogramsFloatPtr &hists_b,
                                                       std::vector<float> &ftr_weights,
                                                       float threshold );

    /** \brief Large-scale version of reduceHistSetAUsingB. */
    static HistogramsFloatPtr LSReduceHistSetAUsingB( HistogramsFloatPtr &hists_a,
                                                         HistogramsFloatPtr &hists_b,
                                                         std::vector<float> &ftr_weights,
                                                         float threshold );

    /** \brief Filter histograms to remove repeating. */
    static HistogramsFloatPtr reduceHistograms( HistogramsFloatPtr &histograms,
                                                   std::vector<float> &ftr_weights,
                                                   float threshold );

    /** \brief Match histograms A against histograms B. Return scores. */
    static std::vector<float> matchAToB( HistogramsFloatPtr &hists_a,
                                    HistogramsFloatPtr &hists_b,
                                    std::vector<float> &ftr_weights );

    /** \brief Match histograms A against histograms B. Return scores. */
    static std::vector<float> LSMatchAToB( HistogramsFloatPtr &hists_a,
                                      HistogramsFloatPtr &hists_b,
                                      std::vector<float> &ftr_weights );

    //=======================================================
    //================= MEMORY MANIPULATION =================
    //=======================================================

    /** \brief Copy the specified block of histograms to new structure. */
    static HistogramsFloatPtr copyBlock( HistogramsFloatPtr &histograms,
                                            int start_hist, int num_hists );

    /** \brief Copy the histograms of specified indices to a new structure. */
    static HistogramsFloatPtr copySelectedHists( HistogramsFloatPtr &histograms,
                                                    std::vector<int> &indices );

    /** \brief Split histograms into std::vector of limited size hists. */
    static std::vector<HistogramsFloatPtr> splitHistograms( HistogramsFloatPtr &histograms );

    /** \brief Merge histograms std::vector into single histograms structure. */
    static HistogramsFloatPtr mergeHistograms( const std::vector<HistogramsFloatPtr> &histograms );

    /** \brief Initialize GPU random number generators. */
    static void initCurands( pcl::gpu::DeviceArray<curandState>& dev_states, int size );
};

}

#endif
