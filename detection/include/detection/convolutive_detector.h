#ifndef CONVOLUTIVE_H
#define CONVOLUTIVE_H


#include <iostream>
#include <core/metacloud.h>
#include <core/timers.h>
#include <detection/dense_calculations.h>


namespace heuros
{

class ConvolutiveDetector
{
public:

    //====================================================
    //================= PUBLIC METHODS =================
    //====================================================

    /** \brief Constructor. */
    ConvolutiveDetector();

    /** \brief Destructor. */
    ~ConvolutiveDetector();

    /** \brief Main scene processing method */
    void detectObjects( MetacloudPtr _metacloud );

    /** \brief Add model to the base. */
    void addModel( MetacloudPtr _metacloud, const AnnotationPtr &annotation );

    /** \brief Add antimodel to the base. */
    void addAntimodel( MetacloudPtr _metacloud, const IndicesConstPtr &indices );

    /** \brief Clear the models. */
    void clear();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief Resulting detections */
    DPointCloudPtr detections;

};

}

#endif
