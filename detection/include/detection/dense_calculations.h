#ifndef DENSE_CALCULATIONS_H
#define DENSE_CALCULATIONS_H


#include <pcl/gpu/features/features.hpp>
#include <pcl/point_types.h>

#include <core/metacloud.h>
#include <core/histograms.h>
#include <core/timers.h>
#include <core/utils_gpu.h>


namespace heuros
{

/** \brief ND histogram feature structure */
// @{
struct DenseHistograms
{
    DFloatArrPtr data;

    int step;

    DenseHistograms(){}

    DenseHistograms( int size_hists, int _step )
    {
        create( size_hists, _step );
    }

    void create( int size_hists, int _step )
    {
        step = _step;
        data = DFloatArrPtr( new pcl::gpu::DeviceArray<float>(size_hists*step) );
    }
    int sizeHists() const { return data->size()/step; }

    int sizeFloats() const { return data->size(); }

    int sizeBytes() const { return data->sizeBytes(); }

    float* ptr() const { return data->ptr(); }
};
typedef boost::shared_ptr<DenseHistograms> DenseHistogramsPtr;
typedef boost::shared_ptr<const DenseHistograms> DenseHistogramsConstPtr;
// @}

/** \brief Convert DeviceArray2D to DenseHistograms structure */
template<class T>
DenseHistogramsPtr cvtToDenseHists( pcl::gpu::DeviceArray2D<T> &src )
{
    DenseHistogramsPtr dst( new DenseHistograms( src.rows(), src.colsBytes()/sizeof(float) ) );
    cudaMemcpy2D( dst->ptr(), src.colsBytes(), src.ptr(), src.step(), src.colsBytes(), src.rows(), cudaMemcpyDeviceToDevice );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    return dst;
}

class DenseCalculations
{
public:

    //======================================================
    //================= BASIC CALCULATIONS =================
    //======================================================

    /** \brief Calcualte PFH features */
    static DenseHistogramsPtr calcPFH( const MetacloudConstPtr &metacloud_gpu,
                                       const NeighborIndicesPtr &indices );

    /** \brief Calcualte PFHRGB features */
    static DenseHistogramsPtr calcPFHRGB( const MetacloudConstPtr &metacloud_gpu,
                                          const NeighborIndicesPtr &indices );

    /** \brief Calcualte FPFH features */
    static DenseHistogramsPtr calcFPFH( const MetacloudConstPtr &metacloud_gpu,
                                        const DPointCloudConstPtr &search_pts,
                                        float radius );

    /** \brief Standarize the input histograms. */
    static DenseHistogramsPtr standarizeHistograms( const DenseHistogramsPtr &in_hists );

    /** \brief Calculate Pearson's correlation for all pairs. */
    static DFloatArrPtr allPairsPearson( const DenseHistogramsConstPtr &hists_a,
                                        const DenseHistogramsConstPtr &hists_b );

    /** \brief Calculate D2 distance for all pairs. */
    static DFloatArrPtr allPairsL2( const DenseHistogramsConstPtr &hists_a,
                                   const DenseHistogramsConstPtr &hists_b );

    //=======================================================
    //================= HISTOGRAM REDUCTION =================
    //=======================================================

    /** \brief Compare all hists and remove repeating (single correlation kernel). */
    static DenseHistogramsPtr singleReduceHistograms( const DenseHistogramsConstPtr &histograms,
                                                      float threshold );

    /** \brief Compare all hists A and B and reduce A (single correlation kernel). */
    static DenseHistogramsPtr singleReduceHistsAUsingB( const DenseHistogramsConstPtr &hists_a,
                                                        const DenseHistogramsConstPtr &hists_b,
                                                        float threshold );

    /** \brief Large-scale version of singleReduceHistsAUsingB. */
    static DenseHistogramsPtr reduceHistsAUsingB( const DenseHistogramsConstPtr &hists_a,
                                                  const DenseHistogramsConstPtr &hists_b,
                                                  float threshold );

    /** \brief Large-scale version of singleReduceHistograms. */
    static DenseHistogramsPtr reduceHistograms( const DenseHistogramsConstPtr &histograms,
                                                float threshold );

    /** \brief Match histograms A against histograms B. Return scores. */
    static std::vector<float> singleMatchHistsAToB( const DenseHistogramsConstPtr &hists_a,
                                               const DenseHistogramsConstPtr &hists_b );

    /** \brief Match histograms A against histograms B. Return scores. */
    static std::vector<float> matchHistsAToB( const DenseHistogramsConstPtr &hists_a,
                                         const DenseHistogramsConstPtr &hists_b );

    //=======================================================
    //================= MEMORY MANIPULATION =================
    //=======================================================

    /** \brief Copy the specified block of histograms to new structure. */
    static DenseHistogramsPtr copyBlock( const DenseHistogramsConstPtr &histograms,
                                         int start_hist, int num_hists );

    /** \brief Copy the histograms of specified indices to a new structure. */
    static DenseHistogramsPtr copySelectedHists( const DenseHistogramsConstPtr &histograms,
                                                 std::vector<int> &indices );

    /** \brief Split histograms into std::vector of limited size hists. */
    static std::vector<DenseHistogramsPtr> splitHistograms( const DenseHistogramsConstPtr &histograms );

    /** \brief Merge histograms std::vector into single histograms structure. */
    static DenseHistogramsPtr mergeHistograms( const std::vector<DenseHistogramsPtr> &histograms );

    /** \brief Combine color into the last 4 bytes of a pointcloud. */
    static DPointCloudPtr addColor( const DPointCloudConstPtr &cloud_xyz,
                                    const DFloatArrPtr &color );
};

}

#endif
