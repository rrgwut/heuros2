#ifndef CALCULATIONS_CUH
#define CALCULATIONS_CUH

#include <core/metacloud.h>

namespace heuros
{

__device__ double calculateD2(DPoint p1, DPoint p2)
{
    float dist;
    dist = pow(p1.x-p2.x,2);
    dist += pow(p1.y-p2.y,2);
    dist += pow(p1.z-p2.z,2);
    dist = sqrt(dist);
    return dist;
}

__device__ double calculateD3(DPoint p1, DPoint p2, DPoint p3)
{
    float ax = p2.x - p1.x;
    float ay = p2.y - p1.y;
    float az = p2.z - p1.z;
    float bx = p3.x - p1.x;
    float by = p3.y - p1.y;
    float bz = p3.z - p1.z;

    double fx = ay*bz - az*by;
    double fy = az*bx - ax*bz;
    double fz = ax*by - ay*bx;

    double field = sqrt( fx*fx + fy*fy + fz*fz );
    return field/2;
}

__device__ double calculateD4(DPoint p1, DPoint p2, DPoint p3, DPoint p4)
{
    float ax = p2.x - p1.x;
    float ay = p2.y - p1.y;
    float az = p2.z - p1.z;
    float bx = p3.x - p1.x;
    float by = p3.y - p1.y;
    float bz = p3.z - p1.z;
    float cx = p4.x - p1.x;
    float cy = p4.y - p1.y;
    float cz = p4.z - p1.z;

    float fx = ay*bz - az*by;
    float fy = az*bx - ax*bz;
    float fz = ax*by - ay*bx;

    float volume = abs( fx*cx + fy*cy + fz*cz );
    return volume/6;
}

__global__ void blockHistograms1D( const float *features,
                                   const int ftrs_step,
                                   const int *point_indices,
                                   const int *start_indices,
                                   const int *chunk_sizes,
                                   int *chunk_histograms,
                                   int hists_step,
                                   int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        // Create shared memory histograms
        __shared__ int shared_hist[ HIST_RES*N_HISTS_1D ];
        if( threadIdx.x < HIST_RES*N_HISTS_1D )
            shared_hist[threadIdx.x] = 0;

        // Synchronize
        __syncthreads();

        // Get the chunk size for this block
        int chunk_size = chunk_sizes[ block_virtual_idx ];

        // Proceed for the threads that have been assigned a point
        if( threadIdx.x < chunk_size ){
            // Get the clusters structure index
            int chunk_start_idx = start_indices[ block_virtual_idx ];
            int clusters_pt_idx = chunk_start_idx + threadIdx.x; // equals chunks_pt_idx

            // Calculate the cloud point index
            int cloud_pt_idx = point_indices[ clusters_pt_idx ];

            for( int i=0; i<N_HISTS_1D; i++ ){
                float ftr_val = features[ i*ftrs_step + cloud_pt_idx ];
                // Count only non-nans
                if( ftr_val == ftr_val ){
                    // Calculate the bin
                    int ftr_bin = 0.5*(ftr_val+0.9999) * HIST_RES;
                    // AtomicAdd
                    int *bin_ptr = shared_hist + i*HIST_RES + ftr_bin;
                    atomicAdd( bin_ptr, 1 );
                }
            }
        }

        // Synchronize
        __syncthreads();

        // Write to global
        if( threadIdx.x<HIST_RES*N_HISTS_1D )
            chunk_histograms[ hists_step*block_virtual_idx + threadIdx.x ] = shared_hist[ threadIdx.x ];
    }
}

__global__ void blockHistograms2D( const float *features,
                                   const int *lut,
                                   const int ftrs_step,
                                   const int offset,
                                   const int *point_indices,
                                   const int *start_indices,
                                   const int *chunk_sizes,
                                   int *chunk_histograms,
                                   int hists_step,
                                   int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        // Create shared memory histograms
        __shared__ int shared_hist[ HIST_2D_STEP*N_HISTS_2D ];
        if( threadIdx.x < HIST_2D_STEP )
            for( int i=0; i<N_HISTS_2D; i++ )
                shared_hist[i*HIST_2D_STEP + threadIdx.x] = 0;

        __shared__ int shared_lut[N_HISTS_2D*2];
        if( threadIdx.x < N_HISTS_2D*2 )
            shared_lut[threadIdx.x] = lut[threadIdx.x];

        // Synchronize
        __syncthreads();

        // Get the chunk size for this block
        int chunk_size = chunk_sizes[ block_virtual_idx ];

        // Proceed for the threads that have been assigned a point
        if( threadIdx.x < chunk_size ){
            // Get the clusters structure index
            int chunk_start_idx = start_indices[ block_virtual_idx ];
            int clusters_pt_idx = chunk_start_idx + threadIdx.x; // equals chunks_pt_idx

            // Calculate the cloud point index
            int cloud_pt_idx = point_indices[ clusters_pt_idx ];

            for( int i=0; i<N_HISTS_2D; i++ ){
                // Get the features of interest
                int ftr_1_idx = shared_lut[2*i];
                int ftr_2_idx = shared_lut[2*i+1];

                float ftr_val_1 = features[ ftr_1_idx*ftrs_step + cloud_pt_idx ];
                float ftr_val_2 = features[ ftr_2_idx*ftrs_step + cloud_pt_idx ];
                // Count only non-nans
                if( ftr_val_1 == ftr_val_1 && ftr_val_2 == ftr_val_2 ){
                    // Calculate the bins
                    int ftr_bin_1 = 0.5*(ftr_val_1+0.9999) * HIST_RES;
                    int ftr_bin_2 = 0.5*(ftr_val_2+0.9999) * HIST_RES;
                    // AtomicAdd
                    int *bin_ptr = shared_hist + i*HIST_2D_STEP + ftr_bin_1*HIST_RES + ftr_bin_2;
                    atomicAdd( bin_ptr, 1 );
                }
            }
        }

        // Synchronize
        __syncthreads();

        // Write to global
        if( threadIdx.x < HIST_2D_STEP )
            for( int i=0; i<N_HISTS_2D; i++ )
                chunk_histograms[ hists_step*block_virtual_idx + offset + i*HIST_2D_STEP + threadIdx.x ] =
                        shared_hist[i*HIST_2D_STEP + threadIdx.x];
    }
}

__global__ void blockMinmax( const DPoint *cloud,
                             const int *point_indices,
                             DPoint *block_mins,
                             DPoint *block_maxes,
                             const int *start_indices,
                             const int *chunk_sizes,
                             int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        // Create shared memory histograms
        __shared__ float shared_min[3];
        __shared__ float shared_max[3];
        if( threadIdx.x < 3 ){
            shared_min[threadIdx.x] = 9999;
            shared_max[threadIdx.x] = -9999;
        }

        // Synchronize
        __syncthreads();

        // Get the chunk size for this block
        int chunk_size = chunk_sizes[ block_virtual_idx ];

        // Proceed for the threads that have been assigned a point
        if( threadIdx.x < chunk_size ){
            // Get the clusters structure index
            int chunk_start_idx = start_indices[ block_virtual_idx ];
            int clusters_pt_idx = chunk_start_idx + threadIdx.x; // equals chunks_pt_idx

            // Get the cloud point
            int cloud_pt_idx = point_indices[ clusters_pt_idx ];
            DPoint point = cloud[ cloud_pt_idx ];

            fatomicMin( shared_min+0, point.x );
            fatomicMin( shared_min+1, point.y );
            fatomicMin( shared_min+2, point.z );

            fatomicMax( shared_max+0, point.x );
            fatomicMax( shared_max+1, point.y );
            fatomicMax( shared_max+2, point.z );
        }

        // Synchronize
        __syncthreads();

        // Write results to global memory
        if( threadIdx.x == 0 ){
            DPoint min, max;
            min.x = shared_min[0]; min.y = shared_min[1]; min.z = shared_min[2];
            max.x = shared_max[0]; max.y = shared_max[1]; max.z = shared_max[2];
            block_mins[block_virtual_idx] = min;
            block_maxes[block_virtual_idx] = max;
        }
    }
}

__global__ void equalizeD234( const int *hi_res_hists,
                              int *dst_hists,
                              int dst_step,
                              int dst_offset,
                              int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        __shared__ int shared_dst_hists[N_HIST_FTRS*HIST_RES];
        if( threadIdx.x < N_HIST_FTRS*HIST_RES )
            shared_dst_hists[ threadIdx.x ] = 0;

        __shared__ int shared_max[N_HIST_FTRS]; // subopt. separate loop?
        if( threadIdx.x < N_HIST_FTRS ){
            shared_max[threadIdx.x] = 0;
        }

        // Synchronize
        __syncthreads();

        // Loop over histogram features
        for( int i=0; i<N_HIST_FTRS; i++ ){
            // Get the feature values for this bin position
            const int *hi_res_addr = hi_res_hists +
                    ( N_HIST_FTRS*block_virtual_idx + i )*D234_RES +
                    threadIdx.x;
            int hi_res_val = *hi_res_addr;

            // If bin val is > 0, atomic max
            if( hi_res_val > 0 )
                atomicMax( shared_max+i, threadIdx.x );

            // Synchronize
            __syncthreads();

            // Calculate dst bin
            int max_bin = shared_max[i];
            float scale = float(HIST_RES) / (max_bin+1.0);
            int dst_bin = threadIdx.x * scale;

            if( hi_res_val > 0 ){
                // Atomic add
                int *shared_dst_addr = shared_dst_hists + i*HIST_RES + dst_bin;
                atomicAdd( shared_dst_addr, hi_res_val ); //*************************
            }
        }

        // Synchronize
        __syncthreads();

        // Write results to global memory
        if( threadIdx.x < N_HIST_FTRS*HIST_RES ){
            int *dst_addr = dst_hists + block_virtual_idx*dst_step + dst_offset + threadIdx.x;
            *dst_addr = shared_dst_hists[ threadIdx.x ];
        }
    }
}

__global__ void blockD234( const DPoint *cloud,
                           const int *cluster_sizes,
                           const int *start_indices,
                           const int *point_indices,
                           const int *corresp_clusters,
                           const float *clusters_d2_max,
                           const float *clusters_d3_max,
                           const float *clusters_d4_max,
                           int *hi_res_histograms,
                           curandState* states,
                           int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x,
         block_virtual_idx = blockIdx.x;
         idx < N;
         idx += blockDim.x*gridDim.x,
         block_virtual_idx += gridDim.x )
    {
        // Create shared memory histograms
        __shared__ int shared_hist[ D234_RES*N_HIST_FTRS ];
        if( threadIdx.x < D234_RES )
            for(int i=0; i<N_HIST_FTRS; i++ )
                shared_hist[i*D234_RES + threadIdx.x] = 0;

        // Synchronize
        __syncthreads();

        int cluster_idx = corresp_clusters[block_virtual_idx];
        int cluster_size = cluster_sizes[cluster_idx];
        int cluster_start = start_indices[cluster_idx];
        double d2_max = clusters_d2_max[cluster_idx];
        double d3_max = clusters_d3_max[cluster_idx];
        double d4_max = clusters_d4_max[cluster_idx];

        // Copy state to local memory for efficiency
        curandState local_state = states[ idx % (5*blockDim.x*gridDim.x) ];

        // Loop to calculate several point sets for statistical relevance
        for( int  i=0; i<N_D4_ITERATIONS; i++ ){
            // Pick random points
            int p1_idx = point_indices[ cluster_start + curand(&local_state) % cluster_size ];
            int p2_idx = point_indices[ cluster_start + curand(&local_state) % cluster_size ];
            int p3_idx = point_indices[ cluster_start + curand(&local_state) % cluster_size ];
            int p4_idx = point_indices[ cluster_start + curand(&local_state) % cluster_size ];
            DPoint p1 = cloud[p1_idx];
            DPoint p2 = cloud[p2_idx];
            DPoint p3 = cloud[p3_idx];
            DPoint p4 = cloud[p4_idx];

            // D4
            double ftr_val = calculateD4( p1, p2, p3, p4 );
            int ftr_bin = ftr_val/d4_max*0.9999 * D234_RES;
            // AtomicAdd
            int *bin_ptr = shared_hist + 2*D234_RES + ftr_bin; // 2 is D4's index
            if( ftr_bin < 0 || ftr_bin >= D234_RES || ftr_bin != ftr_bin )
                printf( "D4 ERROR!!! ... bin: %d :: val: %f :: max: %f\n", ftr_bin, ftr_val, d4_max );
            atomicAdd( bin_ptr, 1 );

            if( i<N_D3_ITERATIONS ){
                ftr_val = calculateD3( p1, p2, p3 );
                ftr_bin = ftr_val/d3_max*0.9999 * D234_RES;
                // AtomicAdd
                bin_ptr = shared_hist + 1*D234_RES + ftr_bin; // 1 is D3's index
                atomicAdd( bin_ptr, 1 );
            }

            if( i<N_D2_ITERATIONS ){
                ftr_val = calculateD2( p1, p2 );
                ftr_bin = ftr_val/d2_max*0.9999 * D234_RES;
                // AtomicAdd
                bin_ptr = shared_hist + 0*D234_RES + ftr_bin; // 0 is D2's index
                atomicAdd( bin_ptr, 1 );
            }
        }

        // Copy state back to global memory
        states[ idx % (5*blockDim.x*gridDim.x) ] = local_state;

        // Synchronize
        __syncthreads();

        // Write to global
        if( threadIdx.x < D234_RES )
            for(int i=0; i<N_HIST_FTRS; i++ )
                hi_res_histograms[ (N_HIST_FTRS*block_virtual_idx+i)*D234_RES + threadIdx.x ] = shared_hist[ i*D234_RES + threadIdx.x ];

    }
}

__global__ void sumBlocks( const int *chunk_histograms,
                           int *cluster_histograms,
                           const int *corresp_clusters,
                           int hists_cluster_step,
                           int chunks_step,
                           int hists_write_offset,
                           int N )
{
    for( int idx = blockIdx.x*blockDim.x + threadIdx.x; idx < N; idx += blockDim.x*gridDim.x )
    {
        int chunk_idx = idx / chunks_step;
        int cluster_idx = corresp_clusters[chunk_idx];

        int idx_from_chunk_start = idx - chunk_idx*chunks_step; // idx % chunks_step;
        int hists_idx = cluster_idx*hists_cluster_step + hists_write_offset + idx_from_chunk_start;
        atomicAdd( cluster_histograms + hists_idx, chunk_histograms[idx] );
    }
}

}

#endif
