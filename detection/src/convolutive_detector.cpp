#include <detection/convolutive_detector.h>

namespace heuros{

ConvolutiveDetector::ConvolutiveDetector()
{

}

ConvolutiveDetector::~ConvolutiveDetector()
{

}

void ConvolutiveDetector::detectObjects( MetacloudPtr _metacloud )
{
    // Generate grid segmentations
    
    // Compact - remove blobs with too few points
    
    // Calculate blob features
    
    // Classify each blob
}

void ConvolutiveDetector::addModel( MetacloudPtr _metacloud, const AnnotationPtr &annotation )
{
    // Calculate max and min coords

    // Calculate feature
    
    // Train classifier
}

void ConvolutiveDetector::addAntimodel( MetacloudPtr _metacloud, const IndicesConstPtr &indices )
{
    // Call addModel  
}

void ConvolutiveDetector::clear()
{

}

}
