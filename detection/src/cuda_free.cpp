#include <detection/dense_calculations.h>


using namespace pcl;
using namespace pcl::gpu;


namespace heuros
{

DenseHistogramsPtr DenseCalculations::calcPFH( const MetacloudConstPtr &metacloud_gpu,
                                               const NeighborIndicesPtr &indices )
{
    PFHEstimation::PointCloud &cloud_gpu = (PFHEstimation::PointCloud&)(*metacloud_gpu->cloud);
    PFHEstimation::Normals &normals_gpu = (PFHEstimation::Normals&)(*metacloud_gpu->normals);

    DeviceArray2D<PFHSignature125> pfh125_features;
    gpu::PFHEstimation fe_gpu;
    fe_gpu.compute(cloud_gpu, normals_gpu, *indices, pfh125_features);

    DenseHistogramsPtr out_histograms = cvtToDenseHists( pfh125_features );

    return out_histograms;

    //int stub;
    //vector<PFHSignature125> downloaded;
    //pfh125_features.download(downloaded, stub);
}

DenseHistogramsPtr DenseCalculations::calcPFHRGB( const MetacloudConstPtr &metacloud_gpu,
                                                  const NeighborIndicesPtr &indices )
{
    // Pack data as X,Y,Z,RGB
    DPointCloudPtr cloud_xyzrgb = addColor( metacloud_gpu->cloud, metacloud_gpu->colors );

    PFHRGBEstimation::PointCloud &cloud_gpu = (PFHRGBEstimation::PointCloud&)(*cloud_xyzrgb);
    PFHRGBEstimation::Normals &normals_gpu = (PFHRGBEstimation::Normals&)(*metacloud_gpu->normals);

    DeviceArray2D<PFHRGBSignature250> pfhrgb250_features;
    gpu::PFHRGBEstimation fe_gpu;
    fe_gpu.compute(cloud_gpu, normals_gpu, *indices, pfhrgb250_features);

    DenseHistogramsPtr out_histograms = cvtToDenseHists( pfhrgb250_features );

    return out_histograms;
}

DenseHistogramsPtr DenseCalculations::calcFPFH( const MetacloudConstPtr &metacloud_gpu,
                                                const DPointCloudConstPtr &search_pts,
                                                float radius )
{
    FPFHEstimation::PointCloud &cloud_gpu = (FPFHEstimation::PointCloud&)(*metacloud_gpu->cloud);
    FPFHEstimation::Normals &normals_gpu = (FPFHEstimation::Normals&)(*metacloud_gpu->normals);

    Octree::PointCloud &octree_cloud = (pcl::gpu::Octree::PointCloud&)(*search_pts);
    NeighborIndices neighbor_indices( search_pts->size(), 1 );
    metacloud_gpu->octree->nearestKSearchBatch( octree_cloud, 1, neighbor_indices );
    DeviceArray<int> &indices = neighbor_indices.data; // 1 neighbor per point

    int avg_nbrs = radius*radius / (NBR_R_1*NBR_R_1) * metacloud_gpu->avg_neighbors;
    int max_nbrs = avg_nbrs*1.2;

    DeviceArray2D<FPFHSignature33> fpfh33_features;
    gpu::FPFHEstimation fe_gpu;
    fe_gpu.setInputCloud( cloud_gpu );
    fe_gpu.setInputNormals( normals_gpu );
    fe_gpu.setRadiusSearch( radius, max_nbrs );
    fe_gpu.setIndices( indices );
    fe_gpu.compute( fpfh33_features);
    //fe_gpu.compute( fpfh33_features, *metacloud_gpu->octree );
    //fe_gpu.compute(cloud_gpu, normals_gpu, *indices, fpfh33_features);

    DenseHistogramsPtr out_histograms = cvtToDenseHists( fpfh33_features );

    return out_histograms;
}

}
