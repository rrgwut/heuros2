#include <detection/histogram_calculations_gpu.h>
#include <stdio.h>
#include <algorithm>
#include <iomanip>

#include <core/impl/manipulations.cuh>
#include <detection/impl/histograms_1dh2d.cuh>


using namespace std;
using namespace pcl::gpu;


namespace heuros
{

///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////// CALCULATION
///////////////////////////////////////////////////////////////////////

HistogramsIntPtr HistogramCalculationsGPU::calcUnnormHistograms( const MetacloudConstPtr &metacloud_gpu,
                                                                 const ClustersPtr &clusters,
                                                                 DeviceArray<curandState>& dev_states,
                                                                 int include_2d )
{
    //==================================================
    //================= INITIALIZATION =================
    //==================================================

    // Create histograms and set them to 0
    //HistogramsIntPtr test_hist = HistogramsIntPtr( new HistogramsInt() );
    int num_clusters = clusters->num_clusters;
    HistogramsIntPtr histograms( new HistogramsInt( num_clusters,
                                                    N_HISTS_1D,
                                                    N_HIST_FTRS,
                                                    include_2d*N_HISTS_2D) );

    // Create LUT
    if( histograms->num_2d_ftrs > 0 ){
        histograms->lut_2d[0] = 0;
        histograms->lut_2d[1] = 1;
        histograms->lut_2d[2] = 0;
        histograms->lut_2d[3] = 2;
        histograms->lut_2d[4] = 1;
        histograms->lut_2d[5] = 2;
        histograms->lut_2d[6] = 3;
        histograms->lut_2d[7] = 4;
    }

    // Set histograms to 0
    //cudaMemset( histograms->data, 0, histograms->total_size*sizeof(int));
    //cudaSafeCall(cudaGetLastError());
    //cudaSafeCall(cudaDeviceSynchronize());
    UtilsGPU::cudaSetIntsTo( histograms->data, 0, histograms->total_size );

    // CPU - assign points to blocks
    clusters->downloadSizesAndStartInd();
    vector<int> v_start_indices, h_chunk_sizes, h_corresp_clusters;
    for( int i=0; i<clusters->num_clusters; i++ ){
        int n_cluster_blocks = (clusters->h_cluster_sizes[i]-1) / BLOCK_SZ + 1;
        for( int j=0; j<n_cluster_blocks; j++ ){
            h_corresp_clusters.push_back(i);
            v_start_indices.push_back( clusters->h_start_indices[i] + j*BLOCK_SZ );
            int chunk_size = clusters->h_cluster_sizes[i] - j*BLOCK_SZ;
            if( chunk_size > BLOCK_SZ ) chunk_size = BLOCK_SZ;
            h_chunk_sizes.push_back( chunk_size );
        }
    }
    int num_chunks = v_start_indices.size();

    DIntArr start_indices( num_chunks );
    DIntArr chunk_sizes( num_chunks );
    DIntArr corresp_clusters( num_chunks );
    start_indices.upload( v_start_indices.data(), num_chunks );
    chunk_sizes.upload( h_chunk_sizes.data(), num_chunks );
    corresp_clusters.upload( h_corresp_clusters.data(), num_chunks );

    // Create chunks histograms
    HistogramsIntPtr chunk_histograms( new HistogramsInt( num_chunks,
                                                          histograms->num_1d_ftrs,
                                                          histograms->num_h_ftrs,
                                                          histograms->num_2d_ftrs ) );

    //=================================================
    //================= 1D HISTOGRAMS =================
    //=================================================

    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = num_chunks*blockSize;
    //<<<1D HIST BLOCKS KERNEL>>>
    blockHistograms1D <<< nBlocks, blockSize >>>( metacloud_gpu->features->data(),
                                                  metacloud_gpu->features->step,
                                                  clusters->point_indices.ptr(),
                                                  start_indices.ptr(),
                                                  chunk_sizes.ptr(),
                                                  chunk_histograms->data,
                                                  chunk_histograms->clusterSize(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    //=================================================
    //================= 2D HISTOGRAMS =================
    //=================================================

    if( histograms->num_2d_ftrs > 0 ){
        blockSize = BLOCK_SZ;
        nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
        N = num_chunks*blockSize;
        //<<<2D HIST BLOCKS KERNEL>>>
        blockHistograms2D <<< nBlocks, blockSize >>>( metacloud_gpu->features->data(),
                                                      histograms->lut_2d,
                                                      metacloud_gpu->features->step,
                                                      histograms->offset2D(),
                                                      clusters->point_indices.ptr(),
                                                      start_indices.ptr(),
                                                      chunk_sizes.ptr(),
                                                      chunk_histograms->data,
                                                      chunk_histograms->clusterSize(), N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());
    }

    //========================================================
    //================= 1D and 2D CHUNKS SUM =================
    //========================================================

    // Create min max structures
    DPointCloud block_mins( num_chunks );
    DPointCloud block_maxes( num_chunks );
    //<<<SUM BLOCKS KERNEL>>>

    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_chunks*chunk_histograms->clusterSize();
    sumBlocks <<< nBlocks, blockSize >>>( chunk_histograms->data,
                                          histograms->data,
                                          corresp_clusters.ptr(),
                                          histograms->clusterSize(),
                                          chunk_histograms->clusterSize(),
                                          0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    //======================================================
    //================= HISTOGRAM FEATURES =================
    //======================================================

    //<<<CHUNK MINMAX KERNEL>>>
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_chunks*blockSize;
    blockMinmax <<< nBlocks, blockSize >>>( metacloud_gpu->cloud->ptr(),
                                            clusters->point_indices.ptr(),
                                            block_mins.ptr(),
                                            block_maxes.ptr(),
                                            start_indices.ptr(),
                                            chunk_sizes.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    // Download min max to host
    vector<DPoint> h_block_mins( block_mins.size() );
    vector<DPoint> h_block_maxes( block_maxes.size() );
    block_mins.download( h_block_mins.data() );
    block_maxes.download( h_block_maxes.data() );

    // Cluster minmax CPU
    vector<float> h_d2_max( num_chunks );
    vector<float> h_d3_max( num_chunks );
    vector<float> h_d4_max( num_chunks );

    int chunk_idx = 0;
    for( int i=0; i<num_clusters; i++ ){
        int cluster_idx = i;
        DPoint min_pt = h_block_mins[chunk_idx];
        DPoint max_pt = h_block_maxes[chunk_idx];

        while( h_corresp_clusters[chunk_idx] == cluster_idx ){
            min_pt.x = min( min_pt.x, h_block_mins[chunk_idx].x );
            min_pt.y = min( min_pt.y, h_block_mins[chunk_idx].y );
            min_pt.z = min( min_pt.z, h_block_mins[chunk_idx].z );

            max_pt.x = max( max_pt.x, h_block_maxes[chunk_idx].x );
            max_pt.y = max( max_pt.y, h_block_maxes[chunk_idx].y );
            max_pt.z = max( max_pt.z, h_block_maxes[chunk_idx].z );

            chunk_idx++;
            if( chunk_idx >= corresp_clusters.size() ) break;
        }

        vector<double> dims(3);
        dims[0] = max_pt.x - min_pt.x;
        dims[1] = max_pt.y - min_pt.y;
        dims[2] = max_pt.z - min_pt.z;
        //sort( dims.begin(), dims.end() );

        double ta = sqrt( dims[0]*dims[0] + dims[1]*dims[1] );
        double tb = sqrt( dims[0]*dims[0] + dims[2]*dims[2] );
        double tc = sqrt( dims[1]*dims[1] + dims[2]*dims[2] );
        double p = 0.5*(ta+tb+tc);

        h_d2_max[cluster_idx] = 1.5*sqrt( dims[0]*dims[0] + dims[1]*dims[1] + dims[2]*dims[2] );
        h_d3_max[cluster_idx] = 1.5*sqrt( p*(p-ta)*(p-tb)*(p-tc) );
        h_d4_max[cluster_idx] = 1.5*dims[0]*dims[1]*dims[2]/6;

        if( h_d4_max[cluster_idx] < 0.0000001 ) // just in case we got some zeros
            h_d4_max[cluster_idx] = 0.0000001;

        if( h_d3_max[cluster_idx] < 0.0000001 ) // just in case we got some zeros
            h_d3_max[cluster_idx] = 0.0000001;

        if( h_d2_max[cluster_idx] < 0.0000001 ) // just in case we got some zeros
            h_d2_max[cluster_idx] = 0.0000001;
    }

    // Upload minmax to GPU
    DFloatArr d2_max( num_chunks );
    DFloatArr d3_max( num_chunks );
    DFloatArr d4_max( num_chunks );
    d2_max.upload( h_d2_max.data(), num_chunks );
    d3_max.upload( h_d3_max.data(), num_chunks );
    d4_max.upload( h_d4_max.data(), num_chunks );

    // Create high-res histograms for D234
    DIntArr hi_res_chunks( D234_RES*N_HIST_FTRS*num_chunks );

    //assert( num_clusters < 4000 );

    //<<<HIST FEATURES BLOCK KERNEL>>>
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_chunks*blockSize;
    blockD234 <<< nBlocks, blockSize >>>( metacloud_gpu->cloud->ptr(),
                                          clusters->cluster_sizes.ptr(),
                                          clusters->start_indices.ptr(),
                                          clusters->point_indices.ptr(),
                                          corresp_clusters.ptr(),
                                          d2_max.ptr(),
                                          d3_max.ptr(),
                                          d4_max.ptr(),
                                          hi_res_chunks.ptr(),
                                          dev_states.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    DIntArr hi_res_histograms( D234_RES*N_HIST_FTRS*num_clusters );
    UtilsGPU::cudaSetIntsTo( hi_res_histograms.ptr(), 0, hi_res_histograms.size() );

    //<<<SUM BLOCKS KERNEL>>
    blockSize = BLOCK_SZ;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_chunks*N_HIST_FTRS*D234_RES;
    sumBlocks <<< nBlocks, blockSize >>>( hi_res_chunks.ptr(),
                                          hi_res_histograms.ptr(),
                                          corresp_clusters.ptr(),
                                          N_HIST_FTRS*D234_RES,
                                          N_HIST_FTRS*D234_RES,
                                          0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    blockSize = D234_RES;
    nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    N = num_clusters*blockSize;
    equalizeD234 <<< nBlocks, blockSize >>>( hi_res_histograms.ptr(),
                                             histograms->data,
                                             histograms->clusterSize(),
                                             histograms->offsetHistFtrs(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return histograms;
}

HistogramsFloatPtr HistogramCalculationsGPU::standarizeHistograms( const HistogramsIntPtr &in_hists )
{
    int num_clusters = in_hists->num_clusters;

    // Create output hists
    HistogramsFloatPtr out_hists( new HistogramsFloat( num_clusters,
                                                       in_hists->num_1d_ftrs,
                                                       in_hists->num_h_ftrs,
                                                       in_hists->num_2d_ftrs) );

    // 1D histograms and histogram features
    int blockSize = HIST_RES;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = in_hists->num_clusters*(N_HISTS_1D+N_HIST_FTRS) * blockSize;
    standarize <<< nBlocks, blockSize >>>( in_hists->data.ptr(),
                                           out_hists->data.ptr(),
                                           in_hists->clusterSize(),
                                           HIST_RES,
                                           N_HISTS_1D+N_HIST_FTRS, 0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // 2D histograms
    if( in_hists->num_2d_ftrs > 0 ){
        blockSize = HIST_2D_STEP;
        nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
        N = in_hists->num_clusters*N_HISTS_2D * blockSize;
        standarize <<< nBlocks, blockSize >>>( in_hists->data.ptr(),
                                               out_hists->data.ptr(),
                                               in_hists->clusterSize(),
                                               HIST_2D_STEP,
                                               N_HISTS_2D,
                                               in_hists->offset2D(), N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());
    }

    return out_hists;
}

DFloatArrPtr HistogramCalculationsGPU::allPairsPearson( const HistogramsFloatPtr &hists_a,
                                                       const HistogramsFloatPtr &hists_b )
{
    // 1D histograms
    assert( hists_a->numFeatures() == hists_b->numFeatures() );
    int num_features = hists_a->numFeatures();
    int a_num_hists = hists_a->num_clusters;
    int b_num_hists = hists_b->num_clusters;

    // Create output correlations
    int corrs_size = a_num_hists * b_num_hists * num_features;
    DFloatArrPtr correlations( new DFloatArr(corrs_size) );
    if( corrs_size == 0 ) return correlations;

    // 1D histograms and histogram features
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = a_num_hists*b_num_hists*(N_HISTS_1D+N_HIST_FTRS);
    pearsonOnStd <<< nBlocks, blockSize >>>( hists_a->data,
                                             hists_b->data,
                                             correlations->ptr(),
                                             hists_a->clusterSize(),
                                             HIST_RES,
                                             a_num_hists,
                                             b_num_hists,
                                             0, num_features, 0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // 2D histograms
    if( hists_a->num_2d_ftrs > 0 ){
        blockSize = BLOCK_SZ;
        nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
        N = a_num_hists*b_num_hists*N_HISTS_2D;
        pearsonOnStd <<< nBlocks, blockSize >>>( hists_a->data,
                                                 hists_b->data,
                                                 correlations->ptr(),
                                                 hists_a->clusterSize(),
                                                 HIST_2D_STEP,
                                                 a_num_hists,
                                                 b_num_hists,
                                                 hists_a->offset2D(),
                                                 num_features,
                                                 N_HISTS_1D+N_HIST_FTRS, N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());
    }
    return correlations;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// REDUCTION
///////////////////////////////////////////////////////////////////////

HistogramsFloatPtr HistogramCalculationsGPU::reduceSingleHistSet( HistogramsFloatPtr &histograms,
                                                                  vector<float> &ftr_weights,
                                                                  float threshold )
{
    // Manage degenerated cases
    if( histograms->num_clusters == 0 )
        return copyBlock( histograms, 0, histograms->num_clusters );

    // Calculate all correlations
    DFloatArrPtr correlations = allPairsPearson( histograms, histograms );
    vector<float> h_correlations( correlations->size() );
    correlations->download( h_correlations.data() );
    // Weights sum
    float weights_sum = 0;
    for( int w=0; w<ftr_weights.size(); w++ ) weights_sum += ftr_weights[w];
    float scaled_thresh = threshold * weights_sum;

    // Find repeated histograms
    vector<int> nonrep_hist_indices;
    vector<bool> rep_mask( histograms->num_clusters );
    for( int h=0; h<histograms->num_clusters; h++ ){
        bool repeated = false;
        for( int g=0; g<h; g++ ){
            if( rep_mask[g] ) continue; // Compare only to included
            repeated = false;
            int pair_idx = h*histograms->num_clusters+g;
            float score=0;
            for( int f=0; f<histograms->numFeatures(); f++ )
                score += h_correlations[pair_idx*histograms->numFeatures()+f] * ftr_weights[f];
            if( score >= scaled_thresh ){
                repeated = true;
                break;
            }
        }
        rep_mask[h] = repeated;
        if( !repeated ){
            nonrep_hist_indices.push_back(h);
        }
    }

    // Copy to new histograms structure
    HistogramsFloatPtr out_hists = copySelectedHists( histograms, nonrep_hist_indices );

    return out_hists;
}

HistogramsFloatPtr HistogramCalculationsGPU::reduceHistSetAUsingB( HistogramsFloatPtr &hists_a,
                                                                   HistogramsFloatPtr &hists_b,
                                                                   vector<float> &ftr_weights,
                                                                   float threshold )
{
    // Manage degenerated cases
    if( hists_a->num_clusters == 0 || hists_b->num_clusters == 0 )
        return copyBlock( hists_a, 0, hists_a->num_clusters );

    // Calculate all correlations
    DFloatArrPtr correlations = allPairsPearson( hists_a, hists_b );
    vector<float> h_correlations( correlations->size() );
    correlations->download( h_correlations.data() );

    // Weights sum
    float weights_sum = 0;
    for( int w=0; w<ftr_weights.size(); w++ ) weights_sum += ftr_weights[w];
    float scaled_thresh = threshold * weights_sum;

    // Find repeated histograms
    vector<int> nonrep_hist_indices;
    for( int a=0; a<hists_a->num_clusters; a++ ){
        bool repeated = false;
        for( int b=0; b<hists_b->num_clusters; b++ ){
            repeated = false;
            int pair_idx = a*hists_b->num_clusters+b;
            float score=0;
            for( int f=0; f<hists_a->numFeatures(); f++ )
                score += h_correlations[pair_idx*hists_a->numFeatures()+f] * ftr_weights[f];
            if( score >= scaled_thresh ){
                repeated = true;
                break;
            }
        }
        if( !repeated )
            nonrep_hist_indices.push_back(a);
    }

    // Copy to new histograms structure
    HistogramsFloatPtr out_hists = copySelectedHists( hists_a, nonrep_hist_indices );
    return out_hists;
}

HistogramsFloatPtr HistogramCalculationsGPU::LSReduceHistSetAUsingB( HistogramsFloatPtr &hists_a,
                                                                     HistogramsFloatPtr &hists_b,
                                                                     vector<float> &ftr_weights,
                                                                     float threshold )
{
    vector<HistogramsFloatPtr> vector_a = splitHistograms( hists_a );
    vector<HistogramsFloatPtr> vector_b = splitHistograms( hists_b );

    // All a-b set combinations
    for( int a=0; a<vector_a.size(); a++ )
        for( int b=0; b<vector_b.size(); b++ )
            vector_a[a] = reduceHistSetAUsingB( vector_a[a], vector_b[b], ftr_weights, threshold );

    // Merge reduced hists_a
    return mergeHistograms( vector_a );
}

HistogramsFloatPtr HistogramCalculationsGPU::reduceHistograms( HistogramsFloatPtr &histograms,
                                                               vector<float> &ftr_weights,
                                                               float threshold )
{
    vector<HistogramsFloatPtr> reduced_hists = splitHistograms( histograms );

    for( int p=0; p<reduced_hists.size(); p++ )
        if( reduced_hists[p]->num_clusters > 1 )
            reduced_hists[p] = reduceSingleHistSet( reduced_hists[p], ftr_weights, threshold );

    // Inter-set
    for( int i=0; i<reduced_hists.size()-1; i++ )
        for( int j=i+1; j<reduced_hists.size(); j++ )
            reduced_hists[j] = reduceHistSetAUsingB( reduced_hists[j], reduced_hists[i], ftr_weights, threshold );

    // Merge reduced histograms
    return mergeHistograms( reduced_hists );
}

vector<float> HistogramCalculationsGPU::matchAToB( HistogramsFloatPtr &hists_a,
                                                   HistogramsFloatPtr &hists_b,
                                                   vector<float> &ftr_weights )
{
    // Calculate all correlations
    DFloatArrPtr correlations = allPairsPearson( hists_a, hists_b );
    vector<float> h_correlations( correlations->size() );
    correlations->download( h_correlations.data() );

    // Create output scores vector
    vector<float> scores(hists_a->num_clusters);

    // Weights sum
    float weights_sum = 0;
    for( int w=0; w<ftr_weights.size(); w++ ) weights_sum += ftr_weights[w];

    // Find repeated histograms
    for( int a=0; a<hists_a->num_clusters; a++ ){
        float best_score = 0;
        for( int b=0; b<hists_b->num_clusters; b++ ){
            int pair_idx = a*hists_b->num_clusters+b;
            float score=0;
            for( int f=0; f<hists_a->numFeatures(); f++ )
                score += h_correlations[pair_idx*hists_a->numFeatures()+f] * ftr_weights[f];
            if( score > best_score )
                best_score = score;
        }
        scores[a] = best_score / weights_sum;
    }

    return scores;
}

vector<float> HistogramCalculationsGPU::LSMatchAToB( HistogramsFloatPtr &hists_a,
                                                     HistogramsFloatPtr &hists_b,
                                                     vector<float> &ftr_weights )
{
    vector<HistogramsFloatPtr> vector_a = splitHistograms( hists_a );
    vector<HistogramsFloatPtr> vector_b = splitHistograms( hists_b );
    vector<float> scores;

    // All a-b set combinations
    for( int a=0; a<vector_a.size(); a++ ){
        vector<float> part_best_scores( vector_a[a]->num_clusters, 0 );
        for( int b=0; b<vector_b.size(); b++ ){
            vector<float> ab_scores = matchAToB( vector_a[a], vector_b[b], ftr_weights );
            for( int h=0; h<ab_scores.size(); h++ )
                if( ab_scores[h] > part_best_scores[h] )
                    part_best_scores[h] = ab_scores[h];
        }
        scores.insert( scores.end(), part_best_scores.begin(), part_best_scores.end() );
    }

    return scores;
}

////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// MEMORY MANIPULATION
////////////////////////////////////////////////////////////////////////

HistogramsFloatPtr HistogramCalculationsGPU::copyBlock( HistogramsFloatPtr &histograms,
                                                        int start_hist, int num_hists )
{
    HistogramsFloatPtr out_hists( new HistogramsFloat( num_hists,
                                                       histograms->num_1d_ftrs,
                                                       histograms->num_h_ftrs,
                                                       histograms->num_2d_ftrs ) );

    UtilsGPU::manualMemcpy<float>( out_hists->data, histograms->data+start_hist*histograms->clusterSize(), out_hists->total_size );
    return out_hists;
}

HistogramsFloatPtr HistogramCalculationsGPU::copySelectedHists( HistogramsFloatPtr &histograms,
                                                                vector<int> &indices )
{
    // Copy to new histograms structure
    HistogramsFloatPtr out_hists( new HistogramsFloat( indices.size(),
                                                       histograms->num_1d_ftrs,
                                                       histograms->num_h_ftrs,
                                                       histograms->num_2d_ftrs ) );
    DeviceArray<int> d_indices(indices.size());
    d_indices.upload( indices );
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = out_hists->total_size;
    copySelected <<< nBlocks, blockSize >>>( histograms->data,
                                             out_hists->data,
                                             d_indices.ptr(),
                                             histograms->clusterSize(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return out_hists;
}

vector<HistogramsFloatPtr> HistogramCalculationsGPU::splitHistograms( HistogramsFloatPtr &histograms )
{
    const int max_floats = 1000000;
    const int split_sz = max_floats/histograms->clusterSize();

    int full_splits = histograms->num_clusters / split_sz;
    int num_parts = histograms->num_clusters % split_sz == 0 ? full_splits : full_splits + 1;
    vector<HistogramsFloatPtr> split_hists(num_parts);

    for( int p=0; p<num_parts; p++ ){
        int p_sz = split_sz;
        if( p == num_parts-1 )
            p_sz = histograms->num_clusters % split_sz;
        split_hists[p] = copyBlock( histograms, p*split_sz, p_sz );
    }

    return split_hists;
}

HistogramsFloatPtr HistogramCalculationsGPU::mergeHistograms( const vector<HistogramsFloatPtr> &histograms )
{
    assert( histograms.size() > 0 );
    int new_num_hists = 0;
    for( int i=0; i<histograms.size(); i++ )
        new_num_hists += histograms[i]->num_clusters;

    HistogramsFloatPtr out_hists( new HistogramsFloat( new_num_hists,
                                                       histograms[0]->num_1d_ftrs,
                                  histograms[0]->num_h_ftrs,
            histograms[0]->num_2d_ftrs ) );

    // Copy to new histograms structure
    int scan = 0;
    for( int i=0; i<histograms.size(); i++ ){
        if( !histograms[i] ) continue;
        if( histograms[i]->total_size == 0 ) continue;
        UtilsGPU::manualMemcpy<float>( out_hists->data + scan, histograms[i]->data, histograms[i]->total_size );
        scan += histograms[i]->total_size;
    }

    return out_hists;
}

void HistogramCalculationsGPU::initCurands( DeviceArray<curandState>& dev_states, int size )
{
    dev_states.create(size);

    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = dev_states.size();
    setupRandom <<< nBlocks, blockSize >>>( dev_states.ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

}
