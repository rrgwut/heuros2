#include <detection/dense_calculations.h>
#include <core/impl/manipulations.cuh>


using namespace std;
using namespace pcl::gpu;


namespace heuros
{

DenseHistogramsPtr DenseCalculations::standarizeHistograms( const DenseHistogramsPtr &in_hists )
{
    int num_hists = in_hists->sizeHists();

    // Create output hists
    DenseHistogramsPtr out_hists( new DenseHistograms( num_hists, in_hists->step ) );


    // 1D histograms and histogram features
    int blockSize = in_hists->step;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = in_hists->sizeFloats();
    standarize <<< nBlocks, blockSize >>>( in_hists->ptr(),
                                           out_hists->ptr(),
                                           in_hists->step,
                                           in_hists->step,
                                           1, 0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return out_hists;
}

DFloatArrPtr DenseCalculations::allPairsL2( const DenseHistogramsConstPtr &hists_a,
                                                  const DenseHistogramsConstPtr &hists_b )
{
    assert( hists_a->step == hists_b->step );
    int a_num_hists = hists_a->sizeHists();
    int b_num_hists = hists_b->sizeHists();

    // Create output distances
    DFloatArrPtr distances( new DFloatArr(a_num_hists*b_num_hists) );
    if( a_num_hists*b_num_hists == 0 ) return distances;

    // 1D histograms and histogram features
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = a_num_hists*b_num_hists;
    L2Kernel <<< nBlocks, blockSize >>>( hists_a->ptr(),
                                         hists_b->ptr(),
                                         distances->ptr(),
                                         hists_a->step,
                                         hists_a->step,
                                         a_num_hists,
                                         b_num_hists,
                                         0, 1, 0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return distances;
}

DFloatArrPtr DenseCalculations::allPairsPearson( const DenseHistogramsConstPtr &hists_a,
                                                       const DenseHistogramsConstPtr &hists_b )
{
    assert( hists_a->step == hists_b->step );
    int a_num_hists = hists_a->sizeHists();
    int b_num_hists = hists_b->sizeHists();

    // Create output correlations
    DFloatArrPtr correlations( new DFloatArr(a_num_hists*b_num_hists) );
    if( a_num_hists*b_num_hists == 0 ) return correlations;

    // 1D histograms and histogram features
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = a_num_hists*b_num_hists;
    pearsonOnStd <<< nBlocks, blockSize >>>( hists_a->ptr(),
                                             hists_b->ptr(),
                                             correlations->ptr(),
                                             hists_a->step,
                                             hists_a->step,
                                             a_num_hists,
                                             b_num_hists,
                                             0, 1, 0, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return correlations;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// REDUCTION
///////////////////////////////////////////////////////////////////////

DenseHistogramsPtr DenseCalculations::singleReduceHistograms( const DenseHistogramsConstPtr &histograms,
                                                              float threshold )
{
    // Manage degenerated cases
    if( histograms->sizeHists() == 0 )
        return copyBlock( histograms, 0, histograms->sizeHists() );

    // Calculate all correlations
    DFloatArrPtr correlations = allPairsPearson( histograms, histograms );
    vector<float> h_correlations( correlations->size() );
    correlations->download( h_correlations.data() );

    // Find repeated histograms
    vector<int> nonrep_hist_indices;
    vector<bool> rep_mask( histograms->sizeHists() );
    for( int h=0; h<histograms->sizeHists(); h++ ){
        bool repeated = false;
        for( int g=0; g<h; g++ ){
            // Compare only to non-repeated
            if( rep_mask[g] ) continue;
            int pair_idx = h*histograms->sizeHists()+g;
            float score = h_correlations[pair_idx];
            if( score >= threshold ){
                repeated = true;
                break;
            }
        }
        rep_mask[h] = repeated;
        if( !repeated ){
            nonrep_hist_indices.push_back(h);
        }
    }

    // Copy to new histograms structure
    DenseHistogramsPtr out_hists = copySelectedHists( histograms, nonrep_hist_indices );

    return out_hists;
}

DenseHistogramsPtr DenseCalculations::singleReduceHistsAUsingB( const DenseHistogramsConstPtr &hists_a,
                                                                const DenseHistogramsConstPtr &hists_b,
                                                                float threshold )
{
    // Manage degenerated cases
    if( hists_a->sizeHists() == 0 || hists_b->sizeHists() == 0 )
        return copyBlock( hists_a, 0, hists_a->sizeHists() );

    // Calculate all correlations
    DFloatArrPtr correlations = allPairsPearson( hists_a, hists_b );
    vector<float> h_correlations( correlations->size() );
    correlations->download( h_correlations.data() );

    // Find repeated histograms
    vector<int> nonrep_hist_indices;
    for( int a=0; a<hists_a->sizeHists(); a++ ){
        bool repeated = false;
        for( int b=0; b<hists_b->sizeHists(); b++ ){
            int pair_idx = a*hists_b->sizeHists()+b;
            float score = h_correlations[pair_idx];
            if( score >= threshold ){
                repeated = true;
                break;
            }
        }
        if( !repeated )
            nonrep_hist_indices.push_back(a);
    }

    // Copy to new histograms structure
    DenseHistogramsPtr out_hists = copySelectedHists( hists_a, nonrep_hist_indices );
    return out_hists;
}

DenseHistogramsPtr DenseCalculations::reduceHistsAUsingB( const DenseHistogramsConstPtr &hists_a,
                                                          const DenseHistogramsConstPtr &hists_b,
                                                          float threshold )
{
    vector<DenseHistogramsPtr> vector_a = splitHistograms( hists_a );
    vector<DenseHistogramsPtr> vector_b = splitHistograms( hists_b );

    // All a-b set combinations
    for( int a=0; a<vector_a.size(); a++ )
        for( int b=0; b<vector_b.size(); b++ )
            vector_a[a] = singleReduceHistsAUsingB( vector_a[a], vector_b[b], threshold );

    // Merge reduced hists_a
    return mergeHistograms( vector_a );
}

DenseHistogramsPtr DenseCalculations::reduceHistograms( const DenseHistogramsConstPtr &histograms,
                                                        float threshold )
{
    vector<DenseHistogramsPtr> reduced_hists = splitHistograms( histograms );

    for( int p=0; p<reduced_hists.size(); p++ )
        if( reduced_hists[p]->sizeHists() > 1 )
            reduced_hists[p] = singleReduceHistograms( reduced_hists[p], threshold );

    // Inter-set
    for( int i=0; i<reduced_hists.size()-1; i++ )
        for( int j=i+1; j<reduced_hists.size(); j++ )
            reduced_hists[j] = singleReduceHistsAUsingB( reduced_hists[j], reduced_hists[i], threshold );

    // Merge reduced histograms
    return mergeHistograms( reduced_hists );
}

vector<float> DenseCalculations::singleMatchHistsAToB( const DenseHistogramsConstPtr &hists_a,
                                                       const DenseHistogramsConstPtr &hists_b )
{
    // Calculate all correlations
    DFloatArrPtr correlations = allPairsPearson( hists_a, hists_b );
    vector<float> h_correlations( correlations->size() );
    correlations->download( h_correlations.data() );

    // Create output scores vector
    vector<float> scores(hists_a->sizeHists());

    // Find repeated histograms
    for( int a=0; a<hists_a->sizeHists(); a++ ){
        float best_score = 0;
        for( int b=0; b<hists_b->sizeHists(); b++ ){
            int pair_idx = a*hists_b->sizeHists()+b;
            float score = h_correlations[pair_idx];
            if( score > best_score )
                best_score = score;
        }
        scores[a] = best_score;
    }

    return scores;
}

vector<float> DenseCalculations::matchHistsAToB( const DenseHistogramsConstPtr &hists_a,
                                                 const DenseHistogramsConstPtr &hists_b )
{
    vector<DenseHistogramsPtr> vector_a = splitHistograms( hists_a );
    vector<DenseHistogramsPtr> vector_b = splitHistograms( hists_b );
    vector<float> scores;

    // All a-b set combinations
    for( int a=0; a<vector_a.size(); a++ ){
        vector<float> part_best_scores( vector_a[a]->sizeHists(), 0 );
        for( int b=0; b<vector_b.size(); b++ ){
            vector<float> ab_scores = singleMatchHistsAToB( vector_a[a], vector_b[b] );
            for( int h=0; h<ab_scores.size(); h++ )
                if( ab_scores[h] > part_best_scores[h] )
                    part_best_scores[h] = ab_scores[h];
        }
        scores.insert( scores.end(), part_best_scores.begin(), part_best_scores.end() );
    }

    return scores;
}

////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// MEMORY MANIPULATION
////////////////////////////////////////////////////////////////////////

DenseHistogramsPtr DenseCalculations::copyBlock( const DenseHistogramsConstPtr &histograms,
                                                 int start_hist, int num_hists )
{
    DenseHistogramsPtr out_hists( new DenseHistograms( num_hists, histograms->step ) );
    UtilsGPU::manualMemcpy<float>( out_hists->ptr(), histograms->ptr()+start_hist*histograms->step, out_hists->sizeFloats() );
    return out_hists;
}

DenseHistogramsPtr DenseCalculations::copySelectedHists( const DenseHistogramsConstPtr &histograms,
                                                         vector<int> &indices )
{
    // Copy to new histograms structure
    DenseHistogramsPtr out_hists( new DenseHistograms( indices.size(), histograms->step ) );
    DeviceArray<int> d_indices(indices.size());
    d_indices.upload( indices );
    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = out_hists->sizeFloats();
    copySelected <<< nBlocks, blockSize >>>( histograms->ptr(),
                                             out_hists->ptr(),
                                             d_indices.ptr(),
                                             histograms->step, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return out_hists;
}

vector<DenseHistogramsPtr> DenseCalculations::splitHistograms( const DenseHistogramsConstPtr &histograms )
{
    const int max_floats = 1000000;
    const int split_sz = max_floats/histograms->step;

    int full_splits = histograms->sizeHists() / split_sz;
    int num_parts = histograms->sizeHists() % split_sz == 0 ? full_splits : full_splits + 1;
    vector<DenseHistogramsPtr> split_hists(num_parts);

    for( int p=0; p<num_parts; p++ ){
        int p_sz = split_sz;
        if( p == num_parts-1 )
            p_sz = histograms->sizeHists() % split_sz;
        split_hists[p] = copyBlock( histograms, p*split_sz, p_sz );
    }

    return split_hists;
}

DenseHistogramsPtr DenseCalculations::mergeHistograms( const vector<DenseHistogramsPtr> &histograms )
{
    assert( histograms.size() > 0 );
    int new_num_hists = 0;
    for( int i=0; i<histograms.size(); i++ )
        new_num_hists += histograms[i]->sizeHists();

    DenseHistogramsPtr out_hists( new DenseHistograms( new_num_hists, histograms[0]->step ) );

    // Copy to new histograms structure
    int scan = 0;
    for( int i=0; i<histograms.size(); i++ ){
        if( !histograms[i] ) continue;
        if( histograms[i]->sizeFloats() == 0 ) continue;
        UtilsGPU::manualMemcpy<float>( out_hists->ptr() + scan, histograms[i]->ptr(), histograms[i]->sizeFloats() );
        scan += histograms[i]->sizeFloats();
    }

    return out_hists;
}

DPointCloudPtr DenseCalculations::addColor( const DPointCloudConstPtr &cloud_xyz,
                                            const DFloatArrPtr &color )
{
    DPointCloudPtr dst( new DPointCloud(cloud_xyz->size()) );

    int blockSize = BLOCK_SZ;
    int nBlocks = NUM_SM*(MAX_SM_THREADS/blockSize);
    int N = cloud_xyz->size();
    addColorKernel <<< nBlocks, blockSize >>>( dst->ptr(), cloud_xyz->ptr(), color->ptr(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    return dst;
}

}
