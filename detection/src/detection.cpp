#include <detection/detection.h>
#include <boost/chrono.hpp>


using namespace std;


namespace heuros
{

Detection::Detection()
{
    //HistogramCalculationsGPU::initCurands(dev_states, 2048*15*5);
    clear();
    scheduled_clear = false;
}

Detection::~Detection()
{
    cout << "[Heuros::Detection] Destructor" << endl;
}

void Detection::detectObjects( MetacloudPtr _metacloud )
{
    if( scheduled_clear ){
        clear();
        scheduled_clear = false;
    }

    boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();

    metacloud = _metacloud;
    active_idx = -1;

    if( model_names.size() ){
        cout << "WARING: NO OBJECTS ARE KNOWN" << endl;
        //return;
    }

    // Convolutive object detector
    conv_detector.detectObjects(metacloud);

    boost::chrono::nanoseconds sec = boost::chrono::high_resolution_clock::now() - start;
    cout << "\033[31m" << "Detection: Processing time: " << sec.count()/1000000 << " ms" << "\033[0m" << endl;
}

void Detection::addModel( const MetacloudConstPtr &_metacloud, const AnnotationPtr &annotation )
{
    /*int class_idx = -1;
    for(size_t i=0; i<model_names.size(); i++){
        if( annotation->name == model_names[i] ){
            class_idx = i;
            break;
        }
    }

    if(class_idx == -1){
        class_idx = model_names.size();
        model_names.push_back( annotation->name );
    }

    // Particles
    particles.addModel( class_idx, annotation, dev_states );*/
}

void Detection::addAntimodel( const MetacloudConstPtr &_metacloud, const AnnotationPtr &annotation )
{
    /*particles.addAntimodel( indices, dev_states );*/
}

void Detection::scheduleClear()
{
    scheduled_clear = true;
}

void Detection::clear()
{
    model_names.clear();
    conv_detector.clear();
}

}
