# Name of this module
set(MODULE_NAME detection)

# Remove VTK defs for nvcc
remove_vtk_definitions(${MODULE_NAME})

# General settings
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS
     src/*.cpp src/*.hpp src/*.cu include/*.h include/*.hpp include/*.cuh)

# Add CUDA library
cuda_add_library(${MODULE_NAME} STATIC ${SOURCES})
add_dependencies(${MODULE_NAME} interface)
target_link_libraries(${MODULE_NAME} ${LINK_TO_${MODULE_NAME}})
