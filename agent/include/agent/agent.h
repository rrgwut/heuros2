#ifndef AGENT_H
#define AGENT_H

#include <iostream>
#include <pcl/point_cloud.h>

#include <core/metacloud.h>
#include <detection/detection.h>
#include <interface/interface.h>
#include <features/simple_features.h>
#include <segmentation/segmentation.h>
#include <registration/registration.h>
#include <visualization/visualization.h>


namespace heuros
{

// Other class declarations
class SimpleFeatures;
class Segmentation;
class Detection;
class Visualization;
class Interface;
class Registration;

class Agent
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Pointers to the other modules instances */
    // @{
    Segmentation *segmentation;
    Detection *detection;
    Visualization *visualization;
    Interface *interface;
    Registration *registration;
    // }@

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Main scene processing method */
    void process( const MetacloudPtr &metacloud );

    /** \brief Register */
    void processRegistration(const MetacloudPtr &target_cloud, const MetacloudPtr &source_cloud );

};

}

#endif
