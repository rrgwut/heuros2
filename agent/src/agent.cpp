#include <agent/agent.h>

using namespace std;

namespace heuros
{

void Agent::process( const MetacloudPtr &metacloud )
{
    SimpleFeatures::process(metacloud);
    segmentation->process( metacloud );
    detection->detectObjects( metacloud );
    visualization->setMetacloud( metacloud );
    interface->update( metacloud );
}

void Agent::processRegistration(const MetacloudPtr &target_cloud, const MetacloudPtr &source_cloud)
{
    SimpleFeatures::process(source_cloud);
    registration->processCPU(target_cloud, source_cloud);
    //MetacloudPtr merged = registration->processCPU(target_cloud, source_cloud);
    MetacloudPtr merged = registration->processGPU(target_cloud, source_cloud);
    visualization->setMetacloud(merged);
    interface->update(merged);
}

}
