#!/bin/bash

SCRIPT_DIR=$(dirname \"$0\")

shopt -s globstar
for file in ./**/annotation2Dfinal/index.json
#for file in ./**/tttest.txt
do
    echo "Processing $file"
    TEXT=$(cat $file)
    TEXT_FILTERED="${TEXT//\\\"/\"}"
    echo $TEXT_FILTERED > $file
done

